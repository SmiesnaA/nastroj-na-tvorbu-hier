const instances = [];
let nextId = 0;

function getSibling(el, selector, direction = 1) {
  const sibling =
    direction > 0 ? el.nextElementSibling : el.previousElementSibling;
  if (!sibling || sibling.matches(selector)) {
    return sibling;
  }
  return getSibling(sibling, selector, direction);
}

function emit(el, type, data = {}) {
  const event = document.createEvent("Event");
  Object.keys(data).forEach((key) => {
    event[key] = data[key];
  });
  event.initEvent(type, true, true);
  el.dispatchEvent(event);
}

class ContextMenu {
  constructor(
    selector,
    options = {
      className: "",
      minimalStyling: false,
    }
  ) {
    this.selector = selector;
    this.items = [
      {
        name: "Kopírovať",
        fn: () => {
          copyCommand(this.element);
        },
      },
      {
        name: "Prilepiť",
        fn: () => {
          let commands =
            gameScene.getCurrLevel().objectProgrammed.parsedCommands;
          pasteCommand(this.element, commands);
        },
      },
      {
        name: "Vymazať",
        fn: () => {
          let commands =
            gameScene.getCurrLevel().objectProgrammed.parsedCommands;
          deleteCommand(this.element, commands);
        },
      },
    ];
    this.options = options;
    this.id = nextId++;
    this.target = null;

    this.create();
    instances.push(this);
  }

  create() {
    this.menu = document.createElement("ul");
    this.menu.className = "ContextMenu";
    this.menu.setAttribute("data-contextmenu", this.id);
    this.menu.setAttribute("tabindex", -1);
    this.menu.addEventListener("keyup", (e) => {
      switch (e.which) {
        case 38:
          this.moveFocus(-1);
          break;
        case 40:
          this.moveFocus(1);
          break;
        case 27:
          this.hide();
          break;
        default:
      }
    });

    if (this.options.className) {
      this.options.className
        .split(" ")
        .forEach((cls) => this.menu.classList.add(cls));
    }

    this.items.forEach((item, index) => {
      const li = document.createElement("li");

      if (!("name" in item)) {
        li.className = "ContextMenu-divider";
      } else {
        li.className = "ContextMenu-item";
        li.textContent = item.name;
        li.setAttribute("data-contextmenuitem", index);
        li.setAttribute("tabindex", 0);
        li.addEventListener("click", this.select.bind(this, li));
        li.addEventListener("keyup", (e) => {
          if (e.which === 13) {
            this.select(li);
          }
        });
      }

      this.menu.appendChild(li);
    });

    document.body.appendChild(this.menu);
    emit(this.menu, "created");
  }

  show(e, el) {
    this.element = el;
    this.menu.style.left = `${e.pageX}px`;
    this.menu.style.top = `${e.pageY}px`;
    this.menu.classList.add("is-open");
    this.target = e.target;
    this.menu.focus();
    e.preventDefault();

    emit(this.menu, "shown");
  }
  hide() {
    this.menu.classList.remove("is-open");
    this.target = null;
    emit(this.menu, "hidden");
  }

  select(item) {
    const itemId = item.getAttribute("data-contextmenuitem");
    if (this.items[itemId]) {
      this.items[itemId].fn(this.target);
    }
    this.hide();
    emit(this.menu, "itemselected");
  }

  moveFocus(direction = 1) {
    const focused = this.menu.querySelector("[data-contextmenuitem]:focus");
    let next;

    if (focused) {
      next = getSibling(focused, "[data-contextmenuitem]", direction);
    }

    if (!next) {
      next =
        direction > 0
          ? this.menu.querySelector("[data-contextmenuitem]:first-child")
          : this.menu.querySelector("[data-contextmenuitem]:last-child");
    }

    if (next) next.focus();
  }

  on(type, fn) {
    this.menu.addEventListener(type, fn);
  }
  off(type, fn) {
    this.menu.removeEventListener(type, fn);
  }
  destroy() {
    this.menu.parentElement.removeChild(this.menu);
    this.menu = null;
    instances.splice(instances.indexOf(this), 1);
  }
}

// document.addEventListener("contextmenu", (e) => {
//   instances.forEach((menu) => {
//     if (e.target.parentNode.matches(menu.selector)) {
//       menu.show(e);
//     }
//   });
// });

document.addEventListener("click", (e) => {
  instances.forEach((menu) => {
    if (
      !e.target.matches(
        `[data-contextmenu="${menu.id}"], [data-contextmenu="${menu.id}"] *`
      )
    ) {
      menu.hide();
    }
  });
});
