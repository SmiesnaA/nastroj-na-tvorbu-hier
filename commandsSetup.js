const playerCommands = {
  movement: [
    new Up(new Const(10)).toJSON(),
    new Down(new Const(10)).toJSON(),
    new Lt(new Const(10)).toJSON(),
    new Rt(new Const(10)).toJSON(),
    new TurnLt().toJSON(),
    new TurnRt().toJSON(),
    new SetY(new Const(60)).toJSON(),
    new SetR(new Const(1)).toJSON(),
    new SetC(new Const(1)).toJSON(),
    new SetRC(new Const(1), new Const(1)).toJSON(),
    new MoveToCell(
      new Direction(DIRECTIONS_NAMES.LEFT),
      new Direction(DIRECTIONS_NAMES.NULL)
    ).toJSON(),
    new IncX(new Const(10)).toJSON(),
    new IncY(new Const(10)).toJSON(),

    new DecX(new Const(10)).toJSON(),
    new DecY(new Const(10)).toJSON(),
    new SetX(new Const(60)).toJSON(),
    new SetY(new Const(60)).toJSON(),
  ],
  events: [
    new IfKeyPressed(
      new Key(KEY_LISTENER.LEFT),
      null,
      new InterruptHandler(INTERRUPT_OPTIONS.IGNORE)
    ).toJSON(),
    new WhenStartClicked(
      null,
      new InterruptHandler(INTERRUPT_OPTIONS.IGNORE)
    ).toJSON(),
    new Interruption().toJSON(),
  ],
  condition: [
    new If().toJSON(),
    new IfElse().toJSON(),
    new IsObject(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Direction(DIRECTIONS_NAMES.LEFT),
      new Direction(DIRECTIONS_NAMES.NULL)
    ).toJSON(),
    new IsNotObject(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Direction(DIRECTIONS_NAMES.LEFT),
      new Direction(DIRECTIONS_NAMES.NULL)
    ).toJSON(),
    new IsRowCol(new Const(1), new Const(1)).toJSON(),
    new IsRowColObject(
      new Const(1),
      new Const(1),
      new ObjectType(OBJECTS_NAMES.NOTHING)
    ).toJSON(),
    new IsCollisionWith(
      new ObjectType(OBJECTS_NAMES.ENEMY),
      new Direction(DIRECTIONS_NAMES.LEFT)
    ).toJSON(),
  ],
  loop: [
    new While(null, null).toJSON(),
    new Repeat(new Const(3)).toJSON(),
    new RepeatForever(null).toJSON(),
  ],
  subprogram: [],
  general: [
    new StopExecution().toJSON(),
    new StopGame().toJSON(),
    new Wait(new Const(1000)).toJSON(),
    new DeleteObject().toJSON(),
    new HideObject().toJSON(),
    new ShowObject().toJSON(),
    new MoveObjectTo(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Const(1),
      new Const(1)
    ).toJSON(),
    new MoveObjectBy(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Const(1),
      new Const(1)
    ).toJSON(),
  ],
  scene: [
    new IsScore(new Const(0)).toJSON(),
    new SetScore(new Const(0)).toJSON(),
    new ChangeScore(new Const(1)).toJSON(),
    new IsTimer(new Const(0)).toJSON(),
    new SetTimer(new Const(0)).toJSON(),
    new ShowText(new Text("")).toJSON(),
    new RemoveText().toJSON(),
  ],
  mySubprograms: [],
  variables: [],
};

const enemyCommands = {
  movement: [
    new Up(new Const(10)).toJSON(),
    new Down(new Const(10)).toJSON(),
    new Lt(new Const(10)).toJSON(),
    new Rt(new Const(10)).toJSON(),
    new TurnLt().toJSON(),
    new TurnRt().toJSON(),
    new SetR(new Const(1)).toJSON(),
    new SetC(new Const(1)).toJSON(),
    new SetRC(new Const(1), new Const(1)).toJSON(),
    new MoveToCell(
      new Direction(DIRECTIONS_NAMES.LEFT),
      new Direction(DIRECTIONS_NAMES.NULL)
    ).toJSON(),
    new IncX(new Const(10)).toJSON(),
    new IncY(new Const(10)).toJSON(),
    new DecX(new Const(10)).toJSON(),
    new DecY(new Const(10)).toJSON(),
    new SetX(new Const(60)).toJSON(),
    new SetY(new Const(60)).toJSON(),
  ],
  events: [
    new WhenStartClicked(
      null,
      new InterruptHandler(INTERRUPT_OPTIONS.IGNORE)
    ).toJSON(),
    new Interruption().toJSON(),
  ],
  condition: [
    new If().toJSON(),
    new IfElse().toJSON(),
    new IsObject(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Direction(DIRECTIONS_NAMES.LEFT),
      new Direction(DIRECTIONS_NAMES.NULL)
    ).toJSON(),
    new IsNotObject(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Direction(DIRECTIONS_NAMES.LEFT),
      new Direction(DIRECTIONS_NAMES.NULL)
    ).toJSON(),
    new IsRowCol(new Const(1), new Const(1)).toJSON(),
    new IsRowColObject(
      new Const(1),
      new Const(1),
      new ObjectType(OBJECTS_NAMES.NOTHING)
    ).toJSON(),
    new IsCollisionWith(
      new ObjectType(OBJECTS_NAMES.PLAYER),
      new Direction(DIRECTIONS_NAMES.LEFT)
    ).toJSON(),
  ],
  loop: [
    new While(null, null).toJSON(),
    new Repeat(new Const(3)).toJSON(),
    new RepeatForever().toJSON(),
  ],
  subprogram: [new ChasePlayer().toJSON()],
  general: [
    new SetExecutionSpeed(new Const(25)).toJSON(),
    new StopExecution().toJSON(),
    new StopGame().toJSON(),
    new Wait(new Const(1000)).toJSON(),
    new DeleteObject().toJSON(),
    new HideObject().toJSON(),
    new ShowObject().toJSON(),
    new ChasePlayer().toJSON(),
    new MoveObjectTo(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Const(1),
      new Const(1)
    ).toJSON(),
    new MoveObjectBy(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Const(1),
      new Const(1)
    ).toJSON(),
  ],
  scene: [
    new IsScore(new Const(0)).toJSON(),
    new SetScore(new Const(0)).toJSON(),
    new ChangeScore(new Const(1)).toJSON(),
    new IsTimer(new Const(0)).toJSON(),
    new SetTimer(new Const(0)).toJSON(),
    new ShowText(new Text("")).toJSON(),
    new RemoveText().toJSON(),
  ],
  mySubprograms: [],
  variables: [],
};

const itemCommands = {
  movement: [
    new SetR(new Const(1)).toJSON(),
    new SetC(new Const(1)).toJSON(),
    new SetRC(new Const(1), new Const(1)).toJSON(),
    new MoveToCell(
      new Direction(DIRECTIONS_NAMES.LEFT),
      new Direction(DIRECTIONS_NAMES.NULL)
    ).toJSON(),
    new IncX(new Const(10)).toJSON(),
    new IncY(new Const(10)).toJSON(),
    new DecX(new Const(10)).toJSON(),
    new DecY(new Const(10)).toJSON(),
    new SetX(new Const(60)).toJSON(),
    new SetY(new Const(60)).toJSON(),
  ],
  events: [
    new WhenStartClicked(
      null,
      new InterruptHandler(INTERRUPT_OPTIONS.IGNORE)
    ).toJSON(),
    new Interruption().toJSON(),
  ],
  condition: [
    new If().toJSON(),
    new IfElse().toJSON(),
    new IsObject(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Direction(DIRECTIONS_NAMES.LEFT),
      new Direction(DIRECTIONS_NAMES.NULL)
    ).toJSON(),
    new IsNotObject(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Direction(DIRECTIONS_NAMES.LEFT),
      new Direction(DIRECTIONS_NAMES.NULL)
    ).toJSON(),
    new IsRowCol(new Const(1), new Const(1)).toJSON(),
    new IsRowColObject(
      new Const(1),
      new Const(1),
      new ObjectType(OBJECTS_NAMES.NOTHING)
    ).toJSON(),
    new IsCollisionWith(
      new ObjectType(OBJECTS_NAMES.PLAYER),
      new Direction(DIRECTIONS_NAMES.LEFT)
    ).toJSON(),
  ],
  loop: [
    new While(null, null).toJSON(),
    new Repeat(new Const(3)).toJSON(),
    new RepeatForever(null).toJSON(),
  ],
  subprogram: [],
  general: [
    new SetExecutionSpeed(new Const(25)).toJSON(),
    new StopExecution().toJSON(),
    new StopGame().toJSON(),
    new Wait(new Const(1000)).toJSON(),
    new DeleteObject().toJSON(),
    new HideObject().toJSON(),
    new ShowObject().toJSON(),
    new MoveObjectTo(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Const(1),
      new Const(1)
    ).toJSON(),
    new MoveObjectBy(
      new ObjectType(OBJECTS_NAMES.NOTHING),
      new Const(1),
      new Const(1)
    ).toJSON(),
  ],
  scene: [
    new IsScore(new Const(0)).toJSON(),
    new SetScore(new Const(0)).toJSON(),
    new ChangeScore(new Const(1)).toJSON(),
    new IsTimer(new Const(0)).toJSON(),
    new SetTimer(new Const(0)).toJSON(),
    new ShowText(new Text("")).toJSON(),
    new RemoveText().toJSON(),
  ],
  mySubprograms: [],
  variables: [],
};

const COMMANDS_TYPE = {
  [OBJECTS.PLAYER]: playerCommands,
  [OBJECTS.ENEMY]: enemyCommands,
  [OBJECTS.ITEM]: itemCommands,
  mySubprograms: [],
};
