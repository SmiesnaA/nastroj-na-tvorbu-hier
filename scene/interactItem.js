const interactObjectItem = {
  create(objectId) {
    var objectClicked = null;
    interact("canvas")
      .draggable({
        max: Infinity,
        maxPerElement: Infinity,
        origin: "self",
        cursorChecker() {
          return null;
        },
        listeners: {
          move: function (event) {
            dragMoveListener(event);
          },
        },
      })
      .on("tap", function (event) {
        clickListener(event);
      });

    interact(".object").on("tap", function (event) {
      cursorChosen = CURSOR_TYPES.POINTER;
      app.changeCursor(cursorChosen);

      objectClicked = event.currentTarget;

      if (objectClicked.classList.contains("objectClicked")) {
        objectClicked.classList.remove("objectClicked");
      } else {
        document
          .querySelectorAll(".objectClicked")
          .forEach((obj) => obj.classList.remove("objectClicked"));
        objectClicked.classList.add("objectClicked");
      }
    });

    interact("canvas").dropzone({
      accept: ".object",
      overlap: 0.75,

      ondropactivate: function (event) {
        event.target.classList.add("drop-active");
      },
      ondragenter: function (event) {
        var draggableElement = event.relatedTarget;
        var dropzoneElement = event.target;

        dropzoneElement.classList.add("drop-target");
        draggableElement.classList.add("moved-item");
        draggableElement.remove();
      },
      ondragleave: function (event) {
        event.relatedTarget.classList.remove("cloned-item");
        event.relatedTarget.classList.remove("dropped-item");
        event.relatedTarget.classList.remove("can-drop");
      },
      ondrop: function (event) {
        event.relatedTarget.classList.remove("cloned-item");
        event.relatedTarget.classList.remove("moved-item");
        event.relatedTarget.classList.add("dropped-item");
      },
      ondropdeactivate: function (event) {
        event.target.classList.remove("drop-active");
        event.target.classList.remove("drop-target");
      },
    });

    function draw(r, c, imgName, src, objectType) {
      var scene = gameScene.getCurrLevel().scene;
      const obj = new GameObject(
        scene.objectId++,
        r,
        c,
        scene.cellSize,
        imgName,
        src,
        objectType
      );
      scene.setSrcId(obj);
      if (objectType == OBJECTS.PLAYER) {
        scene.setPlayer(r, c, obj);
      } else {
        scene.setGrid(r, c, obj);
      }
      obj.draw();
    }

    function erase(r, c) {
      gameScene.getCurrLevel().scene.removeObject(r, c);
    }

    function clickListener(event) {
      objectClicked = document.querySelector(".objectClicked");
      var scene = gameScene.getCurrLevel().scene;
      const canvas = scene.getCanvas();
      var rect = canvas.getBoundingClientRect();
      var point = getTransformedPoint(
        event.clientX - rect.left,
        event.clientY - rect.top
      );
      const rc = getRC(point.x, point.y);
      const r = rc[0];
      const c = rc[1];

      if (cursorChosen == CURSOR_TYPES.ERASER) {
        erase(r, c);
        return;
      }
      if (!objectClicked || cursorChosen == CURSOR_TYPES.MOVE) {
        return;
      }

      const src = objectClicked.getAttribute("src");
      const imgName = objectClicked.getAttribute("name");
      var objectType;
      if (objectClicked.classList.contains("item")) {
        objectType = OBJECTS.ITEM;
      } else if (objectClicked.classList.contains("wall")) {
        objectType = OBJECTS.WALL;
      } else if (objectClicked.classList.contains("player")) {
        objectType = OBJECTS.PLAYER;
      } else if (objectClicked.classList.contains("enemy")) {
        objectType = OBJECTS.ENEMY;
      }
      draw(r, c, imgName, src, objectType);
    }

    function dragMoveListener(event) {
      objectClicked = document.querySelector(".objectClicked");
      var scene = gameScene.getCurrLevel().scene;
      var point = getTransformedPoint(event.pageX, event.pageY);
      const rc = getRC(point.x, point.y);

      const r = rc[0];
      const c = rc[1];

      if (cursorChosen == CURSOR_TYPES.ERASER) {
        erase(r, c);
        return;
      }
      if (!objectClicked || cursorChosen == CURSOR_TYPES.MOVE) {
        return;
      }

      const src = objectClicked.getAttribute("src");
      const imgName = objectClicked.getAttribute("name");
      var objectType;
      if (objectClicked.classList.contains("item")) {
        objectType = OBJECTS.ITEM;
      } else if (objectClicked.classList.contains("wall")) {
        objectType = OBJECTS.WALL;
      } else if (objectClicked.classList.contains("player")) {
        objectType = OBJECTS.PLAYER;
      } else if (objectClicked.classList.contains("enemy")) {
        objectType = OBJECTS.ENEMY;
      }
      draw(r, c, imgName, src, objectType);
    }

    window.addEventListener(
      "contextmenu",
      function (evt) {
        evt.preventDefault();
      },
      false
    );

    function getTransformedPoint(x, y) {
      var scene = gameScene.getCurrLevel().scene;
      var ctx = scene.getCtx();
      const transform = ctx.getTransform();
      const inverseZoom = 1 / transform.a;

      const transformedX = inverseZoom * x - inverseZoom * transform.e;
      const transformedY = inverseZoom * y - inverseZoom * transform.f;
      return { x: transformedX, y: transformedY };
    }
  },
};
