const programPlayerCommands = [];

var TabObject = Vue.extend({
  created() {
    this.$root.$refs.TabObject = this;
  },
  template: `
    <v-card height="100%">
    <v-row>
    <v-col cols="9" class="pa-0">
    <programming-area id="programmingArea" :object="object"></programming-area>
  </v-col>
    <v-col cols="3" class="pa-0" id="expansionPanel">
    <v-card height="100%">
    <expansion-panel id="expansionPanel" :object="object"></expansion-panel>
    </v-card>
  </v-col>
  </v-row>
  </v-card>`,
  data() {
    return {
      object: gameScene.getCurrLevel().objectProgrammed,
    };
  },
});

Vue.component("tabObject", TabObject);

Vue.component("expansionPanel", {
  created() {
    this.$root.$refs.TabExpansionPanel = this;
  },
  props: ["object"],
  template: `
  <v-expansion-panels accordion>
  <events :commands="commands"></events>
  <movement :commands="commands"></movement>
  <condition :commands="commands"></condition>
  <loop :commands="commands"></loop>
  <general :commands="commands"></general>
  <scene :commands="commands"></scene>
  <my-subprograms :commands="commands"></my-subprograms>
    </v-expansion-panels>`,

  watch: {
    object() {
      this.commands =
        this.object == null ? null : COMMANDS_TYPE[this.object.type];
    },
    commands() {
      this.commands =
        this.object == null ? null : COMMANDS_TYPE[this.object.type];
    },
  },
  data() {
    return {
      commands: this.object == null ? null : COMMANDS_TYPE[this.object.type],
      variables: [],
    };
  },
  methods: {
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
    saveMySubprogram(name) {
      const newSub = new Subroutine(null, name);
      const newCall = new Call(newSub);
      addToSubprograms(newSub.toJSON());
      addToSubprograms(newCall.toJSON());
    },
    saveMyVariable(name) {
      const variable = new Variable(name);
      const newVar = new GetVariable(variable).toJSON();
      const newSetVar = new SetVariable(variable, new Const(0)).toJSON();
      const newChangeVar = new ChangeVariable(
        new Variable(name),
        new Const(1)
      ).toJSON();
      this.variables.push(name);
      COMMANDS_TYPE[this.object.type].variables.push(newVar);
      if (this.variables.length == 1) {
        COMMANDS_TYPE[this.object.type].variables.push(newSetVar);
        COMMANDS_TYPE[this.object.type].variables.push(newChangeVar);
      }
    },
    getVariables() {
      return this.variables;
    },
  },
});

Vue.component("programmingArea", {
  props: ["object"],
  created() {
    this.$root.$refs.ProgrammingArea = this;
  },
  template: `
        <draggable @change="onUnpublishedChange" @contextmenu.native="handler($event)" :swapThreshold="0" v-if="object != null" tag="ul" class="startCommands v-card" :list="object.parsedCommands" :options="clonedCommandOptions" :key="control">
          <li v-for="(el, index) in object.parsedCommands" :key="uuid(el)">
            <condition-command @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type === 'condition'"></condition-command>
            <loop-command @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type === 'loop'"></loop-command>
            <event-command @contextmenu.native="handler($event, el)" class="list-container" v-if="el.type === 'event'" :el="el"></event-command>
            <loop-go-command @contextmenu.native="handler($event, el)" class="list-container" v-if="el.type === 'loopGo'" :el="el"></loop-go-command>
            <variable-command @contextmenu.native="handler($event, el)" class="list-container variableCommand" v-if="el.type === 'variable'" :el="el"></variable-command>
            <general-command @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type === 'general' || el.type == null || el.type == 'callMySubprogram'"></general-command>
            <define-subprogram-command  @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type == 'mySubprograms'"></define-subprogram-command>
            <sound-command  @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type == 'sound'"></sound-command>
          </li>
        </draggable>`,
  props: {
    object: {
      required: true,
    },
  },
  created() {},
  data() {
    return {
      control: 0,
      clonedCommandOptions: {
        ghostClass: "draggable-ghost",
        group: "items",
        emptyInsertThreshold: "20",
        sort: true,
        put: true,
        pull: false,
      },
    };
  },
  methods: {
    onUnpublishedChange({ added }) {
      // if (added && added.element.type == "event") {
      //   let res = findCommand(added.element, this.object.parsedCommands);
      //   if (res) {
      //     let [el, elCommands] = res;
      //     if (el.uid != added.element.uid) {
      //       elCommands.splice(elCommands.indexOf(el), 1);
      //       return;
      //     }
      //   }
      // }
    },
    handler: function (e, el) {
      e.stopPropagation();
      e.preventDefault();
      customMenu.show(e, el);
    },
    changeParameter(index, element) {
      Vue.set(
        element,
        "parameter",
        parseInt(document.getElementById("commandInput" + index).value)
      );
    },
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },
    uuid(e) {
      if (e.uid) return e.uid;

      globalId += 1;
      const key = globalId;
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("generalCommand", {
  props: {
    el: {
      required: true,
    },
  },
  template: `<v-col><v-row>
      <v-col cols="4">{{ el.representationName }}</v-col>
      <v-col v-if="el.parameterType === 'input'" cols="5">
        <input :id="'commandInput' + el.uid" type="number" :value="el.parameter" @change="changeParameter(el.uid, el)" step="1" :min="el.paramMin" :max="el.paramMax">
        </v-col>
      <v-col v-if="el.parameterType === 'textarea'" cols="4">
      <input :id="'commandInput' + el.uid" :value="el.parameter" type="text" @change="changeParameter(el.uid, el)" minlength="1" maxlength="30">
        </v-col>
      <v-col v-if="el.parameterType === 'inputInput'" cols="8">
        <input :id="'commandInput1' + el.uid" type="number" :value="el.parameter1" @change="changeParameter2(el.uid, el, 1)" step="1" :min="el.paramMin" :max="el.paramMax">
        <input :id="'commandInput2' + el.uid" type="number" :value="el.parameter2" @change="changeParameter2(el.uid, el, 2)" step="1" :min="el.paramMin" :max="el.paramMax">
      </v-col>
      <v-col v-if="el.parameterType === 'inputInputSelect'" cols="8">
      <input :id="'commandInput1' + el.uid" type="number" :value="el.parameter1" @change="changeParameter2(el.uid, el, 1)" step="1" :min="el.paramMin" :max="el.paramMax">
      <input :id="'commandInput2' + el.uid" type="number" :value="el.parameter2" @change="changeParameter2(el.uid, el, 2)" step="1" :min="el.paramMin" :max="el.paramMax">
      <select name="options1" :id="'commandInput3' + el.uid"  v-model="el.parameter3" @change="changeParameter2(el.uid, el, 3)" class="text-center">
          <option v-for="option in el.options" :value="option">{{ option }}</option>
        </select>
      </v-col>
      <v-col v-if="el.parameterType === 'selectInputInput'" cols="8">
      <select name="options" :id="'commandInput1' + el.uid"  v-model="el.parameter1" @change="changeParameter2(el.uid, el, 1)" class="text-center">
          <option v-for="option in el.options" :value="option">{{ option }}</option>
        </select>
      <input :id="'commandInput2' + el.uid" type="number" :value="el.parameter2" @change="changeParameter2(el.uid, el, 2)" step="1" :min="el.paramMin" :max="el.paramMax">
      <input :id="'commandInput3' + el.uid" type="number" :value="el.parameter3" @change="changeParameter2(el.uid, el, 3)" step="1" :min="el.paramMin" :max="el.paramMax">
      </v-col>
      <v-col v-if="el.parameterType === 'selectSelect'" cols="4">
        <select name="options1" :id="'commandInput1' + el.uid"  v-model="el.parameter1" @change="changeParameter2(el.uid, el, 1)" class="text-center">
          <option v-for="option in el.options1" :value="option">{{ option }}</option>
        </select>
        <select name="option2" :id="'commandInput2' + el.uid"  v-model="el.parameter2" @change="changeParameter2(el.uid, el, 2)" class="text-center">
        <option v-for="option in el.options2" :value="option">{{ option }}</option>
      </select>
        </v-col>
        <v-col v-if="el.parameterType === 'selectSelectInput'" cols="4">
        <select name="option1" :id="'commandInput1' + el.uid"  v-model="el.parameter1" @change="changeParameter2(el.uid, el, 1)" class="text-center">
          <option v-for="option in el.options1" :value="option">{{ option }}</option>
        </select>
        <select name="option2" :id="'commandInput2' + el.uid"  v-model="el.parameter2" @change="changeParameter2(el.uid, el, 2)" class="text-center">
          <option v-for="option in el.options2" :value="option">{{ option }}</option>
        </select>
        <input list="input" :value="el.parameter3" @change="changeParameter2(el.uid, el, 3)">
        <datalist id="input">
          <option value="hneď"></option>
        </datalist>
        </v-col>
        <v-col v-if="el.parameterType === 'selectSelectSelect'" cols="8">
        <select name="options1" :id="'commandInput1' + el.uid"  v-model="el.parameter1" @change="changeParameter2(el.uid, el, 1)" class="text-center">
          <option v-for="option in el.options1" :value="option">{{ option }}</option>
        </select>
        <select name="option2" :id="'commandInput2' + el.uid"  v-model="el.parameter2" @change="changeParameter2(el.uid, el, 2)" class="text-center">
        <option v-for="option in el.options2" :value="option">{{ option }}</option>
        </select>
        <select name="option3" :id="'commandInput3' + el.uid"  v-model="el.parameter3" @change="changeParameter2(el.uid, el, 3)" class="text-center">
        <option v-for="option in el.options3" :value="option">{{ option }}</option>
        </select>
        </v-col>
      <v-col  cols="5" v-if="el.parameterType === 'select'"><select name="options":id="'commandInput' + el.uid"  v-model="el.parameter"
        @change="changeParameter(el.uid, el)" class="float-right text-center">
        <option v-for="option in el.options" :value="option">{{ option }}</option>
      </select></v-col>
    </v-row>
    <v-row>
  </v-row></v-col>`,
  methods: {
    handler: function (e, el) {
      e.preventDefault();
      customMenu.show(e, el);
    },
    changeParameter(index, element) {
      Vue.set(
        element,
        "parameter",
        document.getElementById("commandInput" + index).value
      );
    },
    changeParameter2(index, element, number) {
      Vue.set(
        element,
        "parameter" + number,
        document.getElementById("commandInput" + number + index).value
      );
    },
  },
});

Vue.component("defineSubprogramCommand", {
  props: {
    el: {
      required: true,
    },
  },
  template: `<v-col><v-row>
  <v-col cols="4">{{ el.representationName }}</v-col>
    </v-row>
    <v-row>
    <v-col cols="8"><nested-draggable class="nested-item" :commands="el.commands.commands1"/></v-col>
  </v-row></v-col>`,
});

Vue.component("eventCommand", {
  props: {
    el: {
      required: true,
    },
  },

  template: `
      <v-col class="eventCommand">
      <v-row>
      <v-col cols="3">Po prerušení</v-col>
      <v-col cols="4">
      <select  name="options" class="float-right text-center" id="options" :id="'commandInput1' + el.uid"
      v-model="el.interruptParam" @change="changeInterruptParameter(el.uid, el, 1)">
      <option v-for="option in el.interruptOptions" :value="option">{{ option }}</option>
      </select></v-col>
      </v-row>
      <v-row>
        <v-col cols="5" text-align="start">{{ el.representationName }}</v-col>
        <v-col cols="6" v-if="el.subtype == 'keyPress'"><select  name="options" class="float-right text-center" id="options" :id="'commandInput2' + el.uid"
        v-model="el.parameter" @change="changeParameter(el.uid, el, 2)">
        <option v-for="option in el.options" :value="option">{{ option }}</option>
        </select></v-col>
      </v-row>
      <v-row>
      </v-row>
      <v-row>
        <v-col cols="1"></v-col>
        <v-col cols="11"><nested-draggable class="nested-item" :commands="el.commands.commands1"/></v-col>
      </v-row>
    </v-col>`,
  methods: {
    changeParameter(index, element, number) {
      Vue.set(
        element,
        "parameter",
        document.getElementById("commandInput" + number + index).value
      );
    },
    changeInterruptParameter(index, element, number) {
      Vue.set(
        element,
        "interruptParameter",
        document.getElementById("commandInput" + number + index).value
      );
    },
  },
});

Vue.component("loopGoCommand", {
  props: {
    el: {
      required: true,
    },
  },

  template: `
    <v-col>
      <v-row>
        <v-col cols="4" text-align="start">{{ el.representationName }}</v-col>
        <v-col cols="4"><select  name="options" class="float-right text-center" id="options" :id="'commandInput1' + el.uid"
        v-model="el.parameter1" @change="changeParameter(el.uid, el, 1)">
        <option v-for="option in el.options" :value="option">{{ option }}</option>
        </select></v-col>
        <v-col cols="4" v-if="el.parameter2 != null"><input :id="'commandInput2' + el.uid" type="number" :value="el.parameter2" @change="changeParameter(el.uid, el, 2)" step="1" min="1" max="5"></v-col>
      </v-row>
    </v-col>`,
  methods: {
    changeParameter(index, element, number) {
      Vue.set(
        element,
        "parameter" + number,
        document.getElementById("commandInput" + number + index).value
      );
    },
  },
});

Vue.component("variableCommand", {
  props: {
    el: {
      required: true,
    },
  },

  template: `
    <v-col>
      <v-row>
        <v-col cols="4">{{ el.representationName }}</v-col>
        <v-row v-if="el.parameterType == 'selectInput'">
        <v-col cols="4"><select  name="options" class="float-right text-center" id="options" :id="'commandInput1' + el.uid"
        v-model="el.parameter1" @change="changeParameter(el.uid, el, 1)">
        <option v-for="option in el.options" :value="option">{{ option }}</option>
        </select></v-col>
        <v-col cols="4" v-if="el.parameter2 != null">
        <input :id="'commandInput2' + el.uid" type="number" :value="el.parameter2" @change="changeParameter(el.uid, el, 2)" step="1" min="1" max="5"></v-col>
      </v-row>
        </v-row>
    </v-col>`,
  created() {
    this.el.options = this.$root.$refs.TabExpansionPanel.getVariables();
  },
  methods: {
    changeParameter(index, element, number) {
      Vue.set(
        element,
        "parameter",
        document.getElementById("commandInput" + number + index).value
      );
    },
  },
});

Vue.component("conditionCommand", {
  props: {
    el: {
      required: true,
    },
  },
  template: `
    <div>
      <if-then-command :el="el"  v-if="el.name === 'if'"></if-then-command>
      <if-then-else-command :el="el"  v-if="el.name === 'ifElse'"></if-then-else-command>
      <v-col v-if="el.name !== 'if' && el.name !== 'ifElse'">
        <v-row>
          <v-col cols="12">{{ el.representationName }}</v-col>
        </v-row>
        <v-row>
          <v-col cols="1"></v-col>
          <v-col cols="11"><nested-draggable class="nested-item" :commands="el.commands.commands1"/></v-col>
        </v-row>
      </v-col>
    </div>`,
});

Vue.component("loopCommand", {
  props: {
    el: {
      required: true,
    },
  },
  template: `
  <v-col>
  <v-row>
    <v-col cols="4">{{ el.representationName }}</v-col>
    <v-col cols="8" v-if="el.parameter != null"><input :id="'commandInput' + el.uid" type="number" :value="el.parameter" @change="changeParameter(el.uid, el)" step="1" min="1" max="5"></v-col>
    <v-col cols="8" v-if="el.parameterType == 'while'"><nested-draggable class="nested-item" :commands="el.commands.commands1"/></v-col>
    </v-row>
  <v-row>
    <v-col cols="1"></v-col>
    <v-col cols="11" v-if="el.parameterType != 'while'"><nested-draggable class="nested-item" :commands="el.commands.commands1"/></v-col>
    <v-col cols="11" v-if="el.parameterType == 'while'"><nested-draggable class="nested-item" :commands="el.commands.commands2"/></v-col>
  </v-row>
</v-col>`,
  methods: {
    changeParameter(index, element) {
      Vue.set(
        element,
        "parameter",
        document.getElementById("commandInput" + index).value
      );
    },
  },
});

Vue.component("levelCommand", {
  props: {
    el: {
      required: true,
    },
  },
  template: `
  <v-col>
  <v-row>
    <v-col cols="4">{{ el.representationName }}</v-col>
    <v-col cols="6">
      <select :id="'commandInput' + el.uid" name="options" class="float-right text-center" @change="changeParameter(el.uid, el)">
        <option v-for="option in el.options" :value="option.number">{{ option.number }}</option>
      </select>
    </v-col>
  </v-row>
</v-col>`,
  created() {
    if (this.el.parameter == null && this.el.options.length != 0) {
      this.el.parameter = 1;
    }
  },
  methods: {
    changeParameter(index, element) {
      Vue.set(
        element,
        "parameter",
        document.getElementById("commandInput" + index).value
      );
    },
  },
});

Vue.component("soundCommand", {
  props: {
    el: {
      required: true,
    },
  },
  template: `
  <v-col>
  <v-row>
    <v-col cols="4">{{ el.representationName }}</v-col>
    <v-col cols="6">
      <select  :id="'commandInput' + el.uid"  name="options" id="options" class="float-right text-center" @change="changeParameter(el.uid, el)">
        <option v-for="(option, index) in el.options" :value="index">{{ option.name }}</option>
      </select>
    </v-col>
  </v-row>
</v-col>`,
  created() {
    if (this.el.parameter == null && this.el.options.length != 0) {
      this.el.parameter = 0;
    }
  },
  methods: {
    changeParameter(index, element) {
      Vue.set(
        element,
        "parameter",
        document.getElementById("commandInput" + index).value
      );
    },
    getName(name) {
      return name.split(/[\\\/]/).pop();
    },
  },
});

Vue.component("ifThenCommand", {
  props: {
    el: {
      required: true,
    },
  },
  template: `
  <v-col>
    <v-row>
      <v-col cols="12">{{ el.representationName1 }}</v-col>
    </v-row>
    <v-row>
      <v-col cols="1"></v-col>
      <v-col cols="11"><nested-draggable class="nested-item" :commands="el.commands.commands1"/></v-col>
    </v-row>
    <v-row>
      <v-col cols="12">{{ el.representationName2 }}</v-col>
    </v-row>
    <v-row>
      <v-col cols="1"></v-col>
      <v-col cols="11"><nested-draggable class="nested-item" :commands="el.commands.commands2"/></v-col>
    </v-row>
  </v-col>`,
});

Vue.component("ifThenElseCommand", {
  props: {
    el: {
      required: true,
    },
  },
  template: ` 
  <v-col>
    <v-row>
      <v-col cols="12">{{ el.representationName1 }}</v-col>
    </v-row>
    <v-row>
      <v-col cols="1"></v-col>
      <v-col cols="11"><nested-draggable class="nested-item" :commands="el.commands.commands1"/></v-col>
    </v-row>
    <v-row>
      <v-col cols="12">{{ el.representationName2 }}</v-col>
    </v-row>
    <v-row>
      <v-col cols="1"></v-col>
      <v-col cols="11"><nested-draggable class="nested-item" :commands="el.commands.commands2"/></v-col>
    </v-row>
    <v-row>
      <v-col cols="12">{{ el.representationName3 }}</v-col>
      <v-col cols="1"></v-col><v-col cols="11"><nested-draggable class="nested-item" :commands="el.commands.commands3"/></v-col>
    </v-row>
</v-col>`,
});

Vue.component("control", {
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Ovládanie</v-expansion-panel-header>
        <v-expansion-panel-content>
          <control-commands></control-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("controlCommands", {
  props: ["commands"],
  template: `
  <draggable v-model="commands.control" :options="availableCommandOptions" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="element in commands.control" :key="element.order">
            {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
        sort: false,
      },
    };
  },

  methods: {
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("events", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Snímanie</v-expansion-panel-header>
        <v-expansion-panel-content>
          <events-commands"></events-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("eventsCommands", {
  props: ["commands"],
  template: `
  <draggable  v-if="commands != null" v-model="commands.events" :options="availableCommandOptions" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="element in commands.events" :key="element.order">
            {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
        sort: false,
      },
    };
  },

  methods: {
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("movementCommands", {
  props: ["commands"],
  template: `
  <draggable v-if="commands != null" v-model="commands.movement" :options="availableCommandOptions" ghostClass="display-none" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="element in commands.movement" :key="element.order">
            {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
        sort: false,
      },
    };
  },

  methods: {
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("sceneCommands", {
  props: ["commands"],
  template: `
  <draggable v-if="commands != null" v-model="commands.scene" :options="availableCommandOptions" ghostClass="display-none" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="element in commands.scene" :key="element.order">
            {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
        sort: false,
      },
    };
  },

  methods: {
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("movement", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Pohyb</v-expansion-panel-header>
        <v-expansion-panel-content>
          <movement-commands :commands="commands"></movement-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("condition", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Podmienky</v-expansion-panel-header>
        <v-expansion-panel-content>
          <condition-commands :commands="commands"></condition-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("scene", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Scéna</v-expansion-panel-header>
        <v-expansion-panel-content>
          <scene-commands :commands="commands"></scene-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("loop", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Cyklus</v-expansion-panel-header>
        <v-expansion-panel-content>
          <loop-commands :commands="commands"></loop-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("subprogram", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Podprogramy</v-expansion-panel-header>
        <v-expansion-panel-content>
          <subprogram-commands :commands="commands"></subprogram-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("mySubprograms", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Moje podprogramy</v-expansion-panel-header>
        <v-expansion-panel-content>
          <my-subprograms-commands :mySubprograms="mySubprograms"></my-subprograms-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
  data() {
    return {
      mySubprograms: COMMANDS_TYPE.mySubprograms,
    };
  },
});

Vue.component("variables", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Premenné</v-expansion-panel-header>
        <v-expansion-panel-content>
          <variables-commands :commands="commands"></variables-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("general", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Iné príkazy</v-expansion-panel-header>
        <v-expansion-panel-content>
          <general-commands :commands="commands"></general-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("events", {
  props: ["commands"],
  template: `
      <v-expansion-panel>
        <v-expansion-panel-header>Udalosti</v-expansion-panel-header>
        <v-expansion-panel-content>
          <events-commands :commands="commands"></events-commands>
        </v-expansion-panel-content>
      </v-expansion-panel>`,
});

Vue.component("generalCommands", {
  props: ["commands"],
  template: `
  <draggable  v-if="commands != null" v-model="commands.general" :options="availableCommandOptions" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="element in commands.general" :key="element.order">
            {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
      },
    };
  },

  methods: {
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("loopCommands", {
  props: ["commands"],
  template: `
  <draggable  v-if="commands != null" v-model="commands.loop" :options="availableCommandOptions" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="element in commands.loop" :key="element.order">
            {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
      },
    };
  },

  methods: {
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("subprogramCommands", {
  props: ["commands"],
  template: `
  <draggable  v-if="commands != null" v-model="commands.subprogram" :options="availableCommandOptions" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="element in commands.subprogram" :key="element.order">
            {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
      },
    };
  },

  methods: {
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("variablesCommands", {
  props: ["commands"],
  template: `
  <v-container>
  <draggable  v-if="commands != null" v-model="commands.variables" :options="availableCommandOptions" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="(element, key) in commands.variables" :key="key">
             {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>
    <new-variable-dialog></new-variable-dialog>
  </v-container>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
        sort: false,
      },
    };
  },

  methods: {
    addVariable() {
      this.$root.$refs.NewVariableDialog.dialog = true;
    },
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("mySubprogramsCommands", {
  props: ["mySubprograms"],
  template: `
  <v-col>
  <draggable  v-if="mySubprograms != null" v-model="mySubprograms" :options="availableCommandOptions" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="element in mySubprograms" :key="element.order">
             {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>
    <new-subprogram-dialog></new-subprogram-dialog>
  </v-col>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
        sort: false,
      },
    };
  },

  methods: {
    addMySubprogram() {
      this.$root.$refs.NewSubprogramDialog.dialog = true;
    },
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("conditionCommands", {
  props: ["commands"],
  template: `
  <draggable  v-if="commands != null" v-model="commands.condition" :options="availableCommandOptions" :clone="handleClone">
        <transition-group name="no" class="list-group" tag="ul">
          <li class="list-group-item py-2" v-for="element in commands.condition" :key="element.order">
            {{ element.representationName }}
          </li>
        </transition-group>
		</draggable>`,
  data() {
    return {
      availableCommandOptions: {
        ghostClass: "draggable-ghost",
        group: {
          name: "items",
          pull: "clone",
          put: false,
        },
        sort: false,
      },
    };
  },

  methods: {
    handleClone(item) {
      let cloneMe = JSON.parse(JSON.stringify(item));
      this.$delete(cloneMe, "uid");
      return cloneMe;
    },

    uuid(e) {
      if (e.uid) return e.uid;
      const key = Math.random().toString(16).slice(2);
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});
