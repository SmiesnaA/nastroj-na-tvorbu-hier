var globalId = 0;

var FabMenu = Vue.extend({
  template: `
    <div>
    <v-speed-dial
      v-model="fab"
      :top="true"
      :right="true"
      :direction="direction"
      :transition="transition"
    >
      <template v-slot:activator>
        <v-btn
          v-model="fab"
          color="#222B56"
          dark
          small
          fab
        >
          <v-icon v-if="fab">
            mdi-close
          </v-icon>
          <v-icon v-else>
            mdi-dots-horizontal
          </v-icon>
        </v-btn>
      </template>
      <v-tooltip left>
      <template v-slot:activator="{ on, attrs }">
        <v-btn
          v-if="!saved"
          fab
          dark
          small
          color="#00695C"
          @click="deleteCanvas"
          v-bind="attrs"
          v-on="on"
        >
          <v-icon>mdi-delete</v-icon>
        </v-btn>
      </template>
      <span>Vymaž scénu</span>
      </v-tooltip>
      <v-tooltip left>
      <template v-slot:activator="{ on, attrs }">
        <v-btn
          fab
          dark
          small
          color="#3949AB"
          @click="downloadDialog = true"
          v-bind="attrs"
          v-on="on"
        >
          <v-icon>mdi-download</v-icon>
        </v-btn>
      </template>
      <span>Exportuj hru</span>
      </v-tooltip>
      <v-tooltip left>
      <template v-slot:activator="{ on, attrs }">
        <v-btn
          fab
          dark
          small
          color="#FF5252"
          :loading="isSelecting" 
          @click.stop="handleFileImport"
          v-bind="attrs"
          v-on="on"
        >
          <v-icon>mdi-file-upload</v-icon>
          <input 
          ref="uploader" 
          class="d-none" 
          type="file" 
          @change="onFileChanged"
      >
        </v-btn>
      </template>
      <span>Importuj hru</span>
      </v-tooltip>
      <v-tooltip left>
      <template v-slot:activator="{ on, attrs }">
        <v-btn
        fab
        dark
        small
        v-if="!saved"
        color="#C0CA33"
        @click="showGrid"
        v-bind="attrs"
        v-on="on"
      >
        <v-icon v-if="grid">mdi-grid</v-icon>
        <v-icon v-if="!grid">mdi-grid-off</v-icon>
      </v-btn>
    </template>
    <span>Skry/zobraz mriežku</span>
    </v-tooltip>
    </v-speed-dial>
    <download-dialog @dialogChange="handleDownloadDialog" @downloadFile="download" :dialog="downloadDialog"></download-dialog>
    </div>`,
  data: () => ({
    grid: true,
    direction: "bottom",
    fab: false,
    fling: false,
    hover: true,
    tabs: null,
    downloadDialog: false,
    transition: "slide-y-reverse-transition",
    isSelecting: false,
    selectedFile: null,
  }),
  methods: {
    handleFileImport() {
      this.isSelecting = true;

      window.addEventListener(
        "focus",
        () => {
          this.isSelecting = false;
        },
        { once: true }
      );
      this.$refs.uploader.click();
    },
    onFileChanged(e) {
      this.selectedFile = e.target.files[0];
      console.log(
        "selected file",
        this.selectedFile,
        this.selectedFile.constructor.name
      );
      this.$root.$refs.Game.uploadGame(this.selectedFile);
      this.fab = false;
    },
    showGrid() {
      this.grid = !this.grid;
      gameScene.showGrid();
    },
    download(name, fileName) {
      this.downloadDialog = false;
      this.$root.$refs.Game.exportGame(name, fileName);
    },
    deleteCanvas() {
      gameScene.getCurrLevel().scene.deleteScene();
    },
    handleDownloadDialog() {
      this.downloadDialog = false;
    },
  },
  created() {
    this.$root.$refs.FabMenu = this;
  },
  computed: {
    saved() {
      return this.$root.$refs.Game.saved;
    },
  },
});

Vue.component("addSoundBtn", {
  template: `
    <v-tooltip right>
    <template v-slot:activator="{ on, attrs }">
      <v-btn
      fab
      dark
      small
      v-bind="attrs"
      v-on="on"
      color="primary"
      :loading="isSelecting" 
      @click.stop="handleFileImport"
      >
      <v-icon>mdi-plus</v-icon>
      <input 
      ref="soundUploader" 
      class="d-none" 
      type="file" 
      @change="onFileChanged"
      >
      </v-btn>
    </template>
    <span>Pridaj zvuk</span>
    </v-tooltip>
  `,
  data() {
    return {
      isSelecting: false,
      selectedFile: null,
    };
  },
  methods: {
    onFileChanged(e) {
      this.selectedFile = e.target.files[0];
      console.log(
        "selected file",
        this.selectedFile,
        this.selectedFile.constructor.name
      );
      let reader = new FileReader();
      reader.onload = () => {
        const dataURL = reader.result;
        this.$emit("uploadsound", {
          file: dataURL,
          name: this.selectedFile.name,
        });
      };
      reader.readAsDataURL(this.selectedFile);
      this.fab = false;
    },
    handleFileImport() {
      this.isSelecting = true;

      window.addEventListener(
        "focus",
        () => {
          this.isSelecting = false;
        },
        { once: true }
      );
      this.$refs.soundUploader.click();
    },
  },
});

Vue.component("addImageBtn", {
  template: `
    <v-tooltip right>
    <template v-slot:activator="{ on, attrs }">
      <v-btn
      fab
      class="mt-n9"
      dark
      small
      v-bind="attrs"
      v-on="on"
      color="primary"
      :loading="isSelecting" 
      @click.stop="handleFileImport"
      >
      <v-icon>mdi-plus</v-icon>
      <input 
      ref="imgUploader" 
      class="d-none" 
      type="file" 
      @change="onFileChanged"
      >
      </v-btn>
    </template>
    <span>Pridaj obrázok</span>
    </v-tooltip>
  `,
  data() {
    return {
      isSelecting: false,
      selectedFile: null,
    };
  },
  methods: {
    onFileChanged(e) {
      this.selectedFile = e.target.files[0];
      console.log(
        "selected file",
        this.selectedFile,
        this.selectedFile.constructor.name
      );
      let reader = new FileReader();
      reader.onload = () => {
        const dataURL = reader.result;
        this.$emit("uploadimg", {
          file: dataURL,
          name: this.selectedFile.name,
        });
      };
      reader.readAsDataURL(this.selectedFile);

      this.fab = false;
    },
    handleFileImport() {
      this.isSelecting = true;

      window.addEventListener(
        "focus",
        () => {
          this.isSelecting = false;
        },
        { once: true }
      );
      this.$refs.imgUploader.click();
    },
  },
});

Vue.component("fabMenu", { FabMenu });

Vue.component("stepper", {
  components: {
    FabMenu,
  },
  created() {
    gameScene.getCurrLevel().scene.init();
    gameScene.getCurrLevel().scene.initInteract();
  },
  data() {
    return {
      componentKey: 0,
      currentStep: 1,
      step: 1,
      show: true,
      steps: 6,
      nameSteps: [
        "Veľkosť scény",
        "Steny",
        "Predmety",
        "Hráč",
        "Postavy",
        "Zvuky",
      ],
    };
  },
  watch: {
    steps(val) {
      if (this.currentStep > val) {
        this.currentStep = val;
      }
    },
  },
  computed: {
    saved() {
      return this.$root.$refs.Game.saved;
    },
  },

  methods: {
    nextStep(n) {
      if (n === this.steps) {
        this.currentStep = 1;
      } else {
        this.currentStep = n + 1;
      }
    },
    backStep(n) {
      if (n - 1 !== 0) {
        this.currentStep = n - 1;
      }
    },
    save() {
      this.$root.$refs.Game.save();
    },
    editScene() {
      this.$root.$refs.Game.edit();
    },
  },
});

const path = window.location.href.substring(
  0,
  window.location.href.lastIndexOf("/")
);

const categories = ["simple", "winter", "desert", "graveyard"];

Vue.component("cardStepper2", {
  data() {
    return {
      selectedFile: null,
      availableItems: [].concat.apply(
        [],
        categories.map((category) => {
          if (category == "simple") {
            return [...Array(19)].map((_, i) => {
              return {
                src: assets[`tiles_${category}`][`${i + 1}.png`],
                name: `${i + 1}.png`,
              };
            });
          }
          return [...Array(9)].map((_, i) => {
            return {
              src: assets[`tiles_${category}`][`${i + 1}.png`],
              name: `${i + 1}.png`,
            };
          });
        })
      ),
    };
  },
  computed: {
    componentKey() {
      return this.availableItems.length;
    },
  },
  methods: {
    uploadImage(data) {
      this.availableItems.push({ src: data.file, name: data.name });
      this.componentKey++;
    },
  },
});
Vue.component("cardStepper3", {
  data() {
    return {
      componentKey: 0,
      selectedFile: null,
      availableItems: [].concat.apply(
        [],
        categories.map((category) => {
          if (category == "simple") {
            return [...Array(17)].map((_, i) => {
              return {
                src: assets[`objects_${category}`][`${i + 1}.png`],
                name: `${i + 1}.png`,
              };
            });
          }
          return [...Array(5)].map((_, i) => {
            return {
              src: assets[`objects_${category}`][`${i + 1}.png`],
              name: `${i + 1}.png`,
            };
          });
        })
      ),
    };
  },
  methods: {
    uploadImage(file) {
      this.availableItems.push({ src: file });
      this.componentKey++;
    },
  },
});
Vue.component("cardStepper4", {
  data() {
    return {
      availableItems: [
        {
          src: assets["characters"]["pacmanSheets_main.png"],
          name: "pacmanSheets_main.png",
        },
        {
          src: assets["characters"]["guy_main.png"],
          name: "guy_main.png",
        },
        {
          src: assets["characters"]["boulderDash_main.png"],
          name: "boulderDash_main.png",
        },
        {
          src: assets["characters"]["elf_main.png"],
          name: "elf_main.png",
        },
        {
          src: assets["characters"]["lion_main.png"],
          name: "lion_main.png",
        },
      ],
    };
  },
  methods: {
    animate(event, item) {
      const filename = item.name.split(".").slice(0, -1).join(".");
      event.currentTarget.src = "assets/characters/" + filename + ".gif";
    },
    stopAnimate(event, item) {
      event.currentTarget.src = item.src;
    },
  },
});
Vue.component("cardStepper5", {
  data() {
    return {
      availableItems: [
        {
          src: assets["characters"]["blinkySheet_main.png"],
          name: "blinkySheet_main.png",
        },
        {
          src: assets["characters"]["pinkBlinkySheet_main.png"],
          name: "pinkBlinkySheet_main.png",
        },
        {
          src: assets["characters"]["blueBlinkySheet_main.png"],
          name: "blueBlinkySheet_main.png",
        },
      ],
    };
  },
  methods: {
    animate(event, item) {
      const filename = item.name.split(".").slice(0, -1).join(".");
      event.currentTarget.src = "assets/characters/" + filename + ".gif";
    },
    stopAnimate(event, item) {
      event.currentTarget.src = item.src;
    },
  },
});

Vue.component("player", {
  props: ["file", "name"],
  template: `
    <v-chip class="ma-1">
      <h5>{{ name }}</h5>
        <v-btn small icon class="ma-0"  @click.native="playing ? pause() : play()" :disabled="!loaded">
            <v-icon v-if="!playing || paused">mdi-play</v-icon>
            <v-icon v-else>mdi-pause</v-icon>
        </v-btn>
        <v-btn small icon class="ma-0" @click.native="stop()" :disabled="!loaded">
            <v-icon>mdi-stop</v-icon>
        </v-btn>
      <audio id="player" ref="player" :src="file"></audio>
  </v-chip>`,
  computed: {
    duration: function () {
      return this.audio ? this.formatTime(this.totalDuration) : "";
    },
  },
  data() {
    return {
      firstPlay: true,
      loaded: false,
      playing: false,
      paused: false,
      percentage: 0,
      currentTime: "00:00:00",
      audio: undefined,
      totalDuration: 0,
    };
  },
  methods: {
    setPosition() {
      this.audio.currentTime = parseInt(
        (this.audio.duration / 100) * this.percentage
      );
    },
    stop() {
      this.audio.pause();
      this.paused = true;
      this.playing = false;
      this.audio.currentTime = 0;
    },
    play() {
      if (this.playing) return;
      this.audio.play().then((_) => (this.playing = true));
      this.paused = false;
    },
    pause() {
      this.paused = !this.paused;
      this.paused ? this.audio.pause() : this.audio.play();
    },
    _handleLoaded: function () {
      if (this.audio.readyState >= 2) {
        if (this.audio.duration === Infinity) {
          this.audio.currentTime = 1e101;
          this.audio.ontimeupdate = () => {
            this.audio.ontimeupdate = () => {};
            this.audio.currentTime = 0;
            this.totalDuration = parseInt(this.audio.duration);
            this.loaded = true;
          };
        } else {
          this.totalDuration = parseInt(this.audio.duration);
          this.loaded = true;
        }
        if (this.autoPlay) this.audio.play();
      } else {
        throw new Error("Failed to load sound file");
      }
    },
    formatTime: function (second) {
      return new Date(second * 1000).toISOString().substr(11, 8);
    },
    _handlePlayingUI: function (e) {
      this.percentage = (this.audio.currentTime / this.audio.duration) * 100;
      this.currentTime = this.formatTime(this.audio.currentTime);
      this.playing = true;
    },
    _handlePlayPause: function (e) {
      if (e.type === "play" && this.firstPlay) {
        this.audio.currentTime = 0;
        if (this.firstPlay) {
          this.firstPlay = false;
        }
      }
      if (
        e.type === "pause" &&
        this.paused === false &&
        this.playing === false
      ) {
        this.currentTime = "00:00:00";
      }
    },
    _handleEnded() {
      this.paused = this.playing = false;
    },
    init: function () {
      this.audio.addEventListener("timeupdate", this._handlePlayingUI);
      this.audio.addEventListener("loadeddata", this._handleLoaded);
      this.audio.addEventListener("pause", this._handlePlayPause);
      this.audio.addEventListener("play", this._handlePlayPause);
      this.audio.addEventListener("ended", this._handleEnded);
    },
  },
  mounted() {
    this.audio = this.$refs.player;
    this.init();
  },
  beforeDestroy() {
    this.audio.removeEventListener("timeupdate", this._handlePlayingUI);
    this.audio.removeEventListener("loadeddata", this._handleLoaded);
    this.audio.removeEventListener("pause", this._handlePlayPause);
    this.audio.removeEventListener("play", this._handlePlayPause);
    this.audio.removeEventListener("ended", this._handleEnded);
  },
});

Vue.component("cardStepper6", {
  data() {
    return {
      sounds: Object.keys(assets["sounds"]).map((key) => {
        const sound = { file: assets["sounds"][key], name: key };
        gameScene.getCurrLevel().addSound(sound);
        return sound;
      }),
      componentKey: 0,
    };
  },
  methods: {
    uploadSound(data) {
      const sound = { file: data.file, name: data.name };
      this.sounds.push(sound);
      gameScene.getCurrLevel().addSound(sound);
      this.componentKey++;
    },
  },
});

Vue.component("cardStepper1", {
  created() {
    this.$root.$refs.sceneProps = this;
  },
  data() {
    return {
      rows: gameScene.getCurrLevel().scene.rows,
      cols: gameScene.getCurrLevel().scene.cols,
      cellSize: gameScene.getCurrLevel().scene.cellSize,
      canvasRules: [
        (v) => !!v || "Pole je povinné",
        (v) => (v && v >= 1) || "Minimálna hodnota je 1",
        (v) => (v && v <= 50) || "Maximálna hodnota je 50",
      ],
    };
  },
  methods: {
    incKey() {
      this.rows = gameScene.getCurrLevel().scene.rows;
      this.cols = gameScene.getCurrLevel().scene.cols;
      this.cellSize = gameScene.getCurrLevel().scene.cellSize;
    },
    setCanvas() {
      if (this.$refs.form.validate()) {
        gameScene
          .getCurrLevel()
          .scene.setCanvas(
            parseInt(this.rows),
            parseInt(this.cols),
            parseInt(this.cellSize)
          );
      }
    },
    set(rows, cols, cellSize) {
      this.rows = rows;
      this.cols = cols;
      this.cellSize = cellSize;
    },
  },
});

Vue.component("gridCanvas", {
  created() {
    this.$root.$refs.GridCanvas = this;
  },
  data() {
    return {
      numberOfColumns: 30,
      numberOfRows: 10,
      cellSize: 35,
    };
  },
  computed: {
    gridStyle() {
      return {
        gridTemplateColumns: `repeat(${this.numberOfColumns}, minmax(${this.cellSize}px, ${this.cellSize}px))`,
      };
    },
    sizeOfCell() {
      return `${this.cellSize}px`;
    },
    cards() {
      return [...Array(this.numberOfColumns * this.numberOfRows).keys()];
    },
  },
  methods: {
    setColumns(number) {
      this.numberOfColumns = number;
    },
    setRows(number) {
      this.numberOfRows = number;
    },
    setCell(number) {
      this.cellSize = number;
    },
    getColumns() {
      return this.numberOfColumns;
    },
    getRows() {
      return this.numberOfRows;
    },
    getCellSize() {
      return this.cellSize;
    },
    getscene() {
      var scene = [];
      var arr = [];
      let children = this.$children;
      children.forEach((el) => {
        if (el.getItems().length == 1) {
          arr.push(el.getItems()[0]);
        } else {
          arr.push({ name: "", src: "", uid: "" });
        }
      });

      for (var i = 0; i < arr.length; i += this.numberOfColumns) {
        scene.push(arr.slice(i, i + this.numberOfColumns));
      }
      return JSON.stringify(scene);
    },
  },
});

var game = Vue.component("game", {
  components: {
    FabMenu,
  },
  created() {
    window.onbeforeunload = function () {
      return true;
    };
    this.$root.$refs.Game = this;
  },
  watch: {
    currentLevel() {
      this.e1 = this.currentLevel.id;
      gameScene.currLevelID = this.currentLevel.id;
    },
    steps() {
      this.steps = gameScene.levels;
    },
  },
  data() {
    return {
      componentKey: 0,
      saved: false,
      steps: gameScene.levels,
      currentLevel: gameScene.getCurrLevel(),
      e1: gameScene.getCurrLevel().id,
    };
  },
  methods: {
    changeStep(n) {
      gameScene.currLevelID = this.steps[n - 1].id;
      app.$forceUpdate();
      if (!this.saved) {
        gameScene.getCurrLevel().scene.initInteract();
        gameScene.getCurrLevel().scene.init();
      } else {
        gameScene.getCurrLevel().initCodeEditor();
      }
      if (this.saved) {
        this.$root.$refs.GameObjects.$forceUpdate();
        this.$root.$refs.GameObjects.incKey();
      }
    },
    addStep() {
      if (this.steps.length < 3) {
        const l = new Level(this.steps.length + 1);
        this.steps.push(l);
      }
    },
    removeStep(n) {
      if (this.steps.length > 1) {
        gameScene.levels = gameScene.levels.filter((l) => l.id != this.e1);
        const levels = [];
        this.steps = gameScene.levels;
        for (let i = 0; i < gameScene.levels.length; i++) {
          let l = gameScene.levels[i];
          l.id = i + 1;
          levels.push(l);
        }
        gameScene.levels = levels;
        this.steps = gameScene.levels;
        this.currentLevel = this.steps[0];
        this.e1 = this.currentLevel.id;
        gameScene.currLevelID = this.currentLevel.id;
        gameScene.getCurrLevel().scene.init();
      }
    },
    uploadGame(file) {
      var reader = new FileReader();
      const self = this;
      reader.onload = function () {
        var text = reader.result;
        const parser = new DOMParser();
        const document = parser.parseFromString(text, "text/html");
        const levelsData = JSON.parse(
          document.getElementById("levelsData").textContent
        );
        gameScene.init(levelsData, self.$root.$refs.Game.saved);
        self.steps = gameScene.levels;
        const scene = gameScene.getCurrLevel().scene;
        self.setCanvas(scene.rows, scene.cols, scene.cellSize);
      };
      reader.readAsText(file);

      this.$root.key++;
    },
    setCanvas(rows, cols, cellSize) {
      this.$root.$refs.sceneProps.set(rows, cols, cellSize);
    },
    save() {
      this.saved = true;
    },
    edit() {
      this.saved = false;
      gameScene.getCurrLevel().scene.reset();
      gameScene.getCurrLevel().scene.init();
      //gameScene.getCurrLevel().scene.initInteract();
    },
    getSavedCanvas() {
      return this.canvas;
    },
    exportGame(name, fileName) {
      download(name, fileName);
    },
  },
});

Vue.component("savedCanvas", {
  created() {
    this.$root.$refs.SavedCanvas = this;
  },
});

Vue.component("trash", {
  template: `
    <draggable id="trash" v-model="items" :options="trashOptions"
      style="z-index: 0;height: 100%;">
    </draggable>
  `,
  data() {
    return {
      items: [],
      trashOptions: {
        draggable: "display-none",
        ghostClass: "display-none",
        group: "items",
        emptyInsertThreshold: "20",
        pull: false,
        put: true,
      },
    };
  },
});

Vue.component("nestedDraggable", {
  template: `
    <draggable :swapThreshold="0" tag="ul" :list="commands" :options="clonedCommandOptions" :class="{ emptyNested: commands.length == 0 }">
      <li v-for="(el, index) in commands" :key="uuid(el)">
        <condition-command @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type === 'condition'"></condition-command>
        <loop-command @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type === 'loop'"></loop-command>
        <loop-go-command @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type === 'loopGo'"></loop-go-command>
        <general-command @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type === 'general' || el.type == null || el.type == 'callMySubprogram'"></general-command>
        <define-subprogram-command @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type == 'mySubprograms'"></define-subprogram-command>
        <sound-command  @contextmenu.native="handler($event, el)" class="list-container" :el="el" v-if="el.type == 'sound'"></sound-command>
        </li>
    </draggable>`,
  props: {
    commands: {
      required: true,
      type: Array,
    },
  },
  created() {},
  data() {
    return {
      clonedCommandOptions: {
        ghostClass: "draggable-ghost",
        group: "items",
        sort: true,
      },
    };
  },
  methods: {
    handler: function (e, el) {
      e.stopPropagation();
      e.preventDefault();
      customMenu.show(e, el);
    },
    changeParameter(index, element) {
      Vue.set(
        element,
        "parameter",
        parseInt(document.getElementById("commandInput" + index).value)
      );
    },
    uuid(e) {
      if (e.uid) return e.uid;

      globalId += 1;
      const key = globalId;
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("startNestedDraggable", {
  template: `
        <v-container class="list-container" v-if="object != null">
            <li class="ma-2 start">Po kliknutí na štart</li>
            <draggable :swapThreshold="0" tag="ul" class="startCommands" :list="object.parsedCommands" :options="clonedCommandOptions" :key="control">
              <li v-for="(el, index) in object.parsedCommands" :key="uuid(el)">
                <condition-command class="list-container" :el="el" v-if="el.type === 'condition'"></condition-command>
                <loop-command class="list-container" :el="el" v-if="el.type === 'loop'"></loop-command>
                <general-command :el="el" v-if="el.type === 'general' || el.type == null || el.type == 'callMySubprogram'"></general-command>
                <define-subprogram-command  :el="el" v-if="el.type == 'mySubprograms'"></define-subprogram-command>
                <sound-command  :el="el" v-if="el.type == 'sound'"></sound-command>
                <level-command  :el="el" v-if="el.type == 'level'"></level-command>
              </li>
            </draggable>
          </li>
      </v-container>
     `,
  props: {
    object: {
      required: true,
    },
  },
  created() {},
  data() {
    return {
      control: 0,
      clonedCommandOptions: {
        ghostClass: "draggable-ghost",
        group: "items",
        sort: true,
        put: true,
        pull: false,
      },
    };
  },
  methods: {
    changeParameter(index, element) {
      Vue.set(
        element,
        "parameter",
        parseInt(document.getElementById("commandInput" + index).value)
      );
    },
    uuid(e) {
      if (e.uid) return e.uid;

      globalId += 1;
      const key = globalId;
      this.$set(e, "uid", key);
      return e.uid;
    },
  },
});

Vue.component("codeEditor", {
  components: {
    FabMenu,
  },
  data() {
    return {
      running: false,
    };
  },
  methods: {
    editScene() {
      this.$root.$refs.Game.edit();
    },
  },
  created() {
    this.$root.$refs.codeEditor = this;
    gameScene.getCurrLevel().initCodeEditor();
  },
  computed: {
    saved() {
      return this.$root.$refs.Game.saved;
    },
  },
});

Vue.component("sceneEditor", {
  data() {
    return {
      helpDialog: false,
    };
  },
  methods: {
    save() {
      this.$root.$refs.Game.save();
    },
    help() {
      this.helpDialog = true;
    },
  },
});

Vue.component("NewSubprogramDialog", {
  created() {
    this.$root.$refs.NewSubprogramDialog = this;
  },
  template: `
    <v-row justify="center">
      <v-dialog
        v-model="dialog"
        persistent
        max-width="600px"
      >
        <template v-slot:activator="{ on, attrs }">
        <v-btn  v-bind="attrs" v-on="on"  class="mx-2" small outlined fab color="indigo">
        <v-icon dark>
          mdi-plus
        </v-icon>
        </v-btn>
          
        </template>
        <v-card>
        <v-card-title>
        <span class="text-h5">Nový podprogram</span>
      </v-card-title>
      <v-card-text>
      <v-container>
        <v-col>
        <li class="list-container">
        <v-text-field class="ma-5"
        v-model="subprogramName"
        placeholder="Názov podprogramu"
        required
      ></v-text-field></li>
      </v-col>
      </v-container>
         
          </v-card-text>
          <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn
              color="blue darken-1"
              text
              @click="dialog = false"
            >
              Zatvoriť
            </v-btn>
            <v-btn
              color="blue darken-1"
              text
              @click="save"
            >
              Uložiť
            </v-btn>
          </v-card-actions>
        </v-card>
      </v-dialog>
    </v-row>`,
  data: () => ({
    dialog: false,
    subprogramName: "",
  }),
  methods: {
    save() {
      this.dialog = false;
      this.$root.$refs.TabExpansionPanel.saveMySubprogram(this.subprogramName);
    },
  },
});

Vue.component("NewVariableDialog", {
  created() {
    this.$root.$refs.NewSubprogramDialog = this;
  },
  template: `
    <v-row justify="center">
      <v-dialog
        v-model="dialog"
        persistent
        max-width="600px"
      >
        <template v-slot:activator="{ on, attrs }">
        <v-btn  v-bind="attrs" v-on="on"  class="mx-2" small outlined fab color="indigo">
        <v-icon dark>
          mdi-plus
        </v-icon>
        </v-btn>
          
        </template>
        <v-card>
        <v-card-title>
        <span class="text-h5">Nová premenná</span>
      </v-card-title>
      <v-card-text>
      <v-container>
        <v-col>
        <li class="list-container">
        <v-text-field class="ma-5"
        v-model="variableName"
        placeholder="Názov premennej"
        required
      ></v-text-field></li>
      </v-col>
      </v-container>
         
          </v-card-text>
          <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn
              color="blue darken-1"
              text
              @click="dialog = false"
            >
              Zatvoriť
            </v-btn>
            <v-btn
              color="blue darken-1"
              text
              @click="save"
            >
              Uložiť
            </v-btn>
          </v-card-actions>
        </v-card>
      </v-dialog>
    </v-row>`,
  data: () => ({
    dialog: false,
    variableName: "",
  }),
  created() {
    this.variableName = "";
  },
  methods: {
    save() {
      this.dialog = false;
      this.$root.$refs.TabExpansionPanel.saveMyVariable(this.variableName);
    },
  },
});

Vue.component("CookieDialog", {
  template: `
  <div class="text-center">
    <v-dialog
      v-model="open"
      width="500"
    >
      <v-card>
      <v-toolbar
        color="primary"
        dark
      >Pomocník</v-toolbar>
        <v-card-text>
          <v-col>
            <h1>Vitaj</h1>
            <v-spacer></v-spacer>
            <div class="text-h2 pa-6">
              Vytvor si hru podľa vlastných predstáv. <br>
              Veľkosť scény, počet riadkov, počet stĺpcov a veľkosť štvorca môžeš nastaviť v lište na ľavej časti obrazovky v prvom kroku.
              Vybraním obrázka zo spodnej lišty a kliknutím na políčko v scéne ho na dané políčko umiestniš. 
              Ak chceš na scénu umiesniť viac rovnakých obrázkov, ťahaním ponad scénu sa obrázok vloží na políčko.
              Ak chceš zvoliť vlastný obrázok z počítača, kliknutím na tlačidlo s ikonou + ho môžeš pridať do ponuky obrázkov.
              Ak by si mal/a problém pri tvorení, vždy môžeš použiť nápovedu.
              <v-icon
                small
                color="orange darken-2"
              >
                mdi-help-circle 
              </v-icon>
              na obrazovke. 
            </div>
            <div>
            Vpravo na obrazovke sa nachádza menu  
              <v-icon small color="primary" dark>
                mdi-dots-horizontal
              </v-icon>
            Kliknutím naň sa ti zobrazia možnosti, ktoré môžeš použiť. Môžeš zmazať scénu, importovať alebo exportovať hru, či zobraziť alebo skryť mriežku. 
            </div>
          </v-col>

        </v-card-text>

        <v-divider></v-divider>

        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn
            color="primary"
            text
            @click="handleClick()"
          >
            Začni tvoriť
          </v-btn>
        </v-card-actions>
      </v-card>
    </v-dialog>
  </div>
  `,
  data() {
    return {
      open: this.isOpened(),
    };
  },
  methods: {
    isOpened() {
      return getLocalItem("welcomeDialog") == null;
    },
    handleClick() {
      this.open = false;
      setLocalItem("welcomeDialog", true);
    },
  },
});

Vue.component("EditorHelpDialog", {
  props: ["step"],
  template: `
  <v-dialog
    v-model="dialog"
    width="500"
  >
    <template v-slot:activator="{ on, attrs }">
    <v-col cols="1" id="helpDialog" class="my-1 mx-n1">
        <v-icon
        @click="dialog = true"
        large
        color="orange darken-2"
        >
        mdi-help-circle 
        </v-icon>
      </v-col>
    </template>

    <v-card>
    <v-toolbar
        color="primary"
        dark
      >Pomocník</v-toolbar>

      <v-card-text>
        <v-col>
            <h3>Potrebuješ pomôcť?</h3>
            <v-spacer></v-spacer>
            <div class="text-h2 pa-6">
              {{ texts[step - 1] }}
            </div>
          </v-col>
      </v-card-text>

      <v-divider></v-divider>

      <v-card-actions>
        <v-spacer></v-spacer>
        <v-btn
          color="primary"
          text
          @click="dialog = false"
        >
         Ok
        </v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
  `,
  data() {
    return {
      dialog: false,
      texts: [
        "Nastav veľkosť scény, počet riadkov, počet stĺpcov a veľkosť štvorca v lište na spodnej časti obrazovky.",
        `Vybraním obrázka zo spodnej lišty a kliknutím na políčko v scéne ho na dané políčko umiestniš. 
        Ak chceš na scénu umiesniť viac rovnakých obrázkov, ťahaním ponad scénu sa vloží na políčko.
        Ak chceš zvoliť vlastný obrázok z počítača, kliknutím na pole s fotoaparátom ho môžeš pridať do ponuky obrázkov.`,
        `Vybraním obrázka zo spodnej lišty a kliknutím na políčko v scéne ho na dané políčko umiestniš. 
        Ak chceš na scénu umiesniť viac rovnakých obrázkov, ťahaním ponad scénu sa vloží na políčko.
        Ak chceš zvoliť vlastný obrázok z počítača, kliknutím na pole s fotoaparátom ho môžeš pridať do ponuky obrázkov.`,
        `Vybraním obrázka zo spodnej lišty a kliknutím na políčko v scéne ho na dané políčko umiestniš.`,
        `Vybraním obrázka zo spodnej lišty a kliknutím na políčko v scéne ho na dané políčko umiestniš. 
        Ak chceš na scénu umiesniť viac rovnakých obrázkov, ťahaním ponad scénu sa vloží na políčko.`,
        `Kliknutím na ikonu zvuku pridáš vlastný zvuk do scény.`,
        `Naprogramuj správanie objektov potiahnutím príkazov a udalostí z vysúvacieho panelu do programovacej časti. Pravým kliknutím myši na príkaz sa zobrazí 
        kontextové menu, v ktorom môžeš vybrať možnosť skopírovať a vložiť vybraný príkaz.`,
      ],
    };
  },
});

Vue.component("DownloadDialog", {
  props: ["dialog"],
  created() {
    this.$root.$refs.DownloadDialog = this;
  },
  template: `
    <v-row justify="center">
      <v-dialog
        v-model="dialog"
        persistent
        max-width="300px"
      >
        <v-card>
        <v-card-title>
        <span class="text-h5">Uložiť hru</span>
      </v-card-title>
      <v-col><v-col cols="12">
      <v-text-field
      label="Názov hry"
      v-model="name"
      placeholder="Hra"
      required
      ></v-text-field></v-col>
      <v-col cols="12">
      <v-text-field
      label="Názov súboru"
      v-model="fileName"
      placeholder="hra"
      suffix=".html"
      required
      ></v-text-field>
      </v-col></v-col>
         
          <v-card-actions>
            <v-spacer></v-spacer>
            <v-btn
              color="blue darken-1"
              text
              @click="close"
            >
              Zatvoriť
            </v-btn>
            <v-btn
              color="blue darken-1"
              text
              @click="download"
            >
              Uložiť
            </v-btn>
          </v-card-actions>
        </v-card>
      </v-dialog>
    </v-row>`,
  data: () => ({
    name: "Hra",
    fileName: "hra",
  }),
  methods: {
    download() {
      this.$emit("downloadFile", this.name, this.fileName);
    },
    close() {
      this.$emit("dialogChange", false);
    },
  },
});

Vue.component("addLevelMenu", {
  template: `
  <v-btn
      @click="handleClick"
      class="my-5"
      fab
      dark
      x-small
      dark
      color="teal"
    >
      <v-icon dark>
        mdi-plus
      </v-icon>
    </v-btn>   `,
  data: () => ({}),
  methods: {
    handleClick(event) {
      this.$emit("clicked", "someValue");
    },
  },
});

Vue.component("deleteLevelMenu", {
  template: `
  <v-btn
      @click="handleClick"
      class="my-5"
      fab
      dark
      x-small
      dark
      color="teal"
    >
      <v-icon dark>
        mdi-minus
      </v-icon>
    </v-btn>   `,
  data: () => ({}),
  methods: {
    handleClick(event) {
      this.$emit("clicked", "someValue");
    },
  },
});

Vue.component("LevelStepper", {
  created() {
    this.$root.$refs.LevelStepper = this;
  },
  template: `
  <v-container>
    <v-stepper v-model="e1">
      <v-stepper-header>
      <v-row>
      <v-col cols="9">
        <template v-for="n in steps">
          <v-stepper-step
            :complete="e1 > n"
            :step="n"
            editable
            dark
            color="teal"
          >
            Level {{ n }}
            
          </v-stepper-step>
          <v-divider
            v-if="n !== steps"
          ></v-divider>
        </template>
        </v-col>
        <v-col justify="end" cols="3">
          <add-level-menu @clicked="addStep"></add-level-menu>
          <delete-level-menu  @clicked="removeStep"></delete-level-menu>
        </v-col>
        </v-row>
      </v-stepper-header>

      
      </v-stepper-items>
    </v-stepper>
  </v-container>`,
});

Vue.component("objectInfo", {
  props: ["object"],
  template: `<v-col v-if="object != null">
  <h4>{{ objectInfo }}</h4><br>
  <div v-if="objectType == 'item'">
  <label for="checkbox">Priechodný:  </label>
  <input id="checkbox" :checked="object.passible" @click="checkboxUpdated" type="checkbox"></div>
  </v-col>`,
  data() {
    return {
      incKey: 0,
      passible: this.object && this.object.passible,
    };
  },
  computed: {
    objectType() {
      return this.object.type;
    },
    objectInfo() {
      return "Typ objektu: " + SLOVAK_OBJECT_TYPES[this.object.type];
    },
  },
  methods: {
    checkboxUpdated() {
      if (this.object.passible) {
        this.passible = false;
        this.object.setPassible(false);
      } else {
        this.passible = true;
        this.object.setPassible(true);
      }
    },
  },
});

Vue.component("gameObjects", {
  created() {
    this.$root.$refs.GameObjects = this;
  },
  template: `
  <v-card height="100%" elevation="8" class="scrollable-card" :key="componentKey">
  <v-card-title>Programovanie objektov</v-card-title>
  <v-card-subtitle>Zvoľ objekt, ktorý chceš programovať</v-card-subtitle>
  <v-divider class="mx-4"></v-divider>
  <object-info :object="object"></object-info>
  <v-card-text class="pa-0">
  <div class="container">
  <v-btn-toggle v-model="toggled"
  :key="componentKey"
  v-on:change="setObjectProgrammed(toggled)"
  tile
  mandatory
  color="secondary"
  group>
  <v-col v-for="column in columns" class="pa-1">
    <div v-for="item in column" class="pa-1">
    <v-btn
    elevation="2"
    :value="item.id"
  >
      <img
        class="object"
        :src="item.showSrc"
      >
        <template v-slot:placeholder>
          <v-row
            class="ma-0"
            align="center"
            justify="center"
          >
            <v-progress-circular
              indeterminate
              color="grey lighten-5"
            ></v-progress-circular>
          </v-row>
        </template>
      </img>
      </v-btn>
      </div>
    </v-col>
  </v-btn-toggle>
  </div>
  </v-card-text>
</v-card>`,
  data() {
    return {
      object: gameScene.getCurrLevel().objectProgrammed
        ? gameScene.getCurrLevel().objectProgrammed
        : null,
      componentKey: 0,
      toggled: gameScene.getCurrLevel().objectProgrammed
        ? gameScene.getCurrLevel().objectProgrammed.id
        : 1,
      cols: 4,
      objects: gameScene.getCurrLevel().programmableObjects,
    };
  },
  computed: {
    columns() {
      let columns = [];
      let mid = Math.ceil(this.objects.length / this.cols);
      for (let col = 0; col < this.cols; col++) {
        columns.push(this.objects.slice(col * mid, col * mid + mid));
      }
      return columns;
    },
  },
  methods: {
    incKey() {
      const objects = gameScene.getCurrLevel().programmableObjects;
      this.objects.forEach((_, index) => {
        Vue.set(this.objects, index, objects[index]);
      });
      this.toggled = gameScene.getCurrLevel().objectProgrammed.id;
    },
    setObjectProgrammed(toggled) {
      gameScene.setObjectProgrammed(toggled);
      this.object = gameScene.getCurrLevel().objectProgrammed;
      this.$root.$refs.TabObject.object = this.object;
    },
  },
});

var canvasTools = Vue.component("canvasTools", {
  created() {
    this.$root.$refs.CanvasTools = this;
  },
  template: `
  <v-col class="pa-0">
  <v-btn-toggle id="cursor" v-model="cursorChosen" mandatory>
    <v-col>
    <v-tooltip right>
      <template v-slot:activator="{ on, attrs }">
        <v-btn
          color="secondary"
          small
          dark
          @click="cursor"
          v-bind="attrs"
          v-on="on"
        >
        <v-icon>mdi-cursor-default</v-icon>
        </v-btn>
      </template>
      <span>Kurzor</span>
    </v-tooltip>
    <v-tooltip right>
      <template v-slot:activator="{ on, attrs }">
        <v-btn
          color="secondary"
          small
          @click="pan"
          v-bind="attrs"
          v-on="on"
        >
        <v-icon>mdi-cursor-move</v-icon>
        </v-btn>
      </template>
      <span>Pohyb</span>
    </v-tooltip>
    <v-tooltip right>
      <template v-slot:activator="{ on, attrs }">
        <v-btn
          color="secondary"
          small
          @click="erase"
          v-bind="attrs"
          v-on="on"
        >
        <v-icon>mdi-eraser</v-icon>
        </v-btn>
      </template>
      <span>Guma</span>
    </v-tooltip>
    </v-col>
    </v-btn-toggle>
    <v-tooltip right>
    <template v-slot:activator="{ on, attrs }">
      <v-btn
        color="secondary"
        small
        dark
        @click="zoomIn"
        v-bind="attrs"
        v-on="on"
      >
      <v-icon>mdi-magnify-plus-outline</v-icon>
      </v-btn>
    </template>
    <span>Priblížiť</span>
      </v-tooltip>
      <v-tooltip right>
      <template v-slot:activator="{ on, attrs }">
        <v-btn
          color="secondary"
          small
          @click="zoomOut"
          v-bind="attrs"
          v-on="on"
        >
        <v-icon>mdi-magnify-minus-outline</v-icon>
        </v-btn>
      </template>
      <span>Oddialiť</span>
    </v-tooltip></v-col>
  `,
  data() {
    return {
      cursorChosen: this.cursorChosen,
    };
  },
  methods: {
    zoomIn() {
      zoom(1);
    },
    zoomOut() {
      zoom(-1);
    },
    changeCursor(cursor) {
      this.cursorChosen = cursor;
    },
    pan() {
      this.$setCursorChosen(CURSOR_TYPES.MOVE);
      document.getElementsByTagName("canvas")[0].style.cursor = "move";
    },
    cursor() {
      this.$setCursorChosen(CURSOR_TYPES.POINTER);
      document.getElementsByTagName("canvas")[0].style.cursor = "pointer";
    },
    erase() {
      this.$setCursorChosen(CURSOR_TYPES.ERASER);
    },
  },
});

Vue.component("codeEditorCanvas", {
  template: `
  <v-row justify="end">
    <v-dialog
      v-model="dialog"
      persistent
    >
      <template v-slot:activator="{ on, attrs }">
          <v-btn
            id="executeBtn"
            v-bind="attrs"
            v-on="on"
            small
            @click="start"
            color="green lighten-2"
          >
            Spustiť hru
            </v-btn>
       
      </template>
        <v-card>
        <v-card-text>
        <v-row id="codeScene"></v-row>
        </v-card-text>
        <v-card-actions>
          <v-spacer></v-spacer>
          <v-btn
          id="startBtn"
          :disabled="running"
          @click="execute"
          color="green darken-2"
        >
          Štart
         </v-btn>
          <v-btn
          id="stopBtn"
          :disabled="!running"
          @click="stop"
          color="green darken-2"
        >
          Stop
         </v-btn>
          <v-btn
          id="resetBtn"
          :disabled="running"
          @click="reset"
          color="green darken-5"
        >
          Reset
         </v-btn>
          <v-btn
            color="green darken-1"
            text
            @click="close"
          >
            Zatvoriť
          </v-btn>
        </v-card-actions>
        </v-card>
    </v-dialog>
  </v-row>`,
  data() {
    return {
      dialog: false,
      running: false,
    };
  },
  methods: {
    close() {
      this.dialog = false;
      this.running = false;
      stopScene();
      resetScene();
    },
    start() {
      this.dialog = true;
      gameScene.createCodeScene();
    },
    execute() {
      this.running = true;
      executeCode();
    },
    reset() {
      this.running = false;
      resetScene();
    },
    stop() {
      this.running = false;
      stopScene();
    },
  },
  computed: {
    saved() {
      return this.$root.$refs.Game.saved;
    },
  },
});

Vue.component("landingPage", {
  template: `
  <div class="fillHeight">
  <v-app-bar
      id="appBar"
      color="primary"
      dense
      dark
    >
    <v-toolbar-title>Nástroj na tvorbu hier</v-toolbar-title>
    </v-app-bar>
  <v-parallax
    dark
    height="600"
  >
    <v-row
      align="center"
      justify="center"
    >
      <v-col
        class="text-center"
        cols="12"
      >
        <h1 class="text-h4 mb-4">
          Vytváraj hry a zabávaj sa.
        </h1>
        <v-btn
          class="my-2"
          depressed
          color="primary"
          @click="openGameEditor"
        >
          Začni tvoriť
        </v-btn>
      </v-col>
    </v-row>
  </v-parallax></div>`,
  methods: {
    openGameEditor() {
      this.$root.gameEditor = true;
    },
  },
});

Vue.prototype.$setCursorChosen = function (chosen) {
  cursorChosen = chosen;
};

Vue.prototype.$cursorChosen = cursorChosen;

const vuetify = new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#3f51b5",
        secondary: "#c5d2e7",
        accent: "#8c9eff",
        error: "#b71c1c",
      },
    },
  },
});

app = new Vue({
  el: "#app",
  vuetify: vuetify,
  data() {
    return {
      gameEditor: false,
      key: 0,
    };
  },
  methods: {
    changeCursor(cursor) {
      this.$refs.CanvasTools.changeCursor(cursor);
    },
  },
});

var customMenu = new ContextMenu(".list-container", {
  className: "ContextMenu.theme-custom",
});
