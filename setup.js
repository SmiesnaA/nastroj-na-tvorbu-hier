const OBJECTS = {
  NOTHING: "nothing",
  PLAYER: "player",
  ENEMY: "enemy",
  WALL: "wall",
  ITEM: "item",
};

const OBJECTS_NAMES = {
  NOTHING: "prázdne",
  PLAYER: "hráč",
  ENEMY: "postava",
  WALL: "stena",
  ITEM: "predmet",
};

const SLOVAK_OBJECT_TYPES = {
  [OBJECTS.NOTHING]: "prázdne",
  [OBJECTS.PLAYER]: "hráč",
  [OBJECTS.ENEMY]: "postava",
  [OBJECTS.WALL]: "stena",
  [OBJECTS.ITEM]: "predmet",
};

const OBJECT_TYPES = {
  [OBJECTS_NAMES.NOTHING]: "nothing",
  [OBJECTS_NAMES.PLAYER]: "player",
  [OBJECTS_NAMES.ENEMY]: "enemy",
  [OBJECTS_NAMES.WALL]: "wall",
  [OBJECTS_NAMES.ITEM]: "item",
};

const DIRECTIONS = {
  FACING_RIGHT: 0,
  FACING_UP: 1,
  FACING_LEFT: 2,
  FACING_DOWN: 3,
};

const PROGRAMMABLE_OBJECTS = [OBJECTS.PLAYER, OBJECTS.ENEMY, OBJECTS.ITEM];

const KEY_LISTENER = {
  UP: "šípkaHore",
  DOWN: "šípkaDole",
  LEFT: "šípkaVľavo",
  RIGHT: "šípkaVpravo",
  SPACE: "medzerník",
};

const DIRECTIONS_NAMES = {
  NULL: "x",
  UP: "hore",
  DOWN: "dole",
  LEFT: "vľavo",
  RIGHT: "vpravo",
};

const INTERRUPT_OPTIONS = {
  IGNORE: "ignoruj",
  WAIT: "čakaj",
  BREAK: "ukonči",
  RESTART: "odznovu",
};

const KEYS_ANALYSER = {
  [KEY_LISTENER.DOWN]: "ArrowDown",
  [KEY_LISTENER.UP]: "ArrowUp",
  [KEY_LISTENER.LEFT]: "ArrowLeft",
  [KEY_LISTENER.RIGHT]: "ArrowRight",
  [KEY_LISTENER.SPACE]: " ",
};

const STATUSES = {
  RUNNING: 0,
  SLEEPING: 1,
  TERMINATED: 2,
  STOPPED: 3,
};

const CURSOR_TYPES = {
  POINTER: 0,
  MOVE: 1,
  ERASER: 2,
};

const CYCLE_LOOP = [0, 1];
const FRAME_LIMIT = 8;
const IDLE_TIME = 100;

function setLocalItem(name, value) {
  localStorage.setItem(name, value);
}

const getLocalItem = (name) => {
  return localStorage.getItem(name);
};

var scaleFactor = 1.1;
var cursorChosen = 0;
var moveStep = 1;

function copyCommand(el) {
  navigator.clipboard.writeText(JSON.stringify(el));
}

function pasteCommand(e, commands) {
  navigator.clipboard.readText().then((cliptext) => {
    let newElement = JSON.parse(cliptext);
    newElement.order = o++;
    const key = Math.random().toString(16).slice(2);
    newElement.uid = key;
    if (!e) {
      commands.push(newElement);
      return;
    }
    let res = this.findCommand(e, commands);
    if (res) {
      let [el, elCommands] = res;
      elCommands.splice(elCommands.indexOf(el) + 1, 0, newElement);
    }
  });
}

function deleteCommand(e, commands) {
  let res = this.findCommand(e, commands);
  if (res) {
    let [el, elCommands] = res;
    elCommands.splice(elCommands.indexOf(el), 1);
  }
}

function findCommand(e, commands) {
  if (Array.isArray(commands)) {
    let el = commands.find((c) => c.order == e.order);
    if (commands.length == 0) {
      return undefined;
    }
    if (el) {
      return [el, commands];
    } else {
      let res;
      for (let element of commands) {
        if (element.commands) {
          res = this.findCommand(e, element.commands);
          if (res != undefined) {
            return res;
          }
        }
      }
    }
  } else {
    let res;
    if (commands.commands1) {
      res = this.findCommand(e, commands.commands1);
      if (res != undefined) {
        return res;
      }
    }
    if (commands.commands2) {
      res = this.findCommand(e, commands.commands2);
    }
    return res;
  }
}
