const KEY_STATE = {
  RELEASED: 0,
  PRESSED: 1,
  IMPULSE: 2,
  HOLD: 3,
};

class KeyAutomat {
  constructor() {
    this.state = KEY_STATE.RELEASED;
  }
  check() {
    return this.state != KEY_STATE.RELEASED;
  }
  setReleased() {
    this.state = KEY_STATE.RELEASED;
  }

  tick() {
    if (this.state == KEY_STATE.PRESSED) {
      this.state == KEY_STATE.HOLD;
    } else if (this.state == KEY_STATE.IMPULSE) {
      this.state = KEY_STATE.RELEASED;
    }
  }

  key_down() {
    if (this.state == KEY_STATE.RELEASED) {
      this.state = KEY_STATE.PRESSED;
    } else if (this.state == KEY_STATE.IMPULSE) {
      this.state = KEY_STATE.PRESSED;
    }
  }

  key_up() {
    if (this.state == KEY_STATE.PRESSED) {
      this.state = KEY_STATE.IMPULSE;
    } else if (this.state == KEY_STATE.HOLD) {
      this.state = KEY_STATE.RELEASED;
    }
  }
}

function setKeyListeners() {
  window.addEventListener("keydown", function (e) {
    switch (e.key) {
      case "ArrowLeft":
        e.preventDefault();
        Player.keyListeners.left.key_down();
        break;
      case "ArrowRight":
        e.preventDefault();
        Player.keyListeners.right.key_down();
        break;
      case "ArrowUp":
        e.preventDefault();
        Player.keyListeners.up.key_down();
        break;
      case "ArrowDown":
        e.preventDefault();
        Player.keyListeners.down.key_down();
        break;
      case " ":
        e.preventDefault();
        Player.keyListeners.space.key_down();
        break;
    }
  });
  window.addEventListener("keyup", function (e) {
    switch (e.key) {
      case "ArrowLeft":
        e.preventDefault();
        Player.keyListeners.left.key_up();
        break;
      case "ArrowRight":
        e.preventDefault();
        Player.keyListeners.right.key_up();
        break;
      case "ArrowUp":
        e.preventDefault();
        Player.keyListeners.up.key_up();
        break;
      case "ArrowDown":
        e.preventDefault();
        Player.keyListeners.down.key_up();
        break;
      case " ":
        e.preventDefault();
        Player.keyListeners.space.key_up();
        break;
    }
  });
}

class GameObject {
  constructor(id, r, c, w, imgName, src, type, passible) {
    this.size = w;
    this.srcId = 0;
    this.dx = 0;
    this.dy = 0;
    this.animate = false;
    this.imgName = imgName;
    this.commonObjects = [];
    if (type === OBJECTS.ENEMY) {
      return new Enemy(id, r, c, w, imgName, src, type);
    }
    if (type === OBJECTS.WALL) {
      return new Wall(id, r, c, w, src, type);
    }
    if (type === OBJECTS.ITEM) {
      return new Item(id, r, c, w, src, type, passible);
    }
    if (type === OBJECTS.PLAYER) {
      Player.set(id, r, c, w, imgName, src, type);
      return Player;
    }
    if (type === OBJECTS.NOTHING) {
      return new Nothing(r, c, w);
    }
  }
  setPassible(value) {
    this.passible = value;
  }
  delete() {
    gameScene.getCurrLevel().scene.remove(this.r, this.c, this);
  }
  getNextDir(direction) {
    if (direction == DIRECTIONS_NAMES.LEFT) {
      return { r: 0, c: -1 };
    }
    if (direction == DIRECTIONS_NAMES.RIGHT) {
      return { r: 0, c: 1 };
    }
    if (direction == DIRECTIONS_NAMES.UP) {
      return { r: -1, c: 0 };
    }
    if (direction == DIRECTIONS_NAMES.DOWN) {
      return { r: 1, c: 0 };
    }
    return { r: 0, c: 0 };
  }
  isCollisionWith(objectType) {
    var objects = gameScene
      .getCurrLevel()
      .scene.getObjectsOfType(OBJECT_TYPES[objectType]);

    if (objects.length > 0) {
      for (let o of objects) {
        if (!o.hidden && boxCollision(this, o) && this != o) {
          return true;
        }
      }
    }
    return false;
  }
  increaseX(deltaX) {
    this.dx = deltaX;
  }
  increaseY(deltaY) {
    this.dy = deltaY;
  }
  decreaseX(deltaX) {
    this.dx = -deltaX;
  }
  decreaseY(deltaY) {
    this.dy = -deltaY;
  }
  hide() {
    this.hidden = true;
  }
  show() {
    this.hidden = false;
  }
  setX(x) {
    if (x < this.getCanvas().width && x > 0) {
      this.x = x;
    }
  }
  setY(y) {
    if (y < this.getCanvas().height && y > 0) {
      this.y = y;
    }
  }
  isRowCol(row, col) {
    if (this.r + 1 == row && this.c + 1 == col) {
      return true;
    }
    return false;
  }
  moveObjectTo(objectType, row, col) {
    const scene = gameScene.getCurrLevel().scene;
    var objects = scene.getObjectsOfType(OBJECT_TYPES[objectType]);
    objects.forEach((o) => o.setRowCol(row, col));
  }
  moveObjectBy(objectType, row, col) {
    const scene = gameScene.getCurrLevel().scene;
    var objects = scene.getObjectsOfType(OBJECT_TYPES[objectType]);
    objects.forEach((o) => {
      o.setRowCol(o.r + row, o.c + col);
    });
  }
  isRowColObject(row, col, objectType) {
    const scene = gameScene.getCurrLevel().scene;
    const object = scene.get(row - 1, col - 1);
    if (object != 0) {
      return object.type == OBJECT_TYPES[objectType];
    }
    return false;
  }
  isObject(objectType, direction1, direction2) {
    const nextDir1 = this.getNextDir(direction1);
    const nextDir2 = this.getNextDir(direction2);
    const scene = gameScene.getCurrLevel().scene;
    const objects = scene.getObjects(this.r + nextDir2.r, this.c + nextDir1.c);
    const isObject = Array.from(objects.values()).every(
      (o) => o.type == OBJECT_TYPES[objectType]
    );
    if (isObject) {
      return true;
    }
    return false;
  }
  isNotObject(objectType, direction) {
    return !this.isObject(objectType, direction);
  }
  async moveToCell(direction1, direction2) {
    const nextDir1 = this.getNextDir(direction1);
    const nextDir2 = this.getNextDir(direction2);
    this.setRowCol(this.r + nextDir1.r, this.c + nextDir1.c);
    this.setRowCol(this.r + nextDir2.r, this.c + nextDir2.c);
  }
  async moveToCellStep(direction1, direction2) {
    const nextDir1 = this.getNextDir(direction1);
    const nextDir2 = this.getNextDir(direction2);
    const targetRC = { r: this.r + nextDir2.r, c: this.c + nextDir1.c };
    if (this.r == targetRC.r && this.c == targetRC.c) {
      return;
    }
    const oldRC = { r: this.r, c: this.c };
    while (this.r != targetRC.r) {
      this.y += moveStep * nextDir2.r;
      this.r = Math.floor(this.y / this.h);
      await sleep(100);
    }
    while (this.c != targetRC.c) {
      this.x += moveStep * nextDir1.r;
      this.c = Math.floor(this.x / this.w);
      await sleep(100);
    }
    if (this.r != oldRC.r || this.c != oldRC.c) {
      this.setGrid(oldRC);
    }
  }
  turnLeft() {
    if (this.currDirection == DIRECTIONS.FACING_LEFT) {
      this.currDirection = DIRECTIONS.FACING_DOWN;
    } else if (this.currDirection == DIRECTIONS.FACING_RIGHT) {
      this.currDirection = DIRECTIONS.FACING_UP;
    } else if (this.currDirection == DIRECTIONS.FACING_UP) {
      this.currDirection = DIRECTIONS.FACING_LEFT;
    } else if (this.currDirection == DIRECTIONS.FACING_DOWN) {
      this.currDirection = DIRECTIONS.FACING_DOWN;
    }
  }
  turnRight() {
    if (this.currDirection == DIRECTIONS.FACING_LEFT) {
      this.currDirection = DIRECTIONS.FACING_UP;
    } else if (this.currDirection == DIRECTIONS.FACING_RIGHT) {
      this.currDirection = DIRECTIONS.FACING_DOWN;
    } else if (this.currDirection == DIRECTIONS.FACING_UP) {
      this.currDirection = DIRECTIONS.FACING_RIGHT;
    } else if (this.currDirection == DIRECTIONS.FACING_DOWN) {
      this.currDirection = DIRECTIONS.FACING_LEFT;
    }
  }
  getCanvas() {
    var scene = gameScene.getCurrLevel().scene;
    return scene.getCanvas();
  }
  startAnimation() {
    this.animate = true;
  }
  stopAnimation() {
    this.animate = false;
  }
  moveCharacter(deltaX, deltaY) {
    const oldRC = { r: this.r, c: this.c };
    this.c = Math.floor(this.x / this.w);
    this.r = Math.floor(this.y / this.h);

    let nextR = Math.floor((this.y + deltaY) / this.h);
    let nextC = Math.floor((this.x + deltaX) / this.w);

    let cellObjects = gameScene.getCurrLevel().scene.getObjects(nextR, nextC);
    if (!cellObjects) {
      return;
    }
    let passible = Array.from(cellObjects.values()).every((o) => o.passible);

    if (passible) {
      if (this.x + deltaX > 0 && this.x + deltaX < this.getCanvas().width) {
        this.x += deltaX;
        this.c = nextC;
      }
      if (this.y + deltaY > 0 && this.y + deltaY < this.getCanvas().height) {
        this.y += deltaY;
        this.r = nextR;
      }
    }

    if (this.r != oldRC.r || this.c != oldRC.c) {
      this.setGrid(oldRC);
    }

    this.dx = 0;
    this.dy = 0;
  }
  getIsWall(r, c) {
    const scene = gameScene.getCurrLevel().scene;
    if (scene.get(r, c).type == OBJECTS.WALL) {
      return true;
    }
    return false;
  }
  touchingPlayer() {
    return gameScene.boxCollision(this, Player);
  }
  setGrid(oldRC) {
    const level = gameScene.getCurrLevel();
    level.scene.remove(oldRC.r, oldRC.c, this);
    level.scene.set(this.r, this.c, this);
    this.cell = level.gridCells[getIndex(this.r, this.c)];
  }
  setRowCol(r, c) {
    const oldRC = { r: this.r, c: this.c };
    if (isRC(r, c) && !this.getIsWall(r, c)) {
      this.r = r;
      this.c = c;
      this.x = this.c * this.w;
      this.y = this.r * this.h;
      if (this.r != oldRC.r || this.c != oldRC.c) {
        this.setGrid(oldRC);
      }
    }
  }
  async setR(r) {
    const oldRC = { r: this.r, c: this.c };
    while (this.r != r) {
      if (
        this.r > r &&
        isRC(this.r - 1, this.c) &&
        !this.getIsWall(this.r - 1, this.c)
      ) {
        this.r -= 1;
      } else if (
        this.r < r &&
        isRC(this.r + 1, this.c) &&
        !this.getIsWall(this.r + 1, this.c)
      ) {
        this.r += 1;
      } else {
        break;
      }
    }
  }
  async setC(c) {
    const oldRC = { r: this.r, c: this.c };
    while (this.c != c) {
      if (
        this.c > c &&
        isRC(this.r, this.c - 1) &&
        !this.getIsWall(this.r, this.c - 1)
      ) {
        this.c -= 1;
      } else if (
        this.c < c &&
        isRC(this.r, this.c + 1) &&
        !this.getIsWall(this.r, this.c + 1)
      ) {
        this.c += 1;
      } else {
        break;
      }
      if (this.r != oldRC.r || this.c != oldRC.c) {
        this.setGrid(oldRC);
      }
      this.x = this.c * this.w;
      this.y = this.r * this.h;
    }
  }
  async setRC(r, c) {
    this.oldRC = { r: this.r, c: this.c };
    if (isRC(r, c) && !this.getIsWall(r, c)) {
      await this.setR(r);
      await this.setC(c);
    }
  }
  toJSON() {
    return {
      srcId: this.srcId,
      type: this.type,
      src: this.src,
      imgName: this.imgName,
      passible: this.passible,
    };
  }
  draw(strokeStyle = "black", lineWidth = 0.5) {
    var scene = gameScene.getCurrLevel().scene;

    scene.getCtx().lineWidth = lineWidth;
    scene.getCtx().strokeStyle = scene.gridShown
      ? "black"
      : "rgba(0, 0, 0, 0.0)";

    if (strokeStyle != "black") {
      scene.getCtx().strokeStyle = strokeStyle;
    }

    scene.getCtx().strokeRect(this.x, this.y, this.w, this.h);
    if (this.dx != 0 || this.dy != 0) {
      this.moveCharacter(this.dx, this.dy);
    }
    if (!this.hidden) {
      scene.getCtx().drawImage(this.img, this.x, this.y, this.w, this.h);
    } else {
      scene.getCtx().fillStyle = "rgba(0, 0, 0, 0.0)";
      scene.getCtx().fillRect(this.x, this.y, this.w, this.h);
    }
  }
  setProps(w, r, c) {
    this.w = w;
    this.h = w;
    this.r = r;
    this.c = c;
    this.x = this.c * this.w;
    this.y = this.r * this.h;
    this.oldRC = { r: this.r, c: this.c };
  }
}

class Enemy extends GameObject {
  constructor(id, r, c, w, imgName, src, type) {
    super();
    this.currDirection = DIRECTIONS.FACING_RIGHT;
    this.id = id;
    this.hidden = false;
    this.r = r;
    this.c = c;
    this.prevr = r;
    this.prevc = c;
    this.w = w;
    this.h = w;
    this.x = this.c * this.w;
    this.y = this.r * this.h;
    this.type = type;
    this.dx = 0;
    this.dy = 0;
    this.static = false;
    this.type = OBJECTS.ENEMY;
    this.img = new Image();
    this.hasMoved = false;
    this.chasingPlayer = false;
    this.alreadyChasing = false;
    this.passible = true;
    this.currentLoopIndex = 0;
    this.frameCount = 0;
    this.imgName = imgName;
    const filename = imgName.split("_").slice(0, -1).join(".");
    this.src = assets["characters"][filename + ".png"];
    if (filename.length == 0) {
      this.src = src;
    }
    this.loadImage(this.src);
    this.setCell();
  }
  loadImage(src) {
    this.currDirection = DIRECTIONS.FACING_RIGHT;
    this.img.src = src;
  }
  setCell() {
    if (gameScene.getCurrLevel()) {
      this.cell = gameScene.getCurrLevel().gridCells[getIndex(this.r, this.c)];
    }
  }
  clone() {
    enemies.push(this);
  }
  setStop() {
    this.stop = true;
  }
  draw(strokeStyle = "black", lineWidth = 0.5) {
    var scene = gameScene.getCurrLevel().scene;

    scene.getCtx().lineWidth = lineWidth;
    scene.getCtx().strokeStyle = scene.gridShown
      ? "black"
      : "rgba(0, 0, 0, 0.0)";

    if (strokeStyle != "black") {
      scene.getCtx().strokeStyle = strokeStyle;
    }
    scene.getCtx().strokeRect(this.x, this.y, this.w, this.h);
    if (this.dx != 0 || this.dy != 0) {
      this.moveCharacter(this.dx, this.dy);
    }
    if (!this.hidden) {
      this.drawFrame();
    } else {
      scene.getCtx().fillStyle = "rgba(0, 0, 0, 0.0)";
      scene.getCtx().fillRect(this.x, this.y, this.w, this.h);
    }
  }
  setPlay() {
    this.stop = false;
  }
  getX() {
    return this.x;
  }
  getY() {
    return this.y;
  }
  up(speed) {
    this.currDirection = DIRECTIONS.FACING_UP;
    this.forward(speed);
  }
  down(speed) {
    this.currDirection = DIRECTIONS.FACING_DOWN;
    this.forward(speed);
  }
  left(speed) {
    this.currDirection = DIRECTIONS.FACING_LEFT;
    this.forward(speed);
  }
  right(speed) {
    this.currDirection = DIRECTIONS.FACING_RIGHT;
    this.forward(speed);
  }
  forward(speed) {
    if (this.currDirection == DIRECTIONS.FACING_LEFT) {
      this.dx = -speed;
    } else if (this.currDirection == DIRECTIONS.FACING_RIGHT) {
      this.dx = speed;
    } else if (this.currDirection == DIRECTIONS.FACING_UP) {
      this.dy = -speed;
    } else if (this.currDirection == DIRECTIONS.FACING_DOWN) {
      this.dy = speed;
    }
  }
  setProps(w, r, c) {
    this.w = w;
    this.h = w;
    this.r = r;
    this.c = c;
    this.x = this.c * this.w;
    this.y = this.r * this.w;
    this.currDirection = DIRECTIONS.FACING_RIGHT;
    this.cell = gameScene.getCurrLevel().gridCells[getIndex(this.r, this.c)];
  }
  drawFrame() {
    var scene = gameScene.getCurrLevel().scene;
    var frameX = CYCLE_LOOP[this.currentLoopIndex];
    var frameY = this.currDirection;
    var canvasX = this.x;
    var canvasY = this.y;
    var WIDTH = this.img.width / 2;
    var HEIGHT = this.img.height / 4;
    var scaleW = this.w / WIDTH;
    var scaleH = this.h / HEIGHT;
    var scaled_width = scaleW * WIDTH;
    var scaled_height = scaleH * HEIGHT;

    scene
      .getCtx()
      .drawImage(
        this.img,
        frameX * WIDTH,
        frameY * HEIGHT,
        WIDTH,
        HEIGHT,
        canvasX,
        canvasY,
        scaled_width,
        scaled_height
      );
  }
  changeDir(dir) {
    const x = dir.x;
    const y = dir.y;
    if (x == 1) {
      this.currDirection = DIRECTIONS.FACING_RIGHT;
    } else if (x == -1) {
      this.currDirection = DIRECTIONS.FACING_LEFT;
    } else if (y == 1) {
      this.currDirection = DIRECTIONS.FACING_DOWN;
    } else if (y == -1) {
      this.currDirection = DIRECTIONS.FACING_UP;
    }
  }
  chasePlayer() {
    Player.setCell();
    if (Player.cell == undefined) return;
    this.getPath();
  }
  maze() {
    this.setCell();
    let currentCell = this.cell;

    while (true) {
      currentCell.visited = true;
      let next = currentCell.checkNeighbors();
      if (next) {
        stack.push(currentCell);
        currentCell = next;
      } else if (stack.length > 0) {
        currentCell = stack.pop();
      } else {
        for (const cell of gameScene.getCurrLevel().gridCells) {
          cell.visited = false;
        }
        break;
      }
    }
  }
  getPath() {
    const goal = Player.cell;
    let path = this.AStar(goal);
    let newCell = path[1];
    if (path[0] == goal) {
      newCell = goal;
    }
    if (newCell) {
      this.update(newCell);
    }
  }
  update(cell) {
    let scene = gameScene.getCurrLevel().scene;
    let dir = getDirection(this.cell, cell);
    this.changeDir(dir);
    this.dx = dir.x * scene.cellSize;
    this.dy = dir.y * scene.cellSize;
  }
  AStar(goal) {
    this.cell = gameScene.getCurrLevel().gridCells[getIndex(this.r, this.c)];
    let openList = [this.cell];
    let closedList = [];
    let goalCell = goal;
    let currentCell;
    let path = [];
    for (let i in gameScene.getCurrLevel().gridCells) {
      gameScene.getCurrLevel().gridCells[i].cost = Infinity;
      gameScene.getCurrLevel().gridCells[i].heuristic = Infinity;
      gameScene.getCurrLevel().gridCells[i].parent = undefined;
    }

    this.cell.cost = 0;
    while (openList.length > 0) {
      openList.sort((x, y) => y.f - x.f);
      currentCell = openList.pop();

      if (currentCell == goalCell) {
        break;
      }
      closedList.push(currentCell);

      for (const n of currentCell.neighbors) {
        if (closedList.includes(n)) continue;

        n.heuristic = manhattanDistance(n, goalCell);
        let newCost = currentCell.cost + 1;

        if (!openList.includes(n) && n.passible) {
          openList.push(n);
        } else if (newCost >= n.cost) {
          continue;
        }
        n.parent = currentCell;
        n.cost = newCost;
        n.f = n.cost + n.heuristic;
      }
    }
    while (currentCell) {
      path.unshift(currentCell);
      currentCell = currentCell.parent;
    }
    return path;
  }
}

class Item extends GameObject {
  constructor(id, r, c, w, src, type, passible) {
    super();
    this.id = id;
    this.r = r;
    this.c = c;
    this.w = w;
    this.h = w;
    this.x = this.c * this.w;
    this.y = this.r * this.h;
    this.src = src;
    this.type = type;
    this.hidden = false;
    this.static = true;
    this.passible = passible;
    this.img = new Image();
    this.loadImage(src);
  }
  loadImage(src) {
    this.img.src = src;
  }
}

const keyPresses = {};

const Player = {
  dir: {
    x: 0,
    y: 0,
  },
  currDirection: DIRECTIONS.FACING_RIGHT,
  static: false,
  r: 0,
  c: 0,
  x: 0,
  y: 0,
  w: 30,
  h: 30,
  dx: 0,
  dy: 0,
  id: null,
  cell: null,
  type: OBJECTS.PLAYER,
  img: null,
  src: "",
  movementSpeed: 10,
  hasMoved: false,
  hidden: false,
  changedPos: false,
  stop: false,
  canvasId: "canvas",
  currentLoopIndex: 0,
  frameCount: 0,
  srcId: 0,
  passible: true,
  animate: false,
  toJSON() {
    return {
      srcId: this.srcId,
      type: this.type,
      src: this.src,
      imgName: this.imgName,
      passible: this.passible,
    };
  },
  setCell() {
    if (gameScene.getCurrLevel()) {
      this.cell = gameScene.getCurrLevel().gridCells[getIndex(this.r, this.c)];
    }
  },
  getNextDir(direction) {
    if (direction == DIRECTIONS_NAMES.LEFT) {
      return { r: 0, c: -1 };
    }
    if (direction == DIRECTIONS_NAMES.RIGHT) {
      return { r: 0, c: 1 };
    }
    if (direction == DIRECTIONS_NAMES.UP) {
      return { r: -1, c: 0 };
    }
    if (direction == DIRECTIONS_NAMES.DOWN) {
      return { r: 1, c: 0 };
    }
    return { r: 0, c: 0 };
  },
  setRowCol(r, c) {
    const oldRC = { r: this.r, c: this.c };
    if (isRC(r, c) && !this.getIsWall(r, c)) {
      this.r = r;
      this.c = c;
      if (this.r != oldRC.r || this.c != oldRC.c) {
        this.setGrid(oldRC);
      }
      this.x = this.c * this.w;
      this.y = this.r * this.h;
    }
  },
  isCollisionWith(objectType) {
    var objects = gameScene
      .getCurrLevel()
      .scene.getObjectsOfType(OBJECT_TYPES[objectType]);
    if (objects.length > 0) {
      objects.forEach((o) => {
        if (!o.hidden && boxCollision(this, o)) {
          return true;
        }
      });
    }
    return false;
  },
  isRowCol(row, col) {
    if (this.r + 1 == row && this.c + 1 == col) {
      return true;
    }
    return false;
  },
  moveObjectTo(objectType, row, col) {
    const scene = gameScene.getCurrLevel().scene;
    var objects = scene.getObjectsOfType(OBJECT_TYPES[objectType]);
    objects.forEach((o) => o.setRowCol(row, col));
  },
  moveObjectBy(objectType, row, col) {
    const scene = gameScene.getCurrLevel().scene;
    var objects = scene.getObjectsOfType(OBJECT_TYPES[objectType]);
    objects.forEach((o) => o.setRowCol(o.r + row, o.c + col));
  },
  isRowColObject(row, col, objectType) {
    const scene = gameScene.getCurrLevel().scene;
    const object = scene.get(row - 1, col - 1);
    if (object != 0) {
      return object.type == OBJECT_TYPES[objectType];
    }
    return false;
  },
  isObject(objectType, direction) {
    const nextDir = this.getNextDir(direction);
    const scene = gameScene.getCurrLevel().scene;
    if (
      scene.get(this.r + nextDir.r, this.c + nextDir.c).type ==
      OBJECT_TYPES[objectType]
    ) {
      return true;
    }
    return false;
  },
  isNotObject(objectType, direction) {
    return !this.isObject(objectType, direction);
  },
  getIsWall(r, c) {
    const scene = gameScene.getCurrLevel().scene;
    if (scene.get(r, c).type == OBJECTS.WALL) {
      return true;
    }
    return false;
  },
  async setR(r) {
    while (this.r != r) {
      if (
        this.r > r &&
        isRC(this.r - 1, this.c) &&
        !this.getIsWall(this.r - 1, this.c)
      ) {
        this.r -= 1;
      } else if (
        this.r < r &&
        isRC(this.r + 1, this.c) &&
        !this.getIsWall(this.r + 1, this.c)
      ) {
        this.r += 1;
      } else {
        return;
      }
      this.y = this.r * this.w;
      await sleep(400);
    }
  },
  async setC(c) {
    while (this.c != c) {
      if (isRC(this.r, c) && !this.getIsWall(this.r, c)) {
        if (
          this.c > c &&
          isRC(this.r, this.c - 1) &&
          !this.getIsWall(this.r, this.c - 1)
        ) {
          this.c -= 1;
        } else if (
          this.c < c &&
          isRC(this.r, this.c + 1) &&
          !this.getIsWall(this.r, this.c + 1)
        ) {
          this.c += 1;
        }
        this.x = this.c * this.w;
        await sleep(400);
      }
    }
  },
  async setRC(r, c) {
    if (isRC(r, c) && !this.getIsWall(r, c)) {
      await this.setR(r);
      await this.setC(c);
    }
  },
  keyListeners: {
    left: new KeyAutomat(),
    right: new KeyAutomat(),
    up: new KeyAutomat(),
    down: new KeyAutomat(),
    space: new KeyAutomat(),
    enter: new KeyAutomat(),
  },
  keyAutomatTick() {
    this.keyListeners.left.tick();
    this.keyListeners.right.tick();
    this.keyListeners.up.tick();
    this.keyListeners.down.tick();
    this.keyListeners.space.tick();
    this.keyListeners.enter.tick();
  },
  setProps(w, r, c) {
    this.w = w;
    this.h = w;
    this.r = r;
    this.c = c;
    this.x = this.c * this.w;
    this.y = this.r * this.w;
    this.currDirection = DIRECTIONS.FACING_RIGHT;
    this.cell = gameScene.getCurrLevel().gridCells[getIndex(this.r, this.c)];
  },
  loadImage(src) {
    this.currDirection = DIRECTIONS.FACING_RIGHT;
    this.img.src = src;

    this.img.onload = () => {
      //Player.drawFrame();
    };
  },
  checkKeyPressed(key) {
    if (key == KEY_LISTENER.LEFT) {
      return Player.keyListeners.left.check();
    }
    if (key == KEY_LISTENER.RIGHT) {
      return Player.keyListeners.right.check();
    }
    if (key == KEY_LISTENER.UP) {
      return Player.keyListeners.up.check();
    }
    if (key == KEY_LISTENER.DOWN) {
      return Player.keyListeners.down.check();
    }
    if (key == KEY_LISTENER.SPACE) {
      return Player.keyListeners.space.check();
    }
    if (key == KEY_LISTENER.ENTER) {
      return Player.keyListeners.enter.check();
    }
    return false;
  },
  setKeyListener(key, subroutine) {
    this.keyListeners[key] = subroutine;
  },
  set(id, r, c, w, imgName, src, type) {
    this.id = id;
    this.w = w;
    this.h = w;
    this.r = r;
    this.c = c;
    this.imgName = imgName;
    this.type = type;
    this.x = this.c * this.w;
    this.y = this.r * this.w;
    this.setCell();
    this.currDirection = DIRECTIONS.FACING_RIGHT;
    const filename = imgName.split("_").slice(0, -1).join(".");
    this.src = assets["characters"][filename + ".png"];
    if (filename.length == 0) {
      this.src = src;
    }
    this.img = new Image();
    this.loadImage(this.src);
  },
  draw(strokeStyle = "black", lineWidth = 0.5) {
    var scene = gameScene.getCurrLevel().scene;
    scene.getCtx().strokeStyle = scene.gridShown
      ? "black"
      : "rgba(0, 0, 0, 0.0)";
    scene.getCtx().lineWidth = lineWidth;

    if (strokeStyle != "black") {
      scene.getCtx().strokeStyle = strokeStyle;
    }
    scene.getCtx().strokeRect(this.x, this.y, this.w, this.h);

    if (Player.dx != 0 || Player.dy != 0) {
      Player.moveCharacter(Player.dx, Player.dy);
    }
    if (!this.hidden) {
      Player.drawFrame();
    } else {
      scene.getCtx().fillStyle = "rgba(0, 0, 0, 0.0)";
      scene.getCtx().fillRect(this.x, this.y, this.w, this.h);
    }
  },
  hide() {
    this.hidden = true;
  },
  show() {
    this.hidden = false;
  },
  delete() {
    gameScene.getCurrLevel().scene.remove(this.r, this.c, this);
  },
  setPlay() {
    this.stop = false;
  },
  increaseX(deltaX) {
    this.dx = deltaX;
  },
  increaseY(deltaY) {
    this.dy = deltaY;
  },
  decreaseX(deltaX) {
    this.dx = -deltaX;
  },
  decreaseY(deltaY) {
    this.dy = -deltaY;
  },
  setX(x) {
    if (x < this.getCanvas().width && x > 0) {
      this.x = x;
    }
  },
  setY(y) {
    if (y < this.getCanvas().height && y > 0) {
      this.y = y;
    }
  },
  getX() {
    return this.x;
  },
  getY() {
    return this.y;
  },
  moveToCell(direction1, direction2) {
    const nextDir1 = this.getNextDir(direction1);
    const nextDir2 = this.getNextDir(direction2);
    if (isRC(this.r + nextDir1.r, this.c + nextDir1.c)) {
      this.setR(this.r + nextDir1.r);
      this.setC(this.c + nextDir1.c);
    }
    if (isRC(this.r + nextDir2.r, this.c + nextDir2.c)) {
      this.setR(this.r + nextDir2.r);
      this.setC(this.c + nextDir2.c);
    }
  },
  up(speed = 2) {
    Player.currDirection = DIRECTIONS.FACING_UP;
    this.forward(speed);
  },
  down(speed = 2) {
    Player.currDirection = DIRECTIONS.FACING_DOWN;
    this.forward(speed);
  },
  turnLeft() {
    if (this.currDirection == DIRECTIONS.FACING_LEFT) {
      this.currDirection = DIRECTIONS.FACING_DOWN;
    } else if (this.currDirection == DIRECTIONS.FACING_RIGHT) {
      this.currDirection = DIRECTIONS.FACING_UP;
    } else if (this.currDirection == DIRECTIONS.FACING_UP) {
      this.currDirection = DIRECTIONS.FACING_LEFT;
    } else if (this.currDirection == DIRECTIONS.FACING_DOWN) {
      this.currDirection = DIRECTIONS.FACING_DOWN;
    }
  },
  turnRight() {
    if (this.currDirection == DIRECTIONS.FACING_LEFT) {
      this.currDirection = DIRECTIONS.FACING_UP;
    } else if (this.currDirection == DIRECTIONS.FACING_RIGHT) {
      this.currDirection = DIRECTIONS.FACING_DOWN;
    } else if (this.currDirection == DIRECTIONS.FACING_UP) {
      this.currDirection = DIRECTIONS.FACING_RIGHT;
    } else if (this.currDirection == DIRECTIONS.FACING_DOWN) {
      this.currDirection = DIRECTIONS.FACING_LEFT;
    }
  },
  left(speed = 2) {
    Player.currDirection = DIRECTIONS.FACING_LEFT;
    this.forward(speed);
  },
  right(speed = 2) {
    Player.currDirection = DIRECTIONS.FACING_RIGHT;
    this.forward(speed);
  },
  forward(speed = 2) {
    if (Player.currDirection == DIRECTIONS.FACING_LEFT) {
      Player.dx = -speed;
    } else if (Player.currDirection == DIRECTIONS.FACING_RIGHT) {
      Player.dx = speed;
    } else if (Player.currDirection == DIRECTIONS.FACING_UP) {
      Player.dy = -speed;
    } else if (Player.currDirection == DIRECTIONS.FACING_DOWN) {
      Player.dy = speed;
    }
  },
  keyDownListener(event) {
    keyPresses[event.key] = true;
  },

  keyUpListener(event) {
    keyPresses[event.key] = false;
  },
  drawFrame() {
    var scene = gameScene.getCurrLevel().scene;
    var frameX = CYCLE_LOOP[this.currentLoopIndex];
    var frameY = this.currDirection;
    var canvasX = this.x;
    var canvasY = this.y;
    var WIDTH = this.img.width / 2;
    var HEIGHT = this.img.height / 4;
    var scaleW = this.w / WIDTH;
    var scaleH = this.h / HEIGHT;
    var scaled_width = scaleW * WIDTH;
    var scaled_height = scaleH * HEIGHT;

    scene
      .getCtx()
      .drawImage(
        this.img,
        frameX * WIDTH,
        frameY * HEIGHT,
        WIDTH,
        HEIGHT,
        canvasX,
        canvasY,
        scaled_width,
        scaled_height
      );
  },
  setGrid(oldRC) {
    const level = gameScene.getCurrLevel();
    level.scene.remove(oldRC.r, oldRC.c, this);
    level.scene.set(this.r, this.c, this);
    this.cell = level.gridCells[getIndex(this.r, this.c)];
  },
  getCanvas() {
    var scene = gameScene.getCurrLevel().scene;
    return scene.getCanvas();
  },
  doAnimate() {
    this.currentLoopIndex++;
    if (this.currentLoopIndex >= CYCLE_LOOP.length) {
      this.currentLoopIndex = 0;
    }
  },
  startAnimation(speed) {
    if (!this.animate) {
      this.animate = true;
      this.animateInterval = setInterval(this.doAnimate, 1000);
    } else {
      return;
    }
  },
  stopAnimation() {
    this.animate = false;
    if (this.animateInterval) {
      clearInterval(this.animateInterval);
    }
  },
  moveCharacter(deltaX, deltaY) {
    this.hasMoved = false;
    const oldRC = { r: this.r, c: this.c };

    let tx = this.x + this.w / 2;
    let ty = this.y + this.h / 2;
    this.c = Math.floor(tx / this.w);
    this.r = Math.floor(ty / this.h);

    let nextR = Math.floor((ty + deltaY) / this.h);
    let nextC = Math.floor((tx + deltaX) / this.w);

    let cellObjects = gameScene.getCurrLevel().scene.getObjects(nextR, nextC);
    if (!cellObjects) {
      return;
    }
    let passible = Array.from(cellObjects.values()).every((o) => o.passible);

    if (passible) {
      if (tx + deltaX > 0 && tx + deltaX < this.getCanvas().width) {
        this.x += deltaX;
        this.hasMoved = true;
        this.c = nextC;
      }
      if (ty + deltaY > 0 && ty + deltaY < this.getCanvas().height) {
        this.y += deltaY;
        this.hasMoved = true;
        this.r = nextR;
      }
    }

    if (this.r != oldRC.r || this.c != oldRC.c) {
      this.setGrid(oldRC);
    }

    if (this.hasMoved) {
      this.currentLoopIndex++;
      if (this.currentLoopIndex >= CYCLE_LOOP.length) {
        this.currentLoopIndex = 0;
      }
    }
    Player.dx = 0;
    Player.dy = 0;
  },
};

class Wall extends GameObject {
  constructor(id, r, c, w, src, type) {
    super();
    this.id = id;
    this.passible = false;
    this.r = r;
    this.c = c;
    this.w = w;
    this.h = w;
    this.x = this.c * this.w;
    this.y = this.r * this.h;
    this.src = src;
    this.type = type;
    this.static = true;
    this.img = new Image();
    this.loadImage(src);
  }
  loadImage(src) {
    this.img.src = src;
    this.img.onload = () => {
      //this.draw();
    };
  }
}

class Nothing extends GameObject {
  constructor(r, c, w) {
    super();
    this.id = 0;
    this.passible = true;
    this.r = r;
    this.c = c;
    this.w = w;
    this.h = w;
    this.x = this.c * this.w;
    this.y = this.r * this.h;
    this.src = "";
    this.type = OBJECTS.NOTHING;
    this.static = true;
  }
  draw() {
    var scene = gameScene.getCurrLevel().scene;
    scene.getCtx().lineWidth = 0.5;
    scene.getCtx().strokeStyle = scene.gridShown
      ? "black"
      : "rgba(0, 0, 0, 0.0)";
    scene.getCtx().strokeRect(this.x, this.y, this.w, this.h);
  }
}
