Vue.component("gameObjects", {
  created() {
    this.$root.$refs.GameObjects = this;
  },
  template: `
  <v-card elevation="8" class="scrollable-card">
  <v-card-subtitle>Programovanie objektov</v-card-subtitle>
  <v-card-text>
  <v-btn-toggle v-model="toggled"
  v-on:change="setObjectProgrammed(toggled)"
  tile
  mandatory
  color="secondary"
  group>
    <v-col
      v-for="(item, key) in objects"
      :key="key"
      cols="cols"
    >
    <v-btn
    elevation="2"
    :value="item.id"
  >
      <v-img
        :src="item.showSrc"
        height="50px"
        width="50px"
      >
        <template v-slot:placeholder>
          <v-row
            class="ma-0"
            align="center"
            justify="center"
          >
            <v-progress-circular
              indeterminate
              color="grey lighten-5"
            ></v-progress-circular>
          </v-row>
        </template>
      </v-img>
      </v-btn>
    </v-col>
  
  </v-btn-toggle>
  </v-card-text>
</v-card>`,
  watch: {
    objects() {
      return gameScene.getCurrLevel().programmableObjects;
    },
    rows() {
      return Math.ceil(
        gameScene.getCurrLevel().programmableObjects.length %
          gameScene.getCurrLevel().scene.cols
      );
    },
  },
  data() {
    return {
      toggled: gameScene.getCurrLevel().objectProgrammed
        ? gameScene.getCurrLevel().objectProgrammed.id
        : 1,
      objects: gameScene.getCurrLevel().programmableObjects,
      cols: gameScene.getCurrLevel().programmableObjects.length,
      rows: Math.ceil(
        gameScene.getCurrLevel().programmableObjects.length %
          gameScene.getCurrLevel().scene.cols
      ),
    };
  },
  methods: {
    setObjectProgrammed(toggled) {
      gameScene.getCurrLevel().hideRectInCanvas();
      gameScene.setObjectProgrammed(toggled);
      const object = gameScene.getCurrLevel().objectProgrammed;
      this.$root.$refs.TabObject.object = object;
      this.showRectInCanvas();
    },
    showRectInCanvas() {
      gameScene.getCurrLevel().showRectInCanvas();
    },
  },
});
