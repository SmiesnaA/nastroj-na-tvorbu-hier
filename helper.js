function manhattanDistance(c1, c2) {
  return Math.abs(c1.c - c2.c) + Math.abs(c1.r - c2.r);
}

function findCell(i, j) {
  return gameScene.gridCells.find((c) => {
    return c.r == i && c.c == j;
  });
}

function getIndex(r, c) {
  if (c > gameScene.cols - 1 || r > gameScene.rows - 1 || r < 0 || c < 0)
    return -1;
  return c + gameScene.cols * r;
}

function getDirection(c1, c2) {
  let x = c2.c - c1.c;
  let y = c2.r - c1.r;
  return { x, y };
}

function areNeighbors(c1, c2) {
  if (c1 && c2) {
    for (const n of c1.neighbors) {
      if (n == c2) return true;
    }
  }
  return false;
}

function transformPoint(ctx, x, y) {
  const matrix = ctx.getTransform();
  return {
    x: matrix.a * point.x + matrix.c * point.y + matrix.e,
    y: matrix.b * point.x + matrix.d * point.y + matrix.f,
  };
}
