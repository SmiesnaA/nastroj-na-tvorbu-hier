var keySub = 0;
let o = 0;

class Syntax {
  generate() {}
}

class Event extends Syntax {}

class Listener extends Syntax {}

class Const extends Syntax {
  constructor(nvalue) {
    super();
    this.value = nvalue;
  }
  translate() {
    return "" + this.value;
  }
  generate(vm) {
    vm.poke(INSTRUCTION_PUSH);
    vm.poke(this.value);
  }
  toJSON() {
    return this.value;
  }
}

class UnaryOperation extends Syntax {
  constructor(ne) {
    super();
    this.e = ne;
  }
}

class Negative extends UnaryOperation {
  generate(vm) {
    this.e.generate();
    vm.poke(INSTRUCTION_NEGATIVE);
  }
  translate() {
    return "-" + this.e.translate();
  }
  toJSON() {
    return -this.e.toJSON();
  }
}

class Text extends Syntax {
  constructor(nvalue) {
    super();
    this.value = nvalue;
  }
  translate() {
    return '"' + this.value + '"';
  }
  generate(vm) {
    vm.poke(INSTRUCTION_PUSH);
    vm.poke(this.value);
  }
  toJSON() {
    return this.value;
  }
}

class Key extends Syntax {
  constructor(nvalue) {
    super();
    this.value = nvalue;
  }
  translate() {
    return "" + this.value;
  }
  generate(vm) {
    vm.poke(INSTRUCTION_PUSH);
    vm.poke(this.value);
  }
  toJSON() {
    return this.value;
  }
}

class Direction extends Syntax {
  constructor(nvalue) {
    super();
    this.value = nvalue;
  }
  translate() {
    return "" + this.value;
  }
  generate(vm) {
    vm.poke(INSTRUCTION_PUSH);
    vm.poke(this.value);
  }
  toJSON() {
    return this.value;
  }
}

class MoveOption extends Syntax {
  constructor(nvalue) {
    super();
    this.value = nvalue;
  }
  translate() {
    return "" + this.value;
  }
  generate(vm) {
    vm.poke(INSTRUCTION_PUSH);
    vm.poke(this.value);
  }
  toJSON() {
    return this.value;
  }
}

class ObjectType extends Syntax {
  constructor(nvalue) {
    super();
    this.value = nvalue;
  }
  translate() {
    return "" + this.value;
  }
  generate(vm) {
    vm.poke(INSTRUCTION_PUSH);
    vm.poke(this.value);
  }
  toJSON() {
    return this.value;
  }
}

class InterruptHandler extends Syntax {
  constructor(nvalue) {
    super();
    this.value = nvalue;
  }
  translate() {
    return "" + this.value;
  }
  generate(vm) {
    vm.poke(INSTRUCTION_PUSH);
    vm.poke(this.value);
    vm.poke(INSTRUCTION_SET_INTER_HANDLER);
  }
  toJSON() {
    return this.value;
  }
}

class ItemCommand extends Syntax {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
}

class EnemyCommand extends Syntax {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
}

class CharacterCommand extends Syntax {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
}

class Up extends CharacterCommand {
  translate() {
    return "up " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_UP);
  }
  toJSON() {
    return {
      name: "up",
      representationName: "Hore",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      commands: [],
    };
  }
}

class Down extends CharacterCommand {
  translate() {
    return "down " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_DOWN);
  }
  toJSON() {
    return {
      name: "down",
      representationName: "Dole",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      commands: [],
    };
  }
}

class Fd extends CharacterCommand {
  translate() {
    return "forward " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_FD);
  }
  toJSON() {
    return {
      name: "forward",
      representationName: "Dopredu",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      commands: [],
    };
  }
}

class Lt extends CharacterCommand {
  translate() {
    return "left " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_LT);
  }
  toJSON() {
    return {
      name: "left",
      representationName: "Vľavo",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      commands: [],
    };
  }
}

class TurnLt extends CharacterCommand {
  translate() {
    return "turnLeft";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_TURN_LT);
  }
  toJSON() {
    return {
      name: "turnLeft",
      representationName: "Otoč sa vľavo",
      order: o++,
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class TurnRt extends CharacterCommand {
  translate() {
    return "turnRight";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_TURN_RT);
  }
  toJSON() {
    return {
      name: "turnRight",
      representationName: "Otoč sa vpravo",
      order: o++,
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class Rt extends CharacterCommand {
  translate() {
    return "right " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_RT);
  }
  toJSON() {
    return {
      name: "right",
      representationName: "Vpravo",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      commands: [],
    };
  }
}

class HideObject extends ItemCommand {
  translate() {
    return "hideObject";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_HIDE_OBJECT);
  }
  toJSON() {
    return {
      name: "hideObject",
      representationName: "Skry objekt",
      order: o++,
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class SetExecutionSpeed extends Syntax {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
  translate() {
    return "setExecutionSpeed " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_SET_EXECUTION_SPEED);
  }
  toJSON() {
    return {
      name: "setExecutionSpeed",
      representationName: "Nastav rýchlosť vykonávania:",
      order: o++,
      parameter: this.param.toJSON(),
      paramMin: 1,
      paramMax: 200,
      parameterType: "input",
      commands: [],
    };
  }
}

class StopExecution extends Syntax {
  constructor() {
    super();
  }
  translate() {
    return "stopExecution";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_STOP_EXECUTION);
  }
  toJSON() {
    return {
      name: "stopExecution",
      representationName: "Zastav vykonávanie",
      order: o++,
      commands: [],
    };
  }
}

class StopGame extends Syntax {
  constructor() {
    super();
  }
  translate() {
    return "stopGame";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_STOP_GAME);
  }
  toJSON() {
    return {
      name: "stopGame",
      representationName: "Zastav hru",
      order: o++,
      commands: [],
    };
  }
}

class IsScore extends ItemCommand {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
  translate() {
    return "isScore " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_IS_SCORE);
  }
  toJSON() {
    return {
      name: "isScore",
      representationName: "Je skóre:",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 0,
      paramMax: 1000,
      commands: [],
    };
  }
}

class SetScore extends ItemCommand {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
  translate() {
    return "setScore " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_SET_SCORE);
  }
  toJSON() {
    return {
      name: "setScore",
      representationName: "Nastav skóre:",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 100,
      commands: [],
    };
  }
}

class ChangeScore extends ItemCommand {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
  translate() {
    return "changeScore " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_CHANGE_SCORE);
  }
  toJSON() {
    return {
      name: "changeScore",
      representationName: "Zmeň skóre o:",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 100,
      commands: [],
    };
  }
}

class ShowObject extends ItemCommand {
  translate() {
    return "showObject";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_SHOW_OBJECT);
  }
  toJSON() {
    return {
      name: "showObject",
      representationName: "Zobraz objekt",
      order: o++,
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class StartAnimation extends Syntax {
  translate() {
    return "startAnimation";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_START_ANIMATION);
  }
  toJSON() {
    return {
      name: "startAnimation",
      representationName: "Spusti animáciu",
      order: o++,
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class StopAnimation extends Syntax {
  translate() {
    return "stopAnimation";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_STOP_ANIMATION);
  }
  toJSON() {
    return {
      name: "stopAnimation",
      representationName: "Zastav animáciu",
      order: o++,
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class IsCollisionWith extends Syntax {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
  translate() {
    return "isCollisionWith " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_IS_COLLISION_WITH);
  }
  toJSON() {
    return {
      name: "isCollisionWith",
      representationName: "Je kolízia s: ",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "select",
      options: [
        OBJECTS_NAMES.NOTHING,
        OBJECTS_NAMES.WALL,
        OBJECTS_NAMES.ITEM,
        OBJECTS_NAMES.PLAYER,
        OBJECTS_NAMES.ENEMY,
      ],
      commands: [],
    };
  }
}

class IncX extends CharacterCommand {
  translate() {
    return "increaseX " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_INC_X);
  }
  toJSON() {
    return {
      name: "increaseX",
      representationName: "Zväčši x o: ",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 200,
      commands: [],
    };
  }
}

class IncY extends CharacterCommand {
  translate() {
    return "increaseY " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_INC_Y);
  }
  toJSON() {
    return {
      name: "increaseY",
      representationName: "Zväčši y o: ",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 200,
      commands: [],
    };
  }
}

class DecX extends CharacterCommand {
  translate() {
    return "decreaseX " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_DEC_X);
  }
  toJSON() {
    return {
      name: "decreaseX",
      representationName: "Zmenši x o: ",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 100,
      commands: [],
    };
  }
}

class DecY extends CharacterCommand {
  translate() {
    return "decreaseY " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_DEC_Y);
  }
  toJSON() {
    return {
      name: "decreaseY",
      representationName: "Zmenši y o: ",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 100,
      commands: [],
    };
  }
}

class SetX extends CharacterCommand {
  translate() {
    return "setX " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_SET_X);
  }
  toJSON() {
    return {
      name: "setX",
      representationName: "Nastav x na: ",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 1000,
      commands: [],
    };
  }
}

class SetY extends CharacterCommand {
  translate() {
    return "setY " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_SET_Y);
  }
  toJSON() {
    return {
      name: "setY",
      representationName: "Nastav y na: ",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 1000,
      commands: [],
    };
  }
}

class SetR extends CharacterCommand {
  translate() {
    return "setR " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_SET_R);
  }
  toJSON() {
    return {
      name: "setR",
      representationName: "Choď na riadok: ",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 100,
      commands: [],
    };
  }
}

class SetRC extends Syntax {
  constructor(param1, param2) {
    super();
    this.param1 = param1;
    this.param2 = param2;
  }
  translate() {
    return "setRC " + this.param1.translate() + " " + this.param2.translate();
  }
  generate(vm) {
    this.param1.generate(vm);
    this.param2.generate(vm);
    vm.poke(INSTRUCTION_SET_RC);
  }
  toJSON() {
    return {
      name: "setRC",
      representationName: "Choď na riadok a stĺpec: ",
      order: o++,
      parameter1: this.param1.toJSON(),
      parameter2: this.param2.toJSON(),
      parameterType: "inputInput",
      paramMin: 1,
      paramMax: 100,
      commands: [],
    };
  }
}

class SetC extends CharacterCommand {
  translate() {
    return "setC " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_SET_C);
  }
  toJSON() {
    return {
      name: "setC",
      representationName: "Choď na stĺpec: ",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 100,
      commands: [],
    };
  }
}

class Wait extends Syntax {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
  translate() {
    return "wait " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_WAIT);
  }
  toJSON() {
    return {
      name: "wait",
      representationName: "Čakaj",
      order: o++,
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 100,
      paramMax: 10000,
      commands: [],
    };
  }
}

class DeleteObject extends CharacterCommand {
  translate() {
    return "deleteObject";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_DEL_SPRITE);
  }
  toJSON() {
    return {
      name: "deleteObject",
      representationName: "Vymaž objekt",
      order: o++,
      parameterType: null,
      commands: [],
    };
  }
}

class IsObject extends CharacterCommand {
  constructor(nparam1, nparam2, nparam3) {
    super();
    this.param1 = nparam1;
    this.param2 = nparam2;
    this.param3 = nparam3;
  }
  translate() {
    return (
      "isObject " +
      this.param1.translate() +
      " " +
      this.param2.translate() +
      " " +
      this.param3.translate()
    );
  }
  generate(vm) {
    this.param1.generate(vm);
    this.param2.generate(vm);
    this.param3.generate(vm);
    vm.poke(INSTRUCTION_IS_OBJECT);
  }
  toJSON() {
    return {
      name: "isObject",
      representationName: "Je",
      order: o++,
      parameter1: this.param1.toJSON(),
      parameter2: this.param2.toJSON(),
      parameter3: this.param3.toJSON(),
      parameterType: "selectSelectSelect",
      options1: [
        OBJECTS_NAMES.NOTHING,
        OBJECTS_NAMES.WALL,
        OBJECTS_NAMES.ITEM,
        OBJECTS_NAMES.PLAYER,
        OBJECTS_NAMES.ENEMY,
      ],
      options2: [
        DIRECTIONS_NAMES.NULL,
        DIRECTIONS_NAMES.LEFT,
        DIRECTIONS_NAMES.RIGHT,
      ],
      options3: [
        DIRECTIONS_NAMES.NULL,
        DIRECTIONS_NAMES.UP,
        DIRECTIONS_NAMES.DOWN,
      ],
      commands: [],
    };
  }
}

class MoveObjectBy extends CharacterCommand {
  constructor(nparam1, nparam2, nparam3) {
    super();
    this.param1 = nparam1;
    this.param2 = nparam2;
    this.param3 = nparam3;
  }
  translate() {
    return (
      "moveObjectBy " +
      this.param1.translate() +
      " " +
      this.param2.translate() +
      " " +
      this.param3.translate()
    );
  }
  generate(vm) {
    this.param1.generate(vm);
    this.param2.generate(vm);
    this.param3.generate(vm);
    vm.poke(INSTRUCTION_MOVE_OBJECT_BY);
  }
  toJSON() {
    return {
      name: "moveObjectBy",
      representationName: "Presuň objekt o riadok, stĺpec",
      order: o++,
      parameter1: this.param1.toJSON(),
      parameter2: this.param2.toJSON(),
      parameter3: this.param3.toJSON(),
      options: [
        OBJECTS_NAMES.NOTHING,
        OBJECTS_NAMES.WALL,
        OBJECTS_NAMES.ITEM,
        OBJECTS_NAMES.PLAYER,
        OBJECTS_NAMES.ENEMY,
      ],
      parameterType: "selectInputInput",
      commands: [],
    };
  }
}

class MoveObjectTo extends CharacterCommand {
  constructor(nparam1, nparam2, nparam3) {
    super();
    this.param1 = nparam1;
    this.param2 = nparam2;
    this.param3 = nparam3;
  }
  translate() {
    return (
      "moveObjectTo " +
      this.param1.translate() +
      " " +
      this.param2.translate() +
      " " +
      this.param3.translate()
    );
  }
  generate(vm) {
    this.param1.generate(vm);
    this.param2.generate(vm);
    this.param3.generate(vm);
    vm.poke(INSTRUCTION_MOVE_OBJECT_TO);
  }
  toJSON() {
    return {
      name: "moveObjectTo",
      representationName: "Presuň objekt na riadok, stĺpec",
      order: o++,
      parameter1: this.param1.toJSON(),
      parameter2: this.param2.toJSON(),
      parameter3: this.param3.toJSON(),
      options: [
        OBJECTS_NAMES.NOTHING,
        OBJECTS_NAMES.WALL,
        OBJECTS_NAMES.ITEM,
        OBJECTS_NAMES.PLAYER,
        OBJECTS_NAMES.ENEMY,
      ],
      parameterType: "selectInputInput",
      commands: [],
    };
  }
}

class IsRowCol extends CharacterCommand {
  constructor(nparam1, nparam2) {
    super();
    this.param1 = nparam1;
    this.param2 = nparam2;
  }
  translate() {
    return (
      "isRowCol " + this.param1.translate() + " " + this.param2.translate()
    );
  }
  generate(vm) {
    this.param1.generate(vm);
    this.param2.generate(vm);
    vm.poke(INSTRUCTION_IS_ROW_COL);
  }
  toJSON() {
    return {
      name: "isRowCol",
      representationName: "Je riadok a stĺpec",
      order: o++,
      parameter1: this.param1.toJSON(),
      parameter2: this.param2.toJSON(),
      parameterType: "inputInput",
      commands: [],
    };
  }
}

class IsRowColObject extends CharacterCommand {
  constructor(nparam1, nparam2, nparam3) {
    super();
    this.param1 = nparam1;
    this.param2 = nparam2;
    this.param3 = nparam3;
  }
  translate() {
    return (
      "isRowColObject " +
      this.param1.translate() +
      " " +
      this.param2.translate() +
      " " +
      this.param3.translate()
    );
  }
  generate(vm) {
    this.param1.generate(vm);
    this.param2.generate(vm);
    this.param3.generate(vm);
    vm.poke(INSTRUCTION_IS_ROW_COL_OBJECT);
  }
  toJSON() {
    return {
      name: "isRowColObject",
      representationName: "Je riadok a stĺpec objekt",
      order: o++,
      parameter1: this.param1.toJSON(),
      parameter2: this.param2.toJSON(),
      parameter3: this.param3.toJSON(),
      options: [
        OBJECTS_NAMES.NOTHING,
        OBJECTS_NAMES.WALL,
        OBJECTS_NAMES.ITEM,
        OBJECTS_NAMES.PLAYER,
        OBJECTS_NAMES.ENEMY,
      ],
      parameterType: "inputInputSelect",
      commands: [],
    };
  }
}

class IsNotObject extends CharacterCommand {
  constructor(nparam1, nparam2, nparam3) {
    super();
    this.param1 = nparam1;
    this.param2 = nparam2;
    this.param3 = nparam3;
  }
  translate() {
    return (
      "isNotObject " +
      this.param1.translate() +
      " " +
      this.param2.translate() +
      " " +
      this.param3.translate()
    );
  }
  generate(vm) {
    this.param1.generate(vm);
    this.param2.generate(vm);
    this.param3.generate(vm);
    vm.poke(INSTRUCTION_IS_NOT_OBJECT);
  }
  toJSON() {
    return {
      name: "isNotObject",
      representationName: "Nie je",
      order: o++,
      parameter1: this.param1.toJSON(),
      parameter2: this.param2.toJSON(),
      parameter3: this.param3.toJSON(),
      parameterType: "selectSelectSelect",
      options1: [
        OBJECTS_NAMES.NOTHING,
        OBJECTS_NAMES.WALL,
        OBJECTS_NAMES.ITEM,
        OBJECTS_NAMES.PLAYER,
        OBJECTS_NAMES.ENEMY,
      ],
      options2: [
        DIRECTIONS_NAMES.NULL,
        DIRECTIONS_NAMES.LEFT,
        DIRECTIONS_NAMES.RIGHT,
      ],
      options3: [
        DIRECTIONS_NAMES.NULL,
        DIRECTIONS_NAMES.UP,
        DIRECTIONS_NAMES.DOWN,
      ],
      commands: [],
    };
  }
}

class MoveToCell extends Syntax {
  constructor(nparam1, nparam2) {
    super();
    this.param1 = nparam1;
    this.param2 = nparam2;
  }
  translate() {
    return (
      "moveToCell " + this.param1.translate() + " " + this.param2.translate()
    );
  }
  generate(vm) {
    this.param1.generate(vm);
    this.param2.generate(vm);
    vm.poke(INSTRUCTION_MOVE_TO_CELL);
  }
  toJSON() {
    return {
      name: "moveToCell",
      representationName: "Posuň sa na políčko",
      order: o++,
      parameter1: this.param1.toJSON(),
      parameter2: this.param2.toJSON(),
      parameterType: "selectSelect",
      options1: [
        DIRECTIONS_NAMES.NULL,
        DIRECTIONS_NAMES.LEFT,
        DIRECTIONS_NAMES.RIGHT,
      ],
      options2: [
        DIRECTIONS_NAMES.NULL,
        DIRECTIONS_NAMES.UP,
        DIRECTIONS_NAMES.DOWN,
      ],
      commands: [],
    };
  }
}

class Repeat extends Syntax {
  constructor(ncount, nbody) {
    super();
    this.count = ncount;
    this.body = nbody;
    this.name = "counter";
  }
  translate() {
    return (
      "repeat " + this.count.translate() + "[" + this.body.translate() + "]"
    );
  }
  generate(vm) {
    if (this.body.items.length > 0) {
      this.count.generate(vm);
      vm.poke(INSTRUCTION_SET_COUNTER);
      var loopBody = vm.adr;
      this.body.generate(vm);
      vm.poke(INSTRUCTION_LOOP);
      vm.poke(loopBody);
    }
  }
  toJSON() {
    return {
      name: "repeat",
      representationName: "Opakuj",
      order: o++,
      type: "loop",
      parameter: this.count.toJSON(),
      parameterType: null,
      commands: { commands1: this.body != null ? this.body.toJSON() : [] },
    };
  }
}

class RepeatForever extends Syntax {
  constructor(nbody) {
    super();
    this.body = nbody;
  }
  translate() {
    return "repeatForever [" + this.body.translate() + "]";
  }
  generate(vm) {
    if (this.body.items.length > 0) {
      vm.poke(INSTRUCTION_JMPLOOP_FOREVER);
      var loop_body = vm.adr;
      this.body.generate(vm);
      vm.poke(INSTRUCTION_LOOP_FOREVER);
      vm.poke(loop_body);
    }
  }
  toJSON() {
    return {
      name: "repeatForever",
      representationName: "Opakuj stále",
      order: o++,
      type: "loop",
      parameter: null,
      parameterType: null,
      commands: { commands1: this.body != null ? this.body.toJSON() : [] },
    };
  }
}

class SetTimer extends Syntax {
  constructor(nvalue) {
    super();
    this.value = nvalue;
  }
  generate(vm) {
    this.value.generate(vm);
    vm.poke(INSTRUCTION_SET_TIMER);
  }
  translate() {
    return "setTimer " + this.value.translate();
  }
  toJSON() {
    return {
      name: "setTimer",
      representationName: "Nastav časovač na: ",
      order: o++,
      type: "general",
      parameter: this.value.toJSON(),
      parameterType: "input",
      paramMin: 1,
      paramMax: 1000,
      commands: [],
    };
  }
}

class IsTimer extends ItemCommand {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
  translate() {
    return "isTimer " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_IS_TIMER);
  }
  toJSON() {
    return {
      name: "isTimer",
      representationName: "Je časovač:",
      order: o++,
      type: "condition",
      parameter: this.param.toJSON(),
      parameterType: "input",
      paramMin: 0,
      paramMax: 1000,
      commands: [],
    };
  }
}

class PlaySound extends Syntax {
  constructor(nsoundId) {
    super();
    this.soundId = nsoundId;
  }
  generate(vm) {
    this.soundId.generate(vm);
    vm.poke(INSTRUCTION_PLAY_SOUND);
  }
  translate() {
    return "playSound " + this.soundId.translate();
  }
  toJSON() {
    return {
      name: "playSound",
      representationName: "Prehraj zvuk",
      order: o++,
      type: "sound",
      parameter: this.soundId != null ? this.soundId.toJSON() : null,
      parameterType: "select",
      options: gameScene.getCurrLevel().sounds,
      commands: [],
    };
  }
}

class StopSounds extends Syntax {
  generate(vm) {
    vm.poke(INSTRUCTION_STOP_SOUNDS);
  }
  translate() {
    return "stopSounds";
  }
  toJSON() {
    return {
      name: "stopSounds",
      representationName: "Zastav zvuky",
      order: o++,
      type: "general",
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class ShowTimer extends Syntax {
  translate() {
    return "showTimer";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_SHOW_TIMER);
  }
  toJSON() {
    return {
      name: "showTimer",
      representationName: "Zobraz časovač",
      order: o++,
      type: "general",
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class HideTimer extends Syntax {
  generate(vm) {
    vm.poke(INSTRUCTION_HIDE_TIMER);
  }
  translate() {
    return "hideTimer";
  }
  toJSON() {
    return {
      name: "hideTimer",
      representationName: "Skry časovač",
      order: o++,
      type: "general",
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class SetLevel extends Syntax {
  constructor(nparam) {
    super();
    this.param = nparam;
  }
  translate() {
    return "setLevel " + this.param.translate();
  }
  generate(vm) {
    this.param.generate(vm);
    vm.poke(INSTRUCTION_SET_LEVEL);
  }
  toJSON() {
    return {
      name: "setLevel",
      representationName: "Nastav level na:",
      order: o++,
      type: "level",
      parameter: this.param != null ? this.param.toJSON() : null,
      parameterType: "select",
      options: gameScene.levels,
      commands: [],
    };
  }
}

class ShowText extends Syntax {
  constructor(ntext) {
    super();
    this.text = ntext;
  }
  translate() {
    return "showText " + this.text.translate();
  }
  generate(vm) {
    this.text.generate(vm);
    vm.poke(INSTRUCTION_SHOW_TEXT);
  }
  toJSON() {
    return {
      name: "showText",
      representationName: "Zobraz text:",
      order: o++,
      parameter: this.text != null ? this.text.toJSON() : "",
      parameterType: "textarea",
      commands: [],
    };
  }
}

class RemoveText extends Syntax {
  translate() {
    return "removeText";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_REMOVE_TEXT);
  }
  toJSON() {
    return {
      name: "removeText",
      representationName: "Vymaž text",
      order: o++,
      parameter: null,
      parameterType: null,
      commands: [],
    };
  }
}

class While extends Syntax {
  constructor(ntest, nbody) {
    super();
    this.test = ntest;
    this.body = nbody;
  }
  generate(vm) {
    var testAdr = vm.adr;
    this.test.generate(vm);
    vm.poke(INSTRUCTION_JUMPIFFALSE);
    var jumpIns = vm.adr;
    vm.adr += 1;
    this.body.generate(vm);
    vm.poke(INSTRUCTION_JUMP);
    vm.poke(testAdr);
    vm.mem[jumpIns] = vm.adr;
  }
  translate() {
    return "while " + this.test.translate() + "[" + this.body.translate() + "]";
  }
  toJSON() {
    return {
      name: "while",
      representationName: "Kým",
      order: o++,
      type: "loop",
      parameter: null,
      parameterType: "while",
      options: [],
      commands: {
        commands1: this.test != null ? this.test.toJSON() : [],
        commands2: this.body != null ? this.body.toJSON() : [],
      },
    };
  }
}

class IfKeyPressed extends Listener {
  constructor(nparam, nbody, ninterruptParam) {
    super();
    this.param = nparam;
    this.body = nbody;
    this.interruptParam = ninterruptParam;
  }
  translate() {
    return (
      "ifKeyPressed " +
      this.interruptParam.translate() +
      " " +
      this.param.translate() +
      "[" +
      this.body.translate() +
      "]"
    );
  }
  generate(vm) {
    this.interruptParam.generate(vm);
    this.param.generate(vm);
    vm.poke(INSTRUCTION_IF_KEY_PRESSED);
    var jumpfalse_ins = vm.adr;
    vm.adr += 1;
    this.body.generate(vm);
    vm.mem[jumpfalse_ins] = vm.adr;
  }
  toJSON() {
    return {
      name: "ifKeyPressed",
      representationName: "Po stlačení klávesu",
      order: o++,
      type: "event",
      subtype: "keyPress",
      parameter: this.param.toJSON(),
      parameterType: "select",
      options: [
        KEY_LISTENER.LEFT,
        KEY_LISTENER.RIGHT,
        KEY_LISTENER.UP,
        KEY_LISTENER.DOWN,
        KEY_LISTENER.SPACE,
      ],
      interruptParam: this.interruptParam.toJSON(),
      interruptOptions: [
        INTERRUPT_OPTIONS.IGNORE,
        INTERRUPT_OPTIONS.WAIT,
        INTERRUPT_OPTIONS.RESTART,
        INTERRUPT_OPTIONS.BREAK,
      ],
      commands: { commands1: this.body != null ? this.body.toJSON() : [] },
    };
  }
}

class If extends Syntax {
  constructor(ntest, nbodytrue) {
    super();
    this.test = ntest;
    this.bodytrue = nbodytrue;
  }
  translate() {
    return (
      "if " + this.test.translate() + " [" + this.bodytrue.translate() + "]"
    );
  }
  generate(vm) {
    this.test.generate(vm);
    vm.poke(INSTRUCTION_JUMPIFFALSE);
    var jumpFalseIns = vm.adr;
    vm.adr += 1;
    this.bodytrue.generate(vm);
    vm.mem[jumpFalseIns] = vm.adr;
  }
  toJSON() {
    return {
      name: "if",
      representationName: "Ak tak",
      representationName1: "Ak",
      representationName2: "tak",
      order: o++,
      type: "condition",
      parameter: 1,
      parameterType: "nested",
      commands: {
        commands1: this.test != null ? this.test.toJSON() : [],
        commands2: this.bodytrue != null ? this.bodytrue.toJSON() : [],
      },
    };
  }
}

class IfElse extends Syntax {
  constructor(ntest, nbodytrue, nbodyfalse) {
    super();
    this.test = ntest;
    this.bodytrue = nbodytrue;
    this.bodyfalse = nbodyfalse;
  }
  translate() {
    return (
      "ifElse " +
      this.test.translate() +
      " [" +
      this.bodytrue.translate() +
      "] [" +
      this.bodyfalse.translate() +
      "]"
    );
  }
  generate(vm) {
    this.test.generate(vm);
    vm.poke(INSTRUCTION_JUMPIFFALSE);
    var jumpfalse_ins = vm.adr;
    vm.adr += 1;
    this.bodytrue.generate(vm);
    vm.poke(INSTRUCTION_JUMP);
    var jump_ins = vm.adr;
    vm.adr += 1;
    vm.mem[jumpfalse_ins] = vm.adr;
    this.bodyfalse.generate(vm);
    vm.mem[jump_ins] = vm.adr;
  }
  toJSON() {
    return {
      name: "ifElse",
      representationName: "Ak tak inak",
      representationName1: "Ak",
      representationName2: "tak",
      representationName3: "inak",
      order: o++,
      type: "condition",
      parameter: 1,
      parameterType: "nested",
      commands: {
        commands1: this.test != null ? this.test.toJSON() : [],
        commands2: this.bodytrue != null ? this.bodytrue.toJSON() : [],
        commands3: this.bodyfalse != null ? this.bodyfalse.toJSON() : [],
      },
    };
  }
}

class WhenStartClicked extends Event {
  constructor(nbody, ninterruptParam) {
    super();
    this.body = nbody;
    this.interruptParam = ninterruptParam;
  }
  translate() {
    return (
      "whenStartClicked " +
      this.interruptParam.translate() +
      "[" +
      this.body.translate() +
      "]"
    );
  }
  generate(vm) {
    this.interruptParam.generate(vm);
    vm.poke(INSTRUCTION_WHEN_START_CLICKED);
    this.body.generate(vm);
  }
  toJSON() {
    return {
      name: "whenStartClicked",
      representationName: "Po spustení",
      order: o++,
      type: "event",
      subtype: "startClick",
      parameter: null,
      interruptParam: this.interruptParam.toJSON(),
      interruptOptions: [
        INTERRUPT_OPTIONS.IGNORE,
        INTERRUPT_OPTIONS.WAIT,
        INTERRUPT_OPTIONS.RESTART,
        INTERRUPT_OPTIONS.BREAK,
      ],
      parameterType: null,
      commands: { commands1: this.body != null ? this.body.toJSON() : [] },
    };
  }
}

class Interruption extends Syntax {
  constructor(nbody) {
    super();
    this.body = nbody;
  }
  translate() {
    return "interruption [" + this.body.translate() + "]";
  }
  generate(vm) {
    vm.poke(INSTRUCTION_INTERRUPTION);
    this.body.generate(vm);
    vm.poke(INSTRUCTION_END_INTERRUPTION);
  }
  toJSON() {
    return {
      name: "interruption",
      representationName: "Prerušenie",
      order: o++,
      type: "condition",
      parameter: null,
      parameterType: null,
      commands: { commands1: this.body != null ? this.body.toJSON() : [] },
    };
  }
}

class Block extends Syntax {
  constructor(nitems) {
    super();
    this.items = nitems;
  }
  add(item) {
    this.items.push(item);
  }
  translate() {
    let result = "";
    for (let item of this.items) {
      result += item.translate() + " ";
    }
    return result;
  }
  generate(vm) {
    this.items.forEach((p) => {
      if (p instanceof Listener) {
        const vmListener = new VirtualMachineListener(vm.object);
        vmListener.generate(p);
      } else {
        p.generate(vm);
      }
    });
  }
  toJSON() {
    let result = [];
    for (let item of this.items) {
      result.push(item.toJSON());
    }
    return result;
  }
}

class CloneSprite extends Syntax {
  constructor() {
    super();
  }
  translate() {
    return "cloneSprite";
  }

  generate(vm) {
    vm.poke(INSTRUCTION_CLONE_SPRITE);
  }
  toJSON() {
    return {
      name: "cloneSprite",
      representationName: "Vytvor klon",
      order: o++,
      type: "general",
      parameter: null,
      parameterType: null,
      options: [],
      commands: [],
    };
  }
}

class Call extends Syntax {
  constructor(nsubr) {
    super();
    this.subr = nsubr;
  }
  generate(vm) {
    vm.poke(INSTRUCTION_CALL);
    vm.poke(this.subr.bodyadr);
  }
  translate() {
    return "" + this.subr.name;
  }
  toJSON() {
    return {
      name: "call",
      representationName: this.subr.name,
      order: o++,
      type: "callMySubprogram",
      parameter: this.subr.name,
      parameterType: null,
      options: [],
      commands: {
        commands1: this.subr.body != null ? [this.subr.body.toJSON()] : [],
      },
    };
  }
}

class Identifier extends Syntax {
  constructor(nname) {
    super();
    this.name = nname;
  }
}

class Subroutine extends Identifier {
  constructor(nbody, nName = "") {
    super();
    this.name = nName;
    this.body = nbody;
    this.bodyadr = 0;
  }
  translate() {
    return "define " + this.name + "[" + this.body.translate() + "]";
  }

  generate(vm) {
    vm.poke(INSTRUCTION_JUMP);
    vm.adr += 1;
    this.bodyadr = vm.adr;
    this.body.generate(vm);
    vm.poke(INSTRUCTION_RETURN);
    vm.mem[this.bodyadr - 1] = vm.adr;
  }
  toJSON() {
    return {
      name: "define",
      representationName: "definuj " + this.name,
      order: o++,
      type: "mySubprograms",
      parameter: this.name,
      parameterType: null,
      options: [],
      commands: { commands1: this.body != null ? this.body.toJSON() : [] },
    };
  }
}

class Variable extends Identifier {
  constructor(nname, npos) {
    super();
    this.name = nname;
    this.pos = npos;
  }

  generate(vm) {
    vm.poke(INSTRUCTION_GET);
    vm.poke(VMController.variables[this.name]);
  }
  translate() {
    return this.name;
  }
  toJSON() {
    return this.name;
  }
}

class GlobalVariable extends Variable {
  generate() {
    vm.poke(INSTRUCTION_GET);
    vm.poke(this.pos);
  }

  generate_set() {
    vm.poke(INSTRUCTION_SET);
    vm.poke(this.pos);
  }
}

class SetVariable extends Syntax {
  constructor(nname, nvalue) {
    super();

    this.variable = nname;
    this.value = nvalue;
    this.value = nvalue;

    if (!(nname instanceof Variable)) {
      var pos = Object.keys(VMController.variables).length;
      VMController.variables[nname] = pos;
      this.variable = new Variable(nname, pos);
    }
  }
  generate() {
    vm.poke(INSTRUCTION_SET);
    vm.poke(this.pos);
  }
  translate() {
    return "set " + this.variable.translate() + " " + this.value.translate();
  }
  toJSON() {
    return {
      name: "set",
      representationName: "Nastav",
      order: o++,
      parameter1: this.variable.toJSON(),
      parameter2: this.value.toJSON(),
      type: "variable",
      options: [],
      parameterType: "selectInput",
      commands: [],
    };
  }
}

class GetVariable extends Variable {
  generate() {
    vm.poke(INSTRUCTION_GET);
    vm.poke(this.pos);
  }
  translate() {
    return "get " + this.name.translate();
  }
  toJSON() {
    return {
      name: "get",
      representationName: this.name,
      order: o++,
      parameter: null,
      parameterType: null,
      type: "variable",
      options: [],
      commands: [],
    };
  }
}

class ChangeVariable extends Syntax {
  constructor(nvar, nvalue) {
    super();
    this.variable = nvar;
    this.value = nvalue;
  }
  generate(vm) {
    vm.poke(this.value);
    vm.poke(INSTRUCTION_CHANGE);
    vm.poke(this.pos);
  }
  translate() {
    return "change " + this.variable.name + " " + this.value.translate();
  }
  toJSON() {
    return {
      name: "change",
      representationName: "Zmeň o: ",
      order: o++,
      parameter1: this.variable.name,
      parameter2: this.value.toJSON(),
      type: "variable",
      parameterType: "selectInput",
      options: [],
      commands: [],
    };
  }
}

class ShowVariable extends Syntax {
  constructor(nname) {
    super();
    this.name = nname;
  }
  translate() {
    return "show " + this.name;
  }
  toJSON() {
    return {
      name: "show",
      representationName: "Zobraz",
      order: o++,
      parameter1: this.value.toJSON(),
      type: "variable",
      parameterType: null,
      options: [],
      commands: [],
    };
  }
  generate(vm) {
    vm.poke(INSTRUCTION_SHOW);
    vm.poke(this.pos);
  }
}

class HideVariable extends Syntax {
  constructor(nname) {
    super();
    this.name = nname;
  }
  translate() {
    return "hide " + this.name;
  }
  toJSON() {
    return {
      name: "hide",
      representationName: "Skry",
      order: o++,
      parameter1: this.value.toJSON(),
      type: "variable",
      parameterType: null,
      options: [],
      commands: [],
    };
  }
  generate() {
    vm.poke(INSTRUCTION_HIDE);
    vm.poke(this.pos);
  }
}

class ChasePlayer extends EnemyCommand {
  translate() {
    return "chasePlayer";
  }

  generate(vm) {
    vm.poke(INSTRUCTION_CHASE_PLAYER);
  }
  toJSON() {
    return {
      name: "chasePlayer",
      representationName: "Naháňaj hráča",
      order: o++,
      type: "general",
      parameter: null,
      parameterType: null,
      options: [],
      commands: [],
    };
  }
}

class StopChasePlayer extends EnemyCommand {
  translate() {
    return "stopChasePlayer";
  }

  generate(vm) {
    vm.poke(INSTRUCTION_STOP_CHASE_PLAYER);
  }
  toJSON() {
    return {
      name: "stopChasePlayer",
      representationName: "Prestaň naháňaj hráča",
      order: o++,
      type: "general",
      parameter: null,
      parameterType: null,
      options: [],
      commands: [],
    };
  }
}

class SyntaxTree {
  constructor(program) {
    this.lexicalAnalyser = new LexicalAnalyser(program);
  }

  parse(program) {
    let result = new Block([]);
    for (let command of program) {
      let parameter = command.parameter;
      let parameter1 = command.parameter1;
      let parameter2 = command.parameter2;
      let parameter3 = command.parameter3;
      let interruptParam = command.interruptParam;

      let name = command.name;
      let c1 = command.commands.commands1;
      let c2 = command.commands.commands2;
      let c3 = command.commands.commands3;

      switch (name) {
        case "up":
          result.add(new Up(new Const(parseInt(parameter))));
          break;
        case "down":
          result.add(new Down(new Const(parseInt(parameter))));
          break;
        case "left":
          result.add(new Lt(new Const(parseInt(parameter))));
          break;
        case "right":
          result.add(new Rt(new Const(parseInt(parameter))));
          break;
        case "turnLeft":
          result.add(new TurnLt());
          break;
        case "turnRight":
          result.add(new TurnRt());
          break;
        case "increaseX":
          result.add(new IncX(new Const(parseInt(parameter))));
          break;
        case "increaseY":
          result.add(new IncY(new Const(parseInt(parameter))));
          break;
        case "decreaseX":
          result.add(new DecX(new Const(parseInt(parameter))));
          break;
        case "decreaseY":
          result.add(new DecY(new Const(parseInt(parameter))));
          break;
        case "define":
          const subr = new Subroutine(this.parse(c1), parameter);
          globals[parameter] = subr;
          result.add(subr);
          break;
        case "setX":
          result.add(new SetX(new Const(parseInt(parameter))));
          break;
        case "setY":
          result.add(new SetY(new Const(parseInt(parameter))));
          break;
        case "setR":
          result.add(new SetR(new Const(parseInt(parameter))));
          break;
        case "setC":
          result.add(new SetC(new Const(parseInt(parameter))));
          break;
        case "setRC":
          result.add(
            new SetRC(
              new Const(parseInt(parameter1)),
              new Const(parseInt(parameter2))
            )
          );
          break;
        case "setExecutionSpeed":
          result.add(new SetExecutionSpeed(new Const(parseInt(parameter))));
          break;
        case "stopExecution":
          result.add(new StopExecution());
          break;
        case "stopGame":
          result.add(new StopGame());
          break;
        case "set":
          result.add(
            new SetVariable(
              new Variable(parameter1),
              new Const(parseInt(parameter2))
            )
          );
          break;
        case "get":
          result.add(new GetVariable(new Variable(parameter)));
          break;
        case "change":
          result.add(
            new ChangeVariable(
              new Variable(parameter1),
              new Const(parseInt(parameter2))
            )
          );
          break;
        case "isScore":
          result.add(new IsScore(new Const(parseInt(parameter))));
          break;
        case "setScore":
          result.add(new SetScore(new Const(parseInt(parameter))));
          break;
        case "changeScore":
          result.add(new ChangeScore(new Const(parseInt(parameter))));
          break;
        case "wait":
          result.add(new Wait(new Const(parseInt(parameter))));
          break;
        case "playSound":
          result.add(new PlaySound(new Const(parseInt(parameter))));
          break;
        case "stopSounds":
          result.add(new StopSounds());
          break;
        case "isRowCol":
          result.add(
            new IsRowCol(
              new Const(parseInt(parameter1)),
              new Const(parseInt(parameter1))
            )
          );
          break;
        case "isRowColObject":
          result.add(
            new IsRowColObject(
              new Const(parseInt(parameter1)),
              new Const(parseInt(parameter2)),
              new ObjectType(parameter3)
            )
          );
          break;
        case "moveObjectTo":
          result.add(
            new MoveObjectTo(
              new ObjectType(parameter1),
              new Const(parseInt(parameter2)),
              new Const(parseInt(parameter3))
            )
          );
          break;
        case "moveObjectBy":
          result.add(
            new MoveObjectBy(
              new ObjectType(parameter1),
              new Const(parseInt(parameter2)),
              new Const(parseInt(parameter3))
            )
          );
          break;
        case "isObject":
          result.add(
            new IsObject(
              new ObjectType(parameter1),
              new Direction(parameter2),
              new Direction(parameter3)
            )
          );
          break;
        case "isNotObject":
          result.add(
            new IsNotObject(
              new ObjectType(parameter1),
              new Direction(parameter2),
              new Direction(parameter3)
            )
          );
          break;
        case "moveToCell":
          result.add(
            new MoveToCell(new Direction(parameter1), new Direction(parameter2))
          );
          break;
        case "isCollisionWith":
          result.add(new IsCollisionWith(new ObjectType(parameter)));
          break;
        case "ifKeyPressed":
          result.add(
            new IfKeyPressed(
              new Key(parameter),
              this.parse(c1),
              new InterruptHandler(interruptParam)
            )
          );
          break;
        case "while":
          result.add(new While(this.parse(c1), this.parse(c2)));
          break;
        case "if":
          result.add(new If(this.parse(c1), this.parse(c2)));
          break;
        case "ifElse":
          result.add(
            new IfElse(this.parse(c1), this.parse(c2), this.parse(c3))
          );
          break;
        case "repeat":
          result.add(
            new Repeat(new Const(parseInt(parameter)), this.parse(c1))
          );
          break;
        case "repeatForever":
          result.add(new RepeatForever(this.parse(c1)));
          break;
        case "deleteObject":
          result.add(new DeleteObject());
          break;
        case "chasePlayer":
          result.add(new ChasePlayer());
          break;
        case "stopChasePlayer":
          result.add(new StopChasePlayer());
          break;
        case "cloneSprite":
          result.add(new CloneSprite());
          break;
        case "showText":
          result.add(new ShowText(new Text(parameter)));
          break;
        case "removeText":
          result.add(new RemoveText());
          break;
        case "hideObject":
          result.add(new HideObject());
          break;
        case "showObject":
          result.add(new ShowObject());
          break;
        case "startAnimation":
          result.add(new StartAnimation());
          break;
        case "stopAnimation":
          result.add(new StopAnimation());
          break;
        case "interruption":
          result.add(new Interruption(this.parse(c1)));
          break;
        case "whenStartClicked":
          result.add(
            new WhenStartClicked(
              this.parse(c1),
              new InterruptHandler(interruptParam)
            )
          );
          break;
        default:
          let fun = new Call(globals[parameter]);
          COMMANDS_TYPE.mySubprograms.push(fun.toJSON());
          result.add(fun);
      }
    }

    return result;
  }
}

var globals = {};
var locals;
var globalvarpos;
var localvarpos;
var returnVariable;

const NOTHING = 0;
const NUMBER = 1;
const WORD = 2;
const SYMBOL = 3;
const DOUBLE = 4;
const KEY = 5;
const TEXT = 6;
const DIRECTION = 7;
const INTERRUPT_HANDLER = 8;
const OBJECT_TYPE = 9;
const MOVE_OPTION = 10;
const NEGATIVE_NUMBER = 11;

var token = "";
var kind = -1;
var position = 0;

class Compiler {
  constructor(input) {
    this.lexicalAnalyser = new LexicalAnalyser(input);
    this.table = new Array(64);

    this.initializeTable();
  }

  inGlobals(token) {
    if (globals[token]) {
      return true;
    }
    return false;
  }
  next() {
    this.lexicalAnalyser.next();
  }

  scan() {
    this.lexicalAnalyser.scan();
  }

  initializeTable() {
    for (let i = 0; i++; i < 64) {
      this.table[i] = this.parse_other;
    }
    this.setItem("left", this.parse_left);
    this.setItem("right", this.parse_right);
    this.setItem("turnLeft", this.parse_turnLeft);
    this.setItem("turnRight", this.parse_turnRight);
    this.setItem("up", this.parse_up);
    this.setItem("down", this.parse_down);
    this.setItem("increaseX", this.parse_increaseX);
    this.setItem("increaseY", this.parse_increaseY);
    this.setItem("decreaseX", this.parse_decreaseX);
    this.setItem("decreaseY", this.parse_decreaseY);
    this.setItem("setX", this.parse_setX);
    this.setItem("setY", this.parse_setY);
    this.setItem("setR", this.parse_setR);
    this.setItem("setC", this.parse_setC);
    this.setItem("setRC", this.parse_setRC);
    this.setItem("ifKeyPressed", this.parse_ifKeyPressed);
    this.setItem("repeat", this.parse_repeat);
    this.setItem("cloneSprite", this.parse_cloneSprite);
    this.setItem("deleteObject", this.parse_deleteObject);
    this.setItem("moveToCell", this.parse_moveToCell);
    this.setItem("isObject", this.parse_isObject);
    this.setItem("isNotObject", this.parse_isNotObject);
    this.setItem("chasePlayer", this.parse_chasePlayer);
    this.setItem("stopChasePlayer", this.parse_stopChasePlayer);
    this.setItem("define", this.parse_definition);
    this.setItem("hideObject", this.parse_hideObject);
    this.setItem("showObject", this.parse_showObject);
    this.setItem("isCollisionWith", this.parse_isCollisionWith);
    this.setItem("repeatForever", this.parse_repeatForever);
    this.setItem("wait", this.parse_wait);
    this.setItem("hide", this.parse_hideVar);
    this.setItem("show", this.parse_showVar);
    this.setItem("change", this.parse_changeVar);
    this.setItem("set", this.parse_setVar);
    this.setItem("get", this.parse_getVar);
    this.setItem("showText", this.parse_showText);
    this.setItem("removeText", this.parse_removeText);
    this.setItem("playSound", this.parse_playSound);
    this.setItem("stopSounds", this.parse_stopSounds);
    this.setItem("setLevel", this.parse_setLevel);
    this.setItem("whenStartClicked", this.parse_whenStartClicked);
    this.setItem("while", this.parse_while);
    this.setItem("if", this.parse_if);
    this.setItem("ifElse", this.parse_ifElse);
    this.setItem("interruption", this.parse_interruption);
    this.setItem("setScore", this.parse_setScore);
    this.setItem("isScore", this.parse_isScore);
    this.setItem("changeScore", this.parse_changeScore);
    this.setItem("setExecutionSpeed", this.parse_setExecutionSpeed);
    this.setItem("stopExecution", this.parse_stopExecution);
    this.setItem("stopGame", this.parse_stopGame);
    this.setItem("isRowCol", this.parse_isRowCol);
    this.setItem("moveObjectTo", this.parse_moveObjectTo);
    this.setItem("moveObjectBy", this.parse_moveObjectBy);
    this.setItem("isRowColObject", this.parse_isRowColObject);
    this.setItem("startAnimation", this.parse_startAnimation);
    this.setItem("stopAnimation", this.parse_stopAnimation);
    this.setItem("isTimer", this.parse_isTimer);
    this.setItem("setTimer", this.parse_setTimer);
  }

  getItem(key) {
    const idx = this.hash(key);
    if (!this.table[idx]) {
      this.setItem(key, this.parse_other);
    }
    const item = this.table[idx].find((x) => x[0] === key);
    if (item == null) {
      return this.table[idx][0][1];
    }
    return item[1];
  }

  setItem(key, value) {
    const idx = this.hash(key);
    if (this.table[idx]) {
      this.table[idx].push([key, value]);
    } else {
      this.table[idx] = [[key, value]];
    }
  }

  parse_interruption(result, self) {
    if (self.lexicalAnalyser.token != "interruption") {
      self.parse_other(result);
    } else {
      self.lexicalAnalyser.scan();
      self.check(SYMBOL, "[");
      self.lexicalAnalyser.scan();
      result.add(new Interruption(self.parse()));
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
    }
  }

  parse_while(result, self) {
    if (self.lexicalAnalyser.token != "while") {
      self.parse_other(result);
    } else {
      self.lexicalAnalyser.scan();
      var test = self.parse();
      self.check(SYMBOL, "[");
      self.lexicalAnalyser.scan();
      result.add(new While(test, self.parse()));
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
    }
  }

  parse_ifKeyPressed(result, self) {
    if (self.lexicalAnalyser.token != "ifKeyPressed") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      var interruptParam = self.expr();
      var param = self.expr();
      self.check(SYMBOL, "[");
      self.lexicalAnalyser.scan();
      var body = self.parse();
      var ifKeyPressed = new IfKeyPressed(param, body, interruptParam);
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
      result.add(ifKeyPressed);
    }
  }

  parse_whenStartClicked(result, self) {
    if (self.lexicalAnalyser.token != "whenStartClicked") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      var interruptParam = self.expr();
      self.check(SYMBOL, "[");
      self.lexicalAnalyser.scan();
      var startClicked = new WhenStartClicked(self.parse(), interruptParam);
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
      result.add(startClicked);
    }
  }

  parse_showObject(result, self) {
    if (self.lexicalAnalyser.token != "showObject") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new ShowObject());
    }
  }

  parse_startAnimation(result, self) {
    if (self.lexicalAnalyser.token != "startAnimation") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new StartAnimation());
    }
  }

  parse_stopAnimation(result, self) {
    if (self.lexicalAnalyser.token != "stopAnimation") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new StopAnimation());
    }
  }

  parse_hideObject(result, self) {
    if (self.lexicalAnalyser.token != "hideObject") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new HideObject());
    }
  }

  parse_hideVar(result, self) {
    if (self.lexicalAnalyser.token != "hide") {
      self.parse_other(result, self);
    } else {
      self.scan();
      var variable = self.expr();
      self.scan();
      result.add(new HideVariable(variable));
    }
  }

  parse_showVar(result, self) {
    if (self.lexicalAnalyser.token != "show") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new ShowVariable(self.expr()));
    }
  }

  parse_setVar(result, self) {
    if (self.lexicalAnalyser.token != "set") {
      self.parse_other(result, self);
    } else {
      self.scan();
      var name = self.expr();
      var value = self.expr();
      result.add(new SetVariable(name, value));
    }
  }

  parse_getVar(result, self) {
    if (self.lexicalAnalyser.token != "get") {
      self.parse_other(result, self);
    } else {
      self.scan();
      var variable = self.expr();
      self.scan();
      result.add(new GetVariable(variable));
    }
  }

  parse_changeVar(result, self) {
    if (self.lexicalAnalyser.token != "change") {
      self.parse_other(result, self);
    } else {
      self.scan();
      var variable = self.expr();
      var value = self.expr();
      result.add(new ChangeVariable(variable, value));
    }
  }

  parse_isScore(result, self) {
    if (self.lexicalAnalyser.token != "isScore") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new IsScore(self.expr()));
    }
  }

  parse_setScore(result, self) {
    if (self.lexicalAnalyser.token != "setScore") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new SetScore(self.expr()));
    }
  }

  parse_changeScore(result, self) {
    if (self.lexicalAnalyser.token != "changeScore") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new ChangeScore(self.expr()));
    }
  }

  parse_isTimer(result, self) {
    if (self.lexicalAnalyser.token != "isTimer") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new IsTimer(self.expr()));
    }
  }

  parse_setTimer(result, self) {
    if (self.lexicalAnalyser.token != "setTimer") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new SetTimer(self.expr()));
    }
  }

  parse_setExecutionSpeed(result, self) {
    if (self.lexicalAnalyser.token != "setExecutionSpeed") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new SetExecutionSpeed(self.expr()));
    }
  }

  parse_stopExecution(result, self) {
    if (self.lexicalAnalyser.token != "stopExecution") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new StopExecution());
    }
  }

  parse_stopGame(result, self) {
    if (self.lexicalAnalyser.token != "stopGame") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new StopGame());
    }
  }

  parse_setLevel(result, self) {
    if (self.lexicalAnalyser.token != "setLevel") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new SetLevel(self.expr()));
    }
  }

  parse_showText(result, self) {
    if (self.lexicalAnalyser.token != "showText") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      const txt = self.expr();
      result.add(new ShowText(txt));
    }
  }

  parse_removeText(result, self) {
    if (self.lexicalAnalyser.token != "removeText") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new RemoveText());
    }
  }

  parse_chasePlayer(result, self) {
    if (self.lexicalAnalyser.token != "chasePlayer") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new ChasePlayer());
    }
  }

  parse_stopChasePlayer(result, self) {
    if (self.lexicalAnalyser.token != "stopChasePlayer") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new StopChasePlayer());
    }
  }

  parse_notWall(result, self) {
    if (self.lexicalAnalyser.token != "notWall") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      result.add(new NotWall(self.expr()));
    }
  }

  parse_moveToCell(result, self) {
    if (self.lexicalAnalyser.token != "moveToCell") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      result.add(new MoveToCell(self.expr(), self.expr()));
    }
  }

  parse_isRowCol(result, self) {
    if (self.lexicalAnalyser.token != "isRowCol") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      result.add(new IsRowCol(self.expr(), self.expr()));
    }
  }

  parse_moveObjectTo(result, self) {
    if (self.lexicalAnalyser.token != "moveObjectTo") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      result.add(new MoveObjectTo(self.expr(), self.expr(), self.expr()));
    }
  }

  parse_moveObjectBy(result, self) {
    if (self.lexicalAnalyser.token != "moveObjectBy") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      result.add(new MoveObjectBy(self.expr(), self.expr(), self.expr()));
    }
  }

  parse_isRowColObject(result, self) {
    if (self.lexicalAnalyser.token != "isRowColObject") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      result.add(new IsRowColObject(self.expr(), self.expr(), self.expr()));
    }
  }

  parse_isObject(result, self) {
    if (self.lexicalAnalyser.token != "isObject") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      result.add(new IsObject(self.expr(), self.expr(), self.expr()));
    }
  }

  parse_isNotObject(result, self) {
    if (self.lexicalAnalyser.token != "isNotObject") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      result.add(new IsNotObject(self.expr(), self.expr(), self.expr()));
    }
  }

  parse_isCollisionWith(result, self) {
    if (self.lexicalAnalyser.token != "isCollisionWith") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      result.add(new IsCollisionWith(self.expr()));
    }
  }

  parse_repeat(result, self) {
    if (self.lexicalAnalyser.token != "repeat") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      var param = self.expr();
      self.check(SYMBOL, "[");
      self.lexicalAnalyser.scan();
      var repeat = new Repeat(param, self.parse());
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
      result.add(repeat);
    }
  }

  parse_repeatForever(result, self) {
    if (self.lexicalAnalyser.token != "repeatForever") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      self.check(SYMBOL, "[");
      self.lexicalAnalyser.scan();
      var repeat = new RepeatForever(self.parse());
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
      result.add(repeat);
    }
  }

  parse_definition(result, self) {
    if (self.lexicalAnalyser.token != "define") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      self.check(WORD);
      let name = self.lexicalAnalyser.token;
      self.lexicalAnalyser.scan();
      let res = new Subroutine(null, name);
      globals[name] = res;
      self.check(SYMBOL, "[");
      self.lexicalAnalyser.scan();
      res.body = self.parse();
      result.add(res);
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
    }
  }

  parse_other(result, self) {
    let name = self.lexicalAnalyser.token;
    self.lexicalAnalyser.scan();
    result.add(self.parse_call(name, self));
  }

  parse_cloneSprite(result, self) {
    if (self.lexicalAnalyser.token != "cloneSprite") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new CloneSprite());
    }
  }

  parse_deleteObject(result, self) {
    if (self.lexicalAnalyser.token != "deleteObject") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new DeleteObject());
    }
  }

  parse_left(result, self) {
    if (self.lexicalAnalyser.token != "left") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new Lt(self.expr()));
    }
  }

  parse_right(result, self) {
    if (self.lexicalAnalyser.token != "right") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new Rt(self.expr()));
    }
  }

  parse_turnLeft(result, self) {
    if (self.lexicalAnalyser.token != "turnLeft") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new TurnLt());
    }
  }

  parse_turnRight(result, self) {
    if (self.lexicalAnalyser.token != "turnRight") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new TurnRt());
    }
  }

  parse_increaseX(result, self) {
    if (self.lexicalAnalyser.token != "increaseX") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new IncX(self.expr()));
    }
  }

  parse_increaseY(result, self) {
    if (self.lexicalAnalyser.token != "increaseY") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new IncY(self.expr()));
    }
  }

  parse_decreaseX(result, self) {
    if (self.lexicalAnalyser.token != "decreaseX") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new DecX(self.expr()));
    }
  }

  parse_decreaseY(result, self) {
    if (self.lexicalAnalyser.token != "decreaseY") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new DecY(self.expr()));
    }
  }

  parse_setX(result, self) {
    if (self.lexicalAnalyser.token != "setX") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new SetX(self.expr()));
    }
  }

  parse_setY(result, self) {
    if (self.lexicalAnalyser.token != "setY") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new SetY(self.expr()));
    }
  }

  parse_setR(result, self) {
    if (self.lexicalAnalyser.token != "setR") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new SetR(self.expr()));
    }
  }

  parse_setC(result, self) {
    if (self.lexicalAnalyser.token != "setC") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new SetC(self.expr()));
    }
  }

  parse_setRC(result, self) {
    if (self.lexicalAnalyser.token != "setRC") {
      self.parse_other(result, self);
    } else {
      self.scan();
      var param1 = self.expr();
      var param2 = self.expr();
      result.add(new SetRC(param1, param2));
    }
  }

  parse_wait(result, self) {
    if (self.lexicalAnalyser.token != "wait") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new Wait(self.expr()));
    }
  }

  parse_playSound(result, self) {
    if (self.lexicalAnalyser.token != "playSound") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new PlaySound(self.expr()));
    }
  }

  parse_stopSounds(result, self) {
    if (self.lexicalAnalyser.token != "stopSounds") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new StopSounds());
    }
  }

  parse_up(result, self) {
    if (self.lexicalAnalyser.token != "up") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new Up(self.expr()));
    }
  }

  parse_down(result, self) {
    if (self.lexicalAnalyser.token != "down") {
      self.parse_other(result, self);
    } else {
      self.scan();
      result.add(new Down(self.expr()));
    }
  }

  parse_if(result, self) {
    if (self.lexicalAnalyser.token != "if") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      var test = self.parse();
      self.check(SYMBOL, "[");
      self.lexicalAnalyser.scan();
      result.add(new If(test, self.parse()));
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
    }
  }

  parse_ifElse(result, self) {
    if (self.lexicalAnalyser.token != "ifElse") {
      self.parse_other(result, self);
    } else {
      self.lexicalAnalyser.scan();
      var test = self.parse();
      self.check(SYMBOL, "[");
      self.lexicalAnalyser.scan();
      var ifelse = new IfElse(test, self.parse(), null);
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
      self.lexicalAnalyser.scan();
      ifelse.bodyfalse = self.parse();
      self.check(SYMBOL, "]");
      self.lexicalAnalyser.scan();
      result.add(ifelse);
    }
  }

  parse() {
    let result = new Block([]);
    this.lexicalAnalyser.kind = 2;
    while (this.lexicalAnalyser.kind == WORD) {
      if (this.lexicalAnalyser.token.length < 2) {
        this.parse_other(result, this);
      } else {
        const func = this.getItem(this.lexicalAnalyser.token);
        func(result, this);
      }
    }
    return result;
  }

  parse_call(name) {
    if (!inGlobals(name)) {
      new Error("Neznámy príkaz " + name);
    }
    if (!(globals[name] instanceof Subroutine)) {
      new Error(name + "nie je podprogram");
    }
    let subr = globals[name];
    let fun = new Call(subr);
    addToSubprograms(subr.toJSON());
    addToSubprograms(fun.toJSON());
    return fun;
  }

  expr() {
    let result;
    if (this.lexicalAnalyser.kind == NUMBER) {
      result = new Const(parseInt(this.lexicalAnalyser.token));
    } else if (this.lexicalAnalyser.kind == NEGATIVE_NUMBER) {
      let token = this.lexicalAnalyser.token.substring(1);
      result = new Negative(new Const(parseInt(token)));
    } else if (this.lexicalAnalyser.kind == KEY) {
      result = new Key(this.lexicalAnalyser.token);
    } else if (this.lexicalAnalyser.kind == TEXT) {
      result = new Text(this.lexicalAnalyser.token);
    } else if (this.lexicalAnalyser.kind == DIRECTION) {
      result = new Direction(this.lexicalAnalyser.token);
    } else if (this.lexicalAnalyser.kind == INTERRUPT_HANDLER) {
      result = new InterruptHandler(this.lexicalAnalyser.token);
    } else if (this.lexicalAnalyser.kind == OBJECT_TYPE) {
      result = new ObjectType(this.lexicalAnalyser.token);
    } else if (this.lexicalAnalyser.kind == MOVE_OPTION) {
      result = new MoveOption(this.lexicalAnalyser.token);
    } else if (this.lexicalAnalyser.kind == WORD) {
      if (!VMController.inVariables(this.lexicalAnalyser.token)) {
        result = this.lexicalAnalyser.token;
      } else {
        result = new Variable(this.lexicalAnalyser.token);
      }
    }
    this.lexicalAnalyser.scan();
    return result;
  }

  hash(text) {
    let hash = 0;
    for (let i = 0; i < text.length; i++) {
      hash += text.charCodeAt(i);
    }
    return hash % this.table.length;
  }

  ord(str) {
    return str.charCodeAt(0);
  }

  check(expectedKind, expectedToken = null) {
    if (this.lexicalAnalyser.token == "x" && expectedKind == NUMBER) {
      new Error("Nie je cislo");
      return false;
    } else {
      if (this.lexicalAnalyser.kind != expectedKind) {
        new Error("Ocakaval som druh " + expectedKind);
        return false;
      }
      if (
        expectedToken != null &&
        this.lexicalAnalyser.token != expectedToken
      ) {
        new Error("Ocakaval som token " + expectedToken);
        return false;
      }
    }
    return true;
  }
}

// Trieda chyba
class Error {
  constructor(text) {
    this.text = text;
    this.log();
  }
  log() {
    console.log("Chyba " + this.text);
  }
}

function inGlobals(token) {
  if (globals[token]) {
    return true;
  }
  return false;
}

function inLocals(token) {
  if (locals[token]) {
    return true;
  }
  return false;
}

class LexicalAnalyser {
  constructor(input) {
    this.index = 0;
    this.look = "";
    this.input = input;

    this.token = "";
    this.kind = -1;
    this.position = 0;
    this.next();
    this.scan();
  }

  isKey(str) {
    return Object.values(KEY_LISTENER).includes(str);
  }

  isDirection(str) {
    return Object.values(DIRECTIONS_NAMES).includes(str);
  }

  isObjectType(str) {
    return Object.values(OBJECTS_NAMES).includes(str);
  }

  isInterruptHandler(str) {
    return Object.values(INTERRUPT_OPTIONS).includes(str);
  }

  isDigit(str) {
    return str.length === 1 && str.match(/[0-9]/i);
  }

  isLetter(str) {
    return str.length === 1 && str.match(/[a-zA-ZšŠčČťŤžŽýÝáÁľĽíÍéÉďĎ_\-?!]/i);
  }

  next() {
    if (this.index >= this.input.length) {
      this.look = "\n".charCodeAt(0);
    } else {
      this.look = this.input[this.index];
      this.index += 1;
    }
  }

  log() {
    console.log("pozicia  druh  token");
    console.log("=====================");
    while (this.kind != NOTHING) {
      console.log(this.position + " " * 8 + this.kind + " " * 5 + this.token);
      this.scan();
    }
  }

  scan() {
    while (this.look == " " || this.look == "\n") {
      this.next();
    }
    this.token = "";
    this.position = this.index - 1;
    if (this.look == "-") {
      this.token += this.look;
      this.next();
      while (this.isDigit(this.look)) {
        this.token += this.look;
        this.next();
      }
      this.kind = NEGATIVE_NUMBER;
    } else if (this.isDigit(this.look)) {
      while (this.isDigit(this.look)) {
        this.token += this.look;
        this.next();
      }
      this.kind = NUMBER;
    } else if (this.isLetter(this.look)) {
      while (this.isLetter(this.look)) {
        this.token += this.look;
        this.next();
      }
      this.kind = WORD;
      if (this.isKey(this.token)) {
        this.kind = KEY;
      }
      if (this.isDirection(this.token)) {
        this.kind = DIRECTION;
      }
      if (this.isObjectType(this.token)) {
        this.kind = OBJECT_TYPE;
      }
      if (this.isInterruptHandler(this.token)) {
        this.kind = INTERRUPT_HANDLER;
      }
    } else if (this.look == '"') {
      this.next();

      while (this.isLetter(this.look) || this.look == " ") {
        this.token += this.look;
        this.next();
      }
      this.next();

      this.kind = TEXT;
    } else if (this.look != "\n".charCodeAt(0)) {
      if (this.isDirection(this.look)) {
        this.kind = DIRECTION;
      } else {
        this.kind = SYMBOL;
      }
      this.token += this.look;
      this.next();
    } else {
      this.kind = NOTHING;
    }
  }
}

// Premenne virtualneho stroja
const INSTRUCTION_EMPTY = 0;
const INSTRUCTION_TURN_LT = 1;
const INSTRUCTION_TURN_RT = 2;
const INSTRUCTION_FD = 3;
const INSTRUCTION_DOWN = 4;
const INSTRUCTION_UP = 5;
const INSTRUCTION_LT = 6;
const INSTRUCTION_RT = 7;
const INSTRUCTION_LOOP = 8;

const INSTRUCTION_PUSH = 9;
const INSTRUCTION_INC_X = 10;
const INSTRUCTION_INC_Y = 11;
const INSTRUCTION_DEC_X = 12;
const INSTRUCTION_DEC_Y = 13;
const INSTRUCTION_SET_X = 14;
const INSTRUCTION_SET_Y = 15;

const INSTRUCTION_GET = 35;
const INSTRUCTION_SET = 36;
const INSTRUCTION_PRINT = 37;
const INSTRUCTION_JUMP = 38;
const INSTRUCTION_SET_COUNTER = 39;

const INSTRUCTION_JUMPIFFALSE = 40;
const INSTRUCTION_CALL = 41;
const INSTRUCTION_RETURN = 42;

const INSTRUCTION_GETLOCAL = 43;
const INSTRUCTION_SETLOCAL = 44;

const INSTRUCTION_LOOP_COUNTER = 45;
const INSTRUCTION_FOR_LOOP = 46;
const INSTRUCTION_RETURN_RESULT = 47;

const INSTRUCTION_CLONE_SPRITE = 50;
const INSTRUCTION_DEL_SPRITE = 51;
const INSTRUCTION_IF_KEY_PRESSED = 52;
const INSTRUCTION_IS_WALL = 53;
const INSTRUCTION_EQUAL = 54;
const INSTRUCTION_DIFFERENT = 55;
const INSTRUCTION_AND = 56;
const INSTRUCTION_OR = 57;
const INSTRUCTION_NOT = 58;
const INSTRUCTION_CHASE_PLAYER = 59;
const INSTRUCTION_STOP_CHASE_PLAYER = 60;
const INSTRUCTION_SHOW_OBJECT = 61;
const INSTRUCTION_HIDE_OBJECT = 62;
const INSTRUCTION_IS_COLLISION_WITH = 63;
const INSTRUCTION_JMPLOOP_FOREVER = 64;
const INSTRUCTION_LOOP_FOREVER = 65;
const INSTRUCTION_WAIT = 66;
const INSTRUCTION_SPRITE_CLICKED = 67;
const INSTRUCTION_SHOW_TEXT = 68;
const INSTRUCTION_REMOVE_TEXT = 69;
const INSTRUCTION_PLAY_SOUND = 70;
const INSTRUCTION_STOP_SOUNDS = 71;
const INSTRUCTION_IS_TIMER = 72;
const INSTRUCTION_IS_SCORE = 73;
const INSTRUCTION_SET_LEVEL = 74;
const INSTRUCTION_CHANGE = 75;
const INSTRUCTION_SHOW = 76;
const INSTRUCTION_HIDE = 77;
const INSTRUCTION_SET_R = 78;
const INSTRUCTION_SET_C = 79;
const INSTRUCTION_SET_RC = 80;
const INSTRUCTION_IS_OBJECT = 81;
const INSTRUCTION_IS_NOT_OBJECT = 82;
const INSTRUCTION_MOVE_TO_CELL = 83;
const INSTRUCTION_WHEN_START_CLICKED = 84;
const INSTRUCTION_INTERRUPTION = 85;
const INSTRUCTION_SET_INTER_HANDLER = 86;
const INSTRUCTION_END_INTERRUPTION = 87;
const INSTRUCTION_SET_EXECUTION_SPEED = 88;
const INSTRUCTION_IS_ROW_COL = 89;
const INSTRUCTION_SET_SCORE = 90;
const INSTRUCTION_CHANGE_SCORE = 91;
const INSTRUCTION_NEGATIVE = 92;
const INSTRUCTION_SET_TIMER = 93;
const INSTRUCTION_STOP_EXECUTION = 94;
const INSTRUCTION_STOP_GAME = 95;
const INSTRUCTION_IS_ROW_COL_OBJECT = 96;
const INSTRUCTION_MOVE_OBJECT_TO = 97;
const INSTRUCTION_MOVE_OBJECT_BY = 98;
const INSTRUCTION_SET_LOOP_COUNTERS = 99;
const INSTRUCTION_ADD = 100;

// Trieda virtualnej masiny
class VirtualMachine {
  constructor(object, type = null) {
    this.object = object;
    this.body_adr = 0;
    this.mem = new Array(10000);
    this.pc = 0;
    this.adr = 0;
    this.peak = 0;
    this.type = type;
    this.frame = 0;
    this.status = STATUSES.TERMINATED;
    this.totalDeltaTime = 0;
    this.speed = fps;
    this.fps = fps;
    this.interval = 1000;
    this.id = null;
    this.delta = 1;
    this.time = 0;
    VMController.add(this);
  }
  interrupt() {
    if (this.interruptHandler == INTERRUPT_OPTIONS.WAIT) {
      this.status = STATUSES.SLEEPING;
      return;
    }
    if (this.interruptHandler == INTERRUPT_OPTIONS.BREAK) {
      this.status = STATUSES.TERMINATED;
      this.pc = 0;
      return;
    }
    if (this.interruptHandler == INTERRUPT_OPTIONS.RESTART) {
      this.pc = 0;
      return;
    }
  }
  endInterruption() {
    if (this.interruptHandler != INTERRUPT_OPTIONS.BREAK) {
      this.status = STATUSES.RUNNING;
    }
  }
  run() {
    this.status = STATUSES.RUNNING;
  }
  stop() {
    this.status = STATUSES.TERMINATED;
  }
  executeStep(deltaTime) {
    this.time += this.delta;
    if (this.time > 0 && this.status == STATUSES.RUNNING) {
      this.totalDeltaTime += deltaTime;
      if (this.totalDeltaTime <= this.interval / this.speed) {
        return;
      }
      this.totalDeltaTime = 0;
      this.execute();
      this.time -= 1;
    }
  }
  wait(t) {
    this.status = STATUSES.SLEEPING;
    var self = this;
    setTimeout(function () {
      self.status = STATUSES.RUNNING;
    }, t);
  }
  enlarge(size) {
    for (let i = 0; i < size; i++) {
      this.mem.push(0);
    }
    console.log("mem", this.mem);
  }
  poke(instruction) {
    if (this.adr >= this.mem.length) {
      this.enlarge(100);
    }
    this.mem[this.adr] = instruction;
    this.adr += 1;
  }
  generate(program) {
    program.generate(this);
  }
  reset() {
    this.pc = 0;
    this.peak = this.mem.length;
    this.frame = this.peak;
  }
  execute() {
    var index;
    if (this.object.srcId == 654) {
      console.log("instruction", this.mem[this.pc]);
    }
    if (this.mem[this.pc] == INSTRUCTION_PUSH) {
      this.pc += 1;
      this.peak -= 1;
      this.mem[this.peak] = this.mem[this.pc];
      this.pc += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_NEGATIVE) {
      this.pc += 1;
      this.mem[this.peak] = -1 * this.mem[this.peak];
    } else if (this.mem[this.pc] == INSTRUCTION_ADD) {
      this.pc += 1;
      this.mem[this.peak + 1] = this.mem[this.peak + 1] + this.mem[this.peak];
      this.peak = this.peak + 1;
    } else if (this.mem[this.pc] == INSTRUCTION_GET) {
      this.pc += 1;
      var index = this.mem[this.pc];
      this.pc += 1;
      this.peak -= 1;
      this.mem[this.peak] = this.mem[index];
    } else if (this.mem[this.pc] == INSTRUCTION_SET) {
      this.pc += 1;
      var index = this.mem[this.pc];
      this.pc += 1;
      this.mem[index] = this.mem[this.peak];
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_SET_EXECUTION_SPEED) {
      this.pc += 1;
      this.speed = (this.fps / 100) * this.mem[this.peak];
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_STOP_EXECUTION) {
      this.pc += 1;
      this.status = STATUSES.STOPPED;
    } else if (this.mem[this.pc] == INSTRUCTION_STOP_GAME) {
      this.pc += 1;
      stopScene();
    } else if (this.mem[this.pc] == INSTRUCTION_JUMP) {
      this.pc = this.mem[this.pc + 1];
    } else if (this.mem[this.pc] == INSTRUCTION_UP) {
      this.pc += 1;
      this.object.up(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_DOWN) {
      this.pc += 1;
      this.object.down(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_LT) {
      this.pc += 1;
      this.object.left(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_RT) {
      this.pc += 1;
      this.object.right(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_TURN_LT) {
      this.pc += 1;
      this.object.turnLeft();
    } else if (this.mem[this.pc] == INSTRUCTION_TURN_RT) {
      this.pc += 1;
      this.object.turnRight();
    } else if (this.mem[this.pc] == INSTRUCTION_SET_X) {
      this.pc += 1;
      this.object.setX(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_SET_Y) {
      this.pc += 1;
      this.object.setY(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_SET_R) {
      this.pc += 1;
      this.object.setR(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_SET_SCORE) {
      this.pc += 1;
      gameScene.getCurrLevel().scene.setScore(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_IS_SCORE) {
      this.pc += 1;
      const isScore = gameScene
        .getCurrLevel()
        .scene.isScore(this.mem[this.peak]);
      this.mem[this.peak] = isScore ? 1 : 0;
    } else if (this.mem[this.pc] == INSTRUCTION_CHANGE_SCORE) {
      this.pc += 1;
      gameScene.getCurrLevel().scene.changeScore(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_SET_TIMER) {
      this.pc += 1;
      gameScene.getCurrLevel().scene.setTimer(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_IS_TIMER) {
      this.pc += 1;
      const isTimer = gameScene
        .getCurrLevel()
        .scene.isTimer(this.mem[this.peak]);
      this.mem[this.peak] = isTimer ? 1 : 0;
    } else if (this.mem[this.pc] == INSTRUCTION_SET_C) {
      this.pc += 1;
      this.object.setC(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_SET_RC) {
      this.pc += 1;
      this.object.setRC(this.mem[this.peak + 1], this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_INC_X) {
      this.pc += 1;
      this.object.increaseX(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_INC_Y) {
      this.pc += 1;
      this.object.increaseY(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_DEC_X) {
      this.pc += 1;
      this.object.decreaseX(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_SHOW_TEXT) {
      this.pc += 1;
      gameScene.getCurrLevel().scene.showText(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_REMOVE_TEXT) {
      this.pc += 1;
      gameScene.getCurrLevel().scene.textShown = null;
    } else if (this.mem[this.pc] == INSTRUCTION_PLAY_SOUND) {
      this.pc += 1;
      gameScene.getCurrLevel().playSound(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_STOP_SOUNDS) {
      this.pc += 1;
      gameScene.getCurrLevel().stopSounds();
    } else if (this.mem[this.pc] == INSTRUCTION_WAIT) {
      this.pc += 1;
      this.wait(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_CLONE_SPRITE) {
      this.pc += 1;
      this.object.clone();
    } else if (this.mem[this.pc] == INSTRUCTION_DEL_SPRITE) {
      this.pc += 1;
      this.object.delete();
      this.status = STATUSES.STOPPED;
    } else if (this.mem[this.pc] == INSTRUCTION_DEC_Y) {
      this.pc += 1;
      this.object.decreaseY(this.mem[this.peak]);
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_SHOW_OBJECT) {
      this.pc += 1;
      this.object.show();
    } else if (this.mem[this.pc] == INSTRUCTION_HIDE_OBJECT) {
      this.pc += 1;
      this.object.hide();
    } else if (this.mem[this.pc] == INSTRUCTION_FOR_LOOP) {
      this.pc += 1;
      var index = this.mem[this.pc];
      if (this.mem[this.peak] >= this.mem[this.peak - 1]) {
        this.pc = index;
      } else {
        this.pc += 1;
        this.peak += 1;
      }
    } else if (this.mem[this.pc] == INSTRUCTION_LOOP) {
      this.pc += 1;
      var index = this.mem[this.pc];
      this.mem[this.peak] = this.mem[this.peak] - 1;
      console.log(
        "this loop count",
        this.mem[this.peak],
        this.mem[this.peak] - 1,
        index,
        this.mem[this.pc],
        this.mem[index]
      );
      if (this.mem[this.peak] > 0) {
        this.pc = index;
      } else {
        this.pc += 1;
        this.peak += 1;
      }
    } else if (this.mem[this.pc] == INSTRUCTION_LOOP_FOREVER) {
      this.pc += 1;
      this.pc = this.mem[this.pc];
    } else if (this.mem[this.pc] == INSTRUCTION_JMPLOOP_FOREVER) {
      this.pc += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_SET_COUNTER) {
      if (this.mem[this.peak] > 0) {
        this.pc += 1;
      } else {
        while (this.mem[this.pc] != INSTRUCTION_LOOP) {
          this.pc += 1;
        }
      }
    } else if (this.mem[this.pc] == INSTRUCTION_SET_LOOP_COUNTERS) {
      if (this.mem[this.peak] > 0) {
        this.pc += 1;
      } else {
        while (this.mem[this.pc] != INSTRUCTION_FOR_LOOP) {
          this.pc += 1;
        }
      }
    } else if (this.mem[this.pc] == INSTRUCTION_SET_INTER_HANDLER) {
      this.pc += 1;
      this.interruptHandler = this.mem[this.peak];
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_IF_KEY_PRESSED) {
      this.pc += 1;
      let keyPressed = this.object.checkKeyPressed(this.mem[this.peak]);
      if (keyPressed == true) {
        this.pc += 1;
      } else {
        this.pc = this.mem[this.pc];
      }
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_WHEN_START_CLICKED) {
      this.pc += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_INTERRUPTION) {
      this.pc += 1;
      VMController.interruptAll(this.id);
    } else if (this.mem[this.pc] == INSTRUCTION_END_INTERRUPTION) {
      this.pc += 1;
      VMController.endInterruptionAll(this.id);
    } else if (this.mem[this.pc] == INSTRUCTION_SPRITE_CLICKED) {
      this.pc += 1;
      let clicked = this.object.checkClicked();

      if (clicked == true) {
        this.pc += 1;
      } else {
        this.pc = this.mem[this.pc];
      }
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_JUMPIFFALSE) {
      this.pc += 1;
      if (this.mem[this.peak] == 0) {
        this.pc = this.mem[this.pc];
      } else {
        this.pc += 1;
      }
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_MOVE_TO_CELL) {
      this.object.moveToCell(this.mem[this.peak + 1], this.mem[this.peak]);
      this.pc += 1;
      this.peak += 1;
    } else if (this.mem[this.pc] == INSTRUCTION_IS_ROW_COL) {
      this.pc += 1;
      const isRowCol = this.object.isRowCol(
        this.mem[this.peak + 1],
        this.mem[this.peak]
      );
      this.mem[this.peak] = isRowCol ? 1 : 0;
    } else if (this.mem[this.pc] == INSTRUCTION_IS_ROW_COL_OBJECT) {
      this.pc += 1;
      const isRowColObject = this.object.isRowColObject(
        this.mem[this.peak + 2],
        this.mem[this.peak + 1],
        this.mem[this.peak]
      );
      this.mem[this.peak + 2] = isRowColObject ? 1 : 0;
      this.peak += 2;
    } else if (this.mem[this.pc] == INSTRUCTION_MOVE_OBJECT_TO) {
      this.pc += 1;
      this.object.moveObjectTo(
        this.mem[this.peak + 2],
        this.mem[this.peak + 1],
        this.mem[this.peak]
      );
      this.peak += 2;
    } else if (this.mem[this.pc] == INSTRUCTION_MOVE_OBJECT_BY) {
      this.pc += 1;
      this.object.moveObjectBy(
        this.mem[this.peak + 2],
        this.mem[this.peak + 1],
        this.mem[this.peak]
      );
    } else if (this.mem[this.pc] == INSTRUCTION_IS_OBJECT) {
      this.pc += 1;
      const isObject = this.object.isObject(
        this.mem[this.peak + 2],
        this.mem[this.peak + 1],
        this.mem[this.peak]
      );
      this.mem[this.peak + 2] = isObject ? 1 : 0;
    } else if (this.mem[this.pc] == INSTRUCTION_IS_NOT_OBJECT) {
      this.pc += 1;
      const isObject = this.object.isObject(
        this.mem[this.peak + 2],
        this.mem[this.peak + 1],
        this.mem[this.peak]
      );
      this.mem[this.peak + 2] = !isObject ? 1 : 0;
    } else if (this.mem[this.pc] == INSTRUCTION_IS_COLLISION_WITH) {
      this.pc += 1;
      const isCollision = this.object.isCollisionWith(this.mem[this.peak]);
      this.mem[this.peak] = isCollision ? 1 : 0;
    } else if (this.mem[this.pc] == INSTRUCTION_CHASE_PLAYER) {
      this.pc += 1;
      this.object.chasePlayer();
    } else if (this.mem[this.pc] == INSTRUCTION_STOP_CHASE_PLAYER) {
      this.pc += 1;
      this.object.stopChasingPlayer();
    } else if (this.mem[this.pc] == INSTRUCTION_CALL) {
      this.pc += 1;
      this.peak -= 1;
      this.mem[this.peak] = this.pc + 1;
      this.peak -= 1;
      this.mem[this.peak] = this.frame;
      this.frame = this.peak;
      this.pc = this.mem[this.pc];
    } else if (this.mem[this.pc] == INSTRUCTION_RETURN) {
      this.pc += 1;
      this.peak = this.frame + 2 + this.mem[this.pc];
      this.pc = this.mem[this.frame + 1];
      this.frame = this.mem[this.frame];
    } else {
      this.status = STATUSES.TERMINATED;
    }
  }
}

class VirtualMachineListener extends VirtualMachine {
  executeStep() {
    if (this.status == STATUSES.SLEEPING) {
      return;
    }
    if (this.status == STATUSES.RUNNING) {
      this.execute();
    } else if (this.status != STATUSES.STOPPED) {
      this.status = STATUSES.RUNNING;
      this.reset();
    }
  }
}

const VMController = {
  VMs: [],
  backupVMs: [],
  interruptAll(vmID) {
    const vm = this.getVMById(vmID);
    this.VMs.forEach((w) => {
      if (w.objectId == vm.objectId && w.id != vm.id) {
        w.interrupt();
      }
    });
  },
  getVMById(id) {
    return this.VMs.find((w) => w.id == id);
  },
  endInterruptionAll(vmID) {
    const vm = this.getVMById(vmID);
    this.VMs.forEach((w) => {
      if (w.objectId == vm.objectId && w.id != vm.id) {
        w.endInterruption();
      }
    });
  },
  getInterruptHandlers() {
    return this.VMs.map((w) => w.interruptHandler);
  },
  getObjectInterruptHandlers(objectId) {
    return this.VMs.map((w) => {
      if (w.object.id == objectId) {
        return w.interruptHandler;
      }
    });
  },
  inVariables(variable) {
    if (this.variables[variable]) {
      return true;
    }
    return false;
  },
  getVarNames() {
    return Object.keys(this.variables);
  },
  add(vm) {
    vm.id = this.VMs.length + 1;
    this.VMs.push(vm);
  },
  run(vm) {
    vm.run();
  },
  runAll() {
    this.VMs.forEach((vm) => vm.run());
  },
  stopAll() {
    this.VMs.forEach((vm) => vm.stop());
  },
  resetAll() {
    this.VMs = this.backupVMs;
    this.VMs.forEach((vm) => vm.reset());
  },
  checkRunning() {
    if (this.VMs.length == 0) {
      return false;
    }
    return this.VMs.some((vm) => vm.status == STATUSES.RUNNING);
  },
  executeStep(deltaTime) {
    let toRemove = [];
    for (vm of this.VMs) {
      if (
        vm.status == STATUSES.RUNNING ||
        vm.status == STATUSES.SLEEPING ||
        (vm instanceof VirtualMachineListener && vm.status != STATUSES.STOPPED)
      ) {
        vm.executeStep(deltaTime);
      } else {
        toRemove.push(vm.id);
      }
    }
    for (let id of toRemove) {
      this.remove(id);
    }
  },
  remove(id) {
    this.VMs = this.VMs.filter((vm) => vm.id !== id);
  },
};

var fps = 40;
var now;
let stack = [];
let realTime = true;
var then = Date.now();
var interval = 1000 / fps;
var deltaTime;
var lastTimestamp;

function step(deltaTime) {
  VMController.executeStep(deltaTime);
  Player.keyAutomatTick();
  gameScene.getCurrLevel().scene.draw("codeScene");
}

function render(timestamp) {
  if (!VMController.checkRunning()) {
    return;
  }

  if (!lastTimestamp) {
    lastTimestamp = timestamp;
  }
  deltaTime = timestamp - lastTimestamp;

  step(deltaTime);

  lastTimestamp = timestamp;
  window.requestAnimationFrame(render);
}

async function executeCode() {
  parseCode();
  VMController.runAll();
  setKeyListeners();
  render();
}

function stopScene() {
  gameScene.getCurrLevel().stopSounds();
  VMController.stopAll();
}

function resetScene() {
  gameScene.resetScene();
  VMController.resetAll();
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function parseCommands(commands, src) {
  const tree = new SyntaxTree(commands);
  const program = tree.parse(commands);
  const translated = program.translate();
  const compiler = new Compiler(translated);
  const parsed = compiler.parse();
  const objects = gameScene.getCurrLevel().scene.getSrcObjects(src);
  objects.forEach((o) => createMachine(program, o));
  VMController.backupVMs = VMController.VMs;
  console.log("VMController", VMController.VMs);
}

function createMachine(program, object) {
  var vm = new VirtualMachine(object);
  vm.generate(program);
  vm.reset();
}

async function parseCode() {
  VMController.VMs = [];
  gameScene.getCurrLevel().programmableObjects.forEach((o) => {
    if (o.parsedCommands.length > 0) {
      parseCommands(o.parsedCommands, o.src);
    }
  });
}

function addToSubprograms(fun) {
  let funExists = COMMANDS_TYPE.mySubprograms.find(
    (subprogram) => subprogram.name == fun.name
  );
  if (!funExists) {
    COMMANDS_TYPE.mySubprograms.push(fun);
  }
}
