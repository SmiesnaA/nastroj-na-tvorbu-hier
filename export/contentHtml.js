const getHTMLcontent = (name) => {
  return `
    <!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8" />
      <link
        href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900"
        rel="stylesheet"
      />
      <link
        href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css"
        rel="stylesheet"
      />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui"
      />
    </head>
    
    <style>
    canvas {
      border: 0.5px solid black;
      background-color: white;
    }
    body {
      background-color: aliceblue;
      font-family: Arial, Helvetica, sans-serif;
    }
    .button {
      border: none;
      color: white;
      padding: 10px 20px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      margin: 4px 2px;
      cursor: pointer;
      display: inline-block;
    }
    .button:disabled {
      color: rgb(150, 150, 150);
      cursor: auto;
    }
    .start {
      background-color: #4caf50;
    }
    canvas {
      display: block;
    }
  </style>
  <body>
    <h2>${name}</h2>
    <div>
      <button type="button" id="startBtn" class="button start" onclick="executeCode()">
        Štart
      </button>
      <button type="button" id="stopBtn" class="button start" onclick="stop()">
      Stop
    </button>
    </div>
    <div id="codeScene"></div>
  `;
};

const getLevelData = () => {
  const levelData = JSON.stringify(gameScene.levels.map((l) => l.toJSON()));
  return `<script id="levelsData" type="application/json">
    ${levelData}
  </script>`;
};
