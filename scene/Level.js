var levelID = 1;

class Level {
  constructor(
    id,
    objects = [],
    commands = [],
    grid = [],
    sounds = [],
    cellSize = 30
  ) {
    this.id = id;
    this.scene = new Scene(id, cellSize, grid, objects);
    this.objects = objects;
    this.commands = commands;
    this.gameObjects = [];
    this.levelObjects = [];
    this.allGameObjects = [];
    this.playerId = -1;
    this.commandObjects = [];
    this.programmableObjects = [];
    this.objectProgrammed = null;
    this.enemies = [];
    this.walls = [];
    this.gridCells = [];
    this.items = [];
    this.shownRectInCanvas = false;
    this.currentCell = null;
    this.sounds = sounds;
    this.audios = [];
    this.loaded = false;
    this.allObjects = new Map();
  }
  toJSON() {
    return {
      number: this.id,
      objects: this.scene.getLevelObjects(),
      cellSize: this.scene.cellSize,

      sounds: this.sounds,
      grid: this.scene.toJSON(),
      commands: this.commandsToJSON(),
    };
  }
  commandsToJSON() {
    if (this.programmableObjects.length == 0) {
      return [];
    }
    return this.programmableObjects.map((o) => o.toJSON());
  }
  setupCells() {
    const scene = this.scene;
    for (let r = 0; r < scene.rows; r++) {
      for (let c = 0; c < scene.cols; c++) {
        let object = scene.get(r, c);
        let cell = new Cell(r, c, object.type, object.passible);
        object.cell = cell;
        this.gridCells.push(cell);
      }
    }
    let enemies = this.scene.getObjectsOfType(OBJECTS.ENEMY);
    enemies.forEach((e) => e.maze());
  }
  async setObjects() {
    for (let i = 0; i < this.rows; i++) {
      for (let j = 0; j < this.cols; j++) {
        const o = this.getObjectsById(this.scene.grid[i][j])[0];
        let obj;
        if (o) {
          obj = new GameObject(
            o.objectId,
            i,
            j,
            this.cellSize,
            o.imgName,
            o.src,
            o.type,
            o.passible
          );
          if (o.type === OBJECTS.ENEMY) {
            this.enemies.push(obj);
          }
          if (o.type === OBJECTS.ITEM) {
            this.items.push(obj);
          }
        } else {
          obj = new GameObject(
            null,
            i,
            j,
            this.cellSize,
            "",
            "",
            OBJECTS.NOTHING
          );
        }

        this.allGameObjects.push(obj);
      }
    }
  }
  initCodeEditor() {
    interact(".object").unset();
    document
      .querySelectorAll(".objectClicked")
      .forEach((obj) => obj.classList.remove("objectClicked"));
    this.createObjects();
    if (this.loaded) {
      this.setProgrammableObjects;
    }
    gameScene.getCurrLevel().scene.setGridIds();
  }
  getObjectsById(srcId) {
    return this.levelObjects.filter((o) => o.srcId == srcId);
  }
  getObjectById(id) {
    return this.levelObjects.find((o) => o.id == id);
  }
  getWalls() {
    return this.walls;
  }
  addSound(sound) {
    const hasSound = this.sounds.some((s) => s.name == sound.name);
    if (!hasSound) {
      this.sounds.push(sound);
    }
  }
  playSound(id) {
    var audio = new Audio(this.sounds[id].file);
    audio.play();
    this.audios.push(audio);
  }
  stopSounds() {
    if (this.audios.length == 0) return;
    this.audios.forEach((a) => {
      a.pause();
      a.currentTime = 0;
    });
  }
  getObjectsOfType(objectType) {
    return this.allObjects.get(objectType);
  }
  createObjects() {
    this.levelObjects = [];
    let index = 1;
    for (let r = 0; r < this.scene.rows; r++) {
      for (let c = 0; c < this.scene.cols; c++) {
        const objects = this.scene.getObjects(r, c);
        objects.forEach((e) => {
          if (e.type != OBJECTS.NOTHING) {
            this.levelObjects.push(
              new LevelObject(
                index++,
                e.srcId,
                e.id,
                e.imgName,
                e.src,
                e.type,
                e.passible
              )
            );
          }
        });
      }
    }

    this.setProgrammableObjects();
    if (this.programmableObjects.length > 0) {
      this.setObjectProgrammed(this.programmableObjects[0].id);

      this.parse(this.commands);
      this.loaded = true;
    }
  }
  getPlayerObject() {
    return this.levelObjects.find((o) => o.type == OBJECTS.PLAYER);
  }
  setProgrammableObjects() {
    let objects = this.levelObjects.filter((o) => o.programmable);
    const uniqueSrc = new Set();
    this.programmableObjects = objects.filter((element) => {
      const isDuplicate = uniqueSrc.has(element.src);
      uniqueSrc.add(element.src);

      if (!isDuplicate) {
        return true;
      }
    });
  }
  getGameObjectsById(id) {
    return this.levelObjects.filter((o) => o.id == id);
  }
  getEnemies() {}
  draw() {
    this.clearCanvas();
    this.allGameObjects.forEach((o) => {
      o.draw();
    });
    if (this.shownRectInCanvas) {
      this.showRectInCanvas();
    }
  }
  redraw() {
    this.clearCanvas();
    this.allGameObjects.forEach((o) => {
      o.draw();
    });
  }
  clearCanvas() {
    Player.getCtx().clearRect(
      0,
      0,
      Player.getCanvas().width,
      Player.getCanvas().height
    );
  }
  parse(commands) {
    commands.forEach((o) => {
      for (let key in o) {
        const levelO = this.getObjectsById(key);
        levelO.forEach((l) => {
          l.commands = o[key];
          if (o[key] != "") {
            l.parseCommandsToJson();
          }
        });
      }
    });
  }
  setCommands() {
    this.commandObjects.forEach((level) => {
      level.parseCommandsToJson();
      if (level.parsedCommands.length != 0) {
        if (level.type === OBJECTS.PLAYER) {
          gameScene.programPlayerCommands = level.parsedCommands;
        }
      }
    });
  }
  setObjectProgrammed(id) {
    this.objectProgrammed = this.getObjectById(id);
  }
}

class LevelObject {
  constructor(id, srcId, objectId, imgName, src, type, passible) {
    this.id = id;
    this.srcId = srcId;
    this.objectId = objectId;
    this.src = src;
    this.passible = passible;
    this.showSrc = this.src;
    this.type = type;
    this.imgName = imgName;
    if (this.type == OBJECTS.PLAYER || this.type == OBJECTS.ENEMY) {
      const filename = imgName.split("_").slice(0, -1).join(".");
      this.showSrc = "assets/characters/" + filename + "_main.png";
    }

    this.commands = [];
    this.parsedCommands = [];
    this.programmable = PROGRAMMABLE_OBJECTS.includes(this.type);
  }
  getCommonObjects() {
    return gameScene.getCurrLevel().getObjectsBySrc(this.object.src);
  }
  parseCommandsToJson() {
    if (PROGRAMMABLE_OBJECTS.includes(this.type)) {
      const compiler = new Compiler(this.commands);
      const compiledCommands = compiler.parse();
      this.parsedCommands = compiledCommands.toJSON();
    }
  }
  getObject() {
    return gameScene.getCurrLevel().scene.getById(this.objectId);
  }
  translateCommands() {
    const tree = new SyntaxTree(this.parsedCommands);
    const program = tree.parse(this.parsedCommands);
    const translated = program.translate();
    return translated;
  }
  setPassible(value) {
    this.passible = value;
    const objects = gameScene.getCurrLevel().scene.srcObjects.get(this.src);
    if (objects && objects.length > 0) {
      objects.forEach((o) => {
        o.passible = value;
      });
    }
  }
  toJSON() {
    return {
      [this.srcId]: this.translateCommands(),
    };
  }
}
