const fs = require("fs");

const contentHtml = fs.readFileSync("./contentHtml.js");
const contentSetup = fs.readFileSync("../setup.js");
const contentGameObject = fs.readFileSync("../scene/GameObject.js");
const contentScene = fs.readFileSync("../scene/scene.js");
const contentLevel = fs.readFileSync("../scene/Level.js");
const contentExecute = fs.readFileSync("../scene/execute.js");

const contentScript =
  "const getJScontent = `<script>" +
  contentSetup.toString() +
  contentGameObject.toString() +
  contentLevel.toString() +
  contentScene.toString() +
  contentExecute.toString() +
  "</script><script>initScene();</script></body></html>`";

fs.writeFileSync(
  "./fileContent.js",
  contentHtml.toString() + contentScript.replaceAll("\\n", "\\\\n")
);
