const Score = {
  score: 0,
  object: null,
  reset() {
    this.score = 0;
    this.setObject();
  },
  set(value) {
    this.score = value;
  },
  get() {
    return this.score;
  },
  change(value) {
    this.score += value;
  },
  getObject() {
    if (!this.object) {
      this.setObject();
    }
    return this.object;
  },
  setObject() {
    var scene = gameScene.getCurrLevel().scene.getScene();
    var obj = scene.querySelector("#score");
    if (!obj) {
      obj = document.createElement("p");
      obj.id = "score";
    }
    obj.innerHTML = "Skóre: " + this.score;
    this.object = obj;
  },
};

const Timer = {
  time: 180,
  object: null,
  reset() {
    this.time = 180;
    this.setObject();
  },
  set(value) {
    this.time = value;
  },
  get() {
    return this.time;
  },
  getObject() {
    if (!this.object) {
      this.setObject();
    }
    return this.object;
  },
  setObject() {
    var scene = gameScene.getCurrLevel().scene.getScene();
    var obj = scene.querySelector("#timer");
    if (!obj) {
      obj = document.createElement("p");
      obj.id = "timer";
    }
    obj.innerHTML = "Časovač: " + this.time;
    this.object = obj;
  },
};

class Scene {
  constructor(id, cellSize, grid = [], objects = []) {
    this.saved = true;
    this.objectId = 0;

    this.xleftView = 0;
    this.ytopView = 0;
    this.widthViewOriginal = 1.0;
    this.heightViewOriginal = 1.0;
    this.widthView = this.widthViewOriginal;
    this.heightView = this.heightViewOriginal;

    this.objects = objects;
    this.grid = [];
    this.backupGrid = [];
    this.gridIds = grid;
    this.initGrid();
    this.gridShown = false;
    this.gameObjects = [];
    this.cellSize = cellSize;
    this.width = this.cols * this.cellSize;
    this.height = this.rows * this.cellSize;
    this.ratioWidth = this.width / this.cols;
    this.ratioHeight = this.height / this.rows;
    const canvas = document.createElement("canvas");
    canvas.width = this.width;
    canvas.height = this.height;
    this.id = id;
    this.canvas = canvas;
    this.canvasID = "canvas";
    this.playerPos = null;

    this.scoreObject = Score;
    this.timerObject = Timer;
    this.textShown = null;

    this.srcObjects = new Map();
    this.allObjects = new Map();

    this.setObjects();
  }
  setScore(value) {
    Score.set(value);
    Score.setObject();
  }
  isScore(value) {
    return value == Score.get();
  }
  changeScore(value) {
    Score.change(value);
    Score.setObject();
  }
  setTimer(value) {
    Timer.set(value);
    Timer.setObject();
  }
  isTimer(value) {
    return value == Timer.get();
  }
  getLevelObjects() {
    let objects = [];
    for (const [key, value] of this.srcObjects) {
      objects.push(value[0]);
    }
    return objects;
  }
  toJSON() {
    var gridIds = Array(this.rows)
      .fill()
      .map(() => Array(this.cols).fill(0));
    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        let obj = this.get(r, c);
        gridIds[r][c] = obj.srcId;
      }
    }
    return gridIds;
  }
  reset() {
    this.textShown = null;
    this.resetObjects();
    this.clear();
    this.draw();
  }
  initInteract() {
    interactObjectItem.create(this.id);
  }
  findPosInGrid(srcId) {
    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        let id = this.gridIds[r][c];
        if (id == srcId) {
          return { r: r, c: c };
        }
      }
    }
    return null;
  }
  initGrid() {
    if (this.gridIds.length == 0) {
      this.rows = 15;
      this.cols = 38;
      this.gridIds = Array(this.rows)
        .fill()
        .map(() => Array(this.cols).fill(0));
    } else {
      this.rows = this.gridIds.length;
      this.cols = this.gridIds[0].length;
    }
    this.grid = Array(this.rows)
      .fill()
      .map(() => Array(this.cols).fill(0));
    this.backupGrid = Array(this.rows)
      .fill()
      .map(() => Array(this.cols).fill(0));
  }
  showGrid() {
    this.gridShown = !this.gridShown;
    this.draw();
  }
  resetObjects() {
    this.grid = Array(this.rows)
      .fill()
      .map(() => Array(this.cols).fill(0));
    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        let obj = this.getSrcObject(this.gridIds[r][c]);
        if (obj) {
          let newObj = new GameObject(
            this.objectId++,
            r,
            c,
            this.cellSize,
            obj.imgName,
            obj.src,
            obj.type,
            obj.passible
          );
          newObj.srcId = obj.srcId;
          this.set(r, c, newObj);
          const srcObj = this.srcObjects.get(newObj.src);
          if (srcObj) {
            srcObj.push(newObj);
          } else {
            this.srcObjects.set(newObj.src, [newObj]);
          }
        } else {
          this.set(r, c, new Nothing(r, c, this.cellSize));
        }
      }
    }
  }
  setObjects() {
    if (this.objects.length == 0) {
      this.objects = [
        new GameObject(
          this.objectId++,
          0,
          0,
          this.cellSize,
          "",
          "",
          OBJECTS.NOTHING
        ),
      ];
      this.srcObjects.set("", [this.objects[0]]);
    }

    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        let obj = this.getSrcObject(this.gridIds[r][c]);
        if (obj) {
          let newObj = new GameObject(
            this.objectId++,
            r,
            c,
            this.cellSize,
            obj.imgName,
            obj.src,
            obj.type,
            obj.passible
          );
          newObj.srcId = obj.srcId;
          this.set(r, c, newObj);
          const srcObj = this.srcObjects.get(newObj.src);
          if (srcObj) {
            srcObj.push(newObj);
          } else {
            this.srcObjects.set(newObj.src, [newObj]);
          }
        } else {
          this.set(r, c, new Nothing(r, c, this.cellSize));
        }
      }
    }

    this.setBackupGrid();
    this.setObjectsType();
  }
  setObjectsType() {
    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        let e = this.grid[r][c];
        const objectsOfType = this.allObjects.get(e.type);
        if (objectsOfType && objectsOfType.length > 0) {
          objectsOfType.push(e);
          this.allObjects.set(e.type, objectsOfType);
        } else {
          this.allObjects.set(e.type, [e]);
        }
      }
    }
  }
  setBackupGrid() {
    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        this.backupGrid[r][c] = this.cloneObject(this.get(r, c));
      }
    }
  }
  objectsToGrid() {
    var grid = [];
    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        let obj = this.getSrcObject(grid[r][c]);
        if (obj) {
          grid[r][c] = obj.id;
        } else {
          grid[r][c] = 0;
        }
      }
    }
    return grid;
  }
  showText(text) {
    this.textShown = text;
    const ctx = this.getCtx();
    const textWidth = ctx.measureText(text).width;
    var gradient = ctx.createLinearGradient(0, 0, this.getCanvas().width, 0);
    gradient.addColorStop("0", " magenta");
    gradient.addColorStop("0.5", "blue");
    gradient.addColorStop("1.0", "red");
    ctx.font = "50px san-serif";
    ctx.fillStyle = gradient;
    ctx.fillText(text, this.width / 2 - textWidth / 2, this.height / 2);
  }
  init() {
    setTimeout(async () => {
      this.gridShown = true;
      const scene = document.getElementById("scene");
      scene.innerHTML = "";
      scene.appendChild(this.canvas);
      createZoomableCanvas();

      this.setCanvas(this.rows, this.cols, this.cellSize);
      this.drawScene();
    }, 100);
  }
  setGridIds() {
    this.gridIds = this.toJSON();
  }
  initCodeScene() {
    setTimeout(async () => {
      this.gridShown = false;
      const scene = document.getElementById("codeScene");
      scene.innerHTML = "";
      scene.appendChild(cloneCanvas(this.canvas));

      let codeScene = this.getScene();
      codeScene.prepend(Score.getObject());
      // codeScene.prepend(Timer.getObject());
      this.draw();
    }, 100);
  }
  getCanvas() {
    let scene = document.getElementById("scene");
    let codeScene = document.getElementById("codeScene");
    if ((scene && codeScene) || !scene) {
      scene = codeScene;
    }
    return scene.getElementsByTagName("canvas")[0];
  }
  getScene() {
    let scene = document.getElementById("scene");
    let codeScene = document.getElementById("codeScene");
    if ((scene && codeScene) || !scene) {
      scene = codeScene;
    }
    return scene;
  }
  draw() {
    this.clear();
    var player = null;
    var characters = [];

    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        const objects = this.getObjects(r, c);
        objects.forEach((o, _key) => {
          if (o.type == OBJECTS.PLAYER) {
            player = o;
          } else if (o.type == OBJECTS.ENEMY) {
            characters.push(o);
          } else {
            o.draw();
          }
        });
      }
      characters.forEach((ch) => ch.draw());
      if (player != null) {
        player.draw();
      }
    }

    if (this.textShown != null) {
      this.showText(this.textShown);
    }
  }
  drawScene() {
    var canvas = this.getCanvas();
    var ctx = this.ctx;
    var p1 = ctx.transformedPoint(0, 0);
    var p2 = ctx.transformedPoint(canvas.width, canvas.height);
    ctx.clearRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);

    ctx.save();
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.restore();
    ctx.lineWidth = 0.5;

    this.draw();
  }
  getSrcObject(srcId) {
    const o = this.objects.find((o) => o.srcId == srcId);
    if (o && o.srcId != 0) {
      return o;
    }
    return null;
  }
  clearCanvas() {
    this.getCtx().clearRect(0, 0, this.width, this.height);
  }
  drawObject(obj) {
    const image = new Image();
    image.onload = () => {
      this.getCtx().drawImage(
        image,
        this.cellSize * obj.c,
        this.cellSize * obj.r,
        this.cellSize,
        this.cellSize
      );
    };
    image.src = obj.img;
  }
  removeObject(r, c) {
    const obj = this.get(r, c);
    const posXY = this.getPos(r, c);
    this.remove(r, c, obj);
    this.drawEmpty(posXY[0], posXY[1]);
    var srcObjects = this.srcObjects.get(obj.src);
    if (!srcObjects) {
      return;
    }
    if (srcObjects.length == 1) {
      this.srcObjects.delete(obj.src);
      return;
    }
    srcObjects = srcObjects.filter((item) => item.id != obj.id);
    this.srcObjects.set(obj.src, srcObjects);
  }
  checkObjectId(id) {
    let c = 0;
    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        if (this.grid[r][c].id == id) {
          return c++;
        }
      }
    }
    return c > 1;
  }
  drawEmpty(r, c) {
    this.getCtx().strokeStyle = this.gridShown ? "black" : "white";
    this.getCtx().lineWidth = 0.5;
    this.getCtx().fillStyle = "white";
    this.getCtx().fillRect(r, c, this.cellSize, this.cellSize);
    this.getCtx().strokeRect(r, c, this.cellSize, this.cellSize);
  }
  objectInfoShown() {
    return this.objectClicked != 0;
  }
  clicked(object) {
    const ctx = this.getCtx();
    if (this.objectClicked != 0) {
      ctx.strokeStyle = this.grid ? "black" : "rgba(0, 0, 0, 0.0)";
      const posXY = this.getPos(this.objectClicked.x, this.objectClicked.y);
      ctx.strokeRect(posXY[0], posXY[1], this.cellSize, this.cellSize);
    }
    this.objectClicked = object;
  }
  getCtx() {
    return this.getCanvas().getContext("2d");
  }
  deleteScene() {
    this.gridIds = Array(this.rows)
      .fill()
      .map(() => Array(this.cols).fill(0));
    this.grid = Array(this.rows)
      .fill()
      .map(() => Array(this.cols).fill(0));
    this.objectId = 0;

    this.setScene(this.rows, this.cols, this.cellSize);
    this.draw();
  }
  setCanvas(rows, cols, cellSize) {
    this.cellSize = cellSize;
    this.rows = rows;
    this.cols = cols;
    this.width = cols * cellSize;
    this.height = rows * cellSize;
    this.ratioWidth = this.width / this.cols;
    this.ratioHeight = this.height / this.rows;
    gameScene.rows = rows;

    this.getCanvas().width = this.cols * this.cellSize;
    this.getCanvas().height = this.rows * this.cellSize;

    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        if (this.get(r, c) == 0) {
          this.set(
            r,
            c,
            new Nothing(r, c, this.cellSize, this.ratioWidth, this.ratioHeight)
          );
        } else {
          const objects = this.getObjects(r, c);
          objects.forEach((obj, _key) => {
            obj.setProps(this.cellSize, r, c);
            this.set(r, c, obj);
          });
        }
      }
    }
    this.draw();
  }
  setScene(rows, cols, cellSize) {
    this.cellSize = cellSize;
    this.rows = rows;
    this.cols = cols;
    this.width = cols * cellSize;
    this.height = rows * cellSize;
    this.ratioWidth = this.width / this.cols;
    this.ratioHeight = this.height / this.rows;
    gameScene.rows = rows;

    this.getCanvas().width = this.cols * this.cellSize;
    this.getCanvas().height = this.rows * this.cellSize;

    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        if (this.get(r, c) == 0) {
          this.set(
            r,
            c,
            new Nothing(r, c, this.cellSize, this.ratioWidth, this.ratioHeight)
          );
        } else {
          const objects = this.getObjects(r, c);
          objects.forEach((obj, _key) => {
            obj.setProps(this.cellSize, r, c);
            this.set(r, c, obj);
          });
        }
      }
    }
  }
  cloneObject(obj) {
    return new GameObject(
      obj.id,
      obj.r,
      obj.c,
      obj.w,
      obj.imgName,
      obj.src,
      obj.type,
      obj.passible
    );
  }
  clear() {
    this.getCtx().clearRect(
      0,
      0,
      this.cols * this.cellSize,
      this.rows * this.cellSize
    );
  }
  getObjectsOfType(type) {
    var objectsType = [];
    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        const objects = this.getObjects(r, c);
        objects.forEach((obj, _key) => {
          if (obj.type == type) {
            objectsType.push(obj);
          }
        });
      }
    }
    return objectsType;
  }
  getSrcObjects(src) {
    var objectsSrc = [];
    for (let r = 0; r < this.rows; r++) {
      for (let c = 0; c < this.cols; c++) {
        const objects = this.getObjects(r, c);
        objects.forEach((obj, _key) => {
          if (obj.src == src) {
            objectsSrc.push(obj);
          }
        });
      }
    }
    return objectsSrc;
  }
  setSrcId(obj) {
    const objects = this.srcObjects.get(obj.src);
    if (objects && objects.length > 0) {
      obj.srcId = objects[0].srcId;
      objects.push(obj);
      return;
    }
    obj.srcId = obj.id;
    this.srcObjects.set(obj.src, [obj]);
  }
  getGameObject(r, c) {
    return this.gameObjects[this.getIndex(r, c)];
  }
  set(r, c, obj) {
    var objects = this.getObjects(r, c);
    if (!objects) {
      this.grid[r][c] = new Map([[obj.id, obj]]);
      return;
    }
    if (this.get(r, c).type == OBJECTS.NOTHING) {
      objects = new Map();
    }
    objects.set(obj.id, obj);
    this.grid[r][c] = objects;
  }
  getObject(r, c, obj) {
    const objects = this.getObjects(r, c);
    if (objects && objects.has(obj.id)) {
      return objects.get(obj.id);
    }
    return 0;
  }
  get(r, c) {
    const objects = this.getObjects(r, c);
    if (objects && objects.size > 0) {
      const [firstElement] = objects.values();
      return firstElement;
    }
    return 0;
  }
  getObjects(r, c) {
    if (this.grid[r]) {
      const objects = this.grid[r][c];
      if (objects) {
        return objects;
      }
    } else {
      this.grid[r] = Array(this.rows)
        .fill()
        .map(() => 0);
    }
    return null;
  }
  remove(r, c, obj) {
    let objects = this.getObjects(r, c);
    if (objects.has(obj.id)) {
      objects.delete(obj.id);
    }
    if (objects.size == 0) {
      let object = new Nothing(r, c, obj.w);
      objects = new Map([[object.id, object]]);
    }
    this.grid[r][c] = objects;
  }
  setGrid(r, c, obj) {
    if (isRC(r, c)) {
      const posXY = this.getPos(r, c);
      this.drawEmpty(posXY[0], posXY[1]);
      let objects = new Map([[obj.id, obj]]);
      this.grid[r][c] = objects;
      this.objects.push(obj);
    }
  }
  setPlayer(r, c, obj) {
    if (this.playerPos) {
      this.removeObject(this.playerPos.r, this.playerPos.c);
      this.playerPos = null;
    }
    this.setGrid(r, c, obj);
    this.playerPos = { r: obj.r, c: obj.c };
  }
  getIndex(r, c) {
    if (c > this.cols - 1 || r > this.rows - 1 || r < 0 || c < 0) return -1;
    return c + this.cols * r;
  }
  getPos(x, y) {
    return [y * this.cellSize, x * this.cellSize];
  }
}

class Cell {
  constructor(r, c, type, passible) {
    this.c = c;
    this.r = r;
    this.passible = passible;
    this.type = type;
    this.visited = false;
    this.neighbors = [];
    this.allNeighbors = [];
    this.cost = 0;
  }
  checkNeighbors() {
    let neighbors = [];
    let gridCells = gameScene.getCurrLevel().gridCells;
    let top = gridCells[getIndex(this.r - 1, this.c)];
    let right = gridCells[getIndex(this.r, this.c + 1)];
    let bottom = gridCells[getIndex(this.r + 1, this.c)];
    let left = gridCells[getIndex(this.r, this.c - 1)];

    top && !top.visited && neighbors.push(top);
    right && !right.visited && neighbors.push(right);
    bottom && !bottom.visited && neighbors.push(bottom);
    left && !left.visited && neighbors.push(left);

    let rIndex = Math.floor(Math.random() * neighbors.length);
    let n = neighbors[rIndex];
    if (n && n.passible) {
      let alreadyNeigbor = false;
      for (const neighbor of this.neighbors) {
        if (n == neighbor) {
          alreadyNeigbor = true;
        }
      }
      if (!alreadyNeigbor && n.passible) {
        this.neighbors.push(n);
        n.neighbors.push(this);
      }
    }
    return n;
  }
}

const gameScene = {
  levels: [new Level(levelID++)],
  currLevelID: 1,
  levelsData: [],
  gridCells: [],
  setLevel(level) {
    this.currLevelID = level;
    this.levels[level].scene.init();
  },
  resetScene() {
    Score.reset();
    Timer.reset();
    gameScene.reset();
  },
  showGrid() {
    gameScene.getCurrLevel().scene.showGrid();
  },
  getCanvas() {
    return gameScene.getCurrLevel().scene.getCanvas();
  },
  setObjectProgrammed(id) {
    this.getCurrLevel().setObjectProgrammed(id);
  },
  async init(levelsData, saved) {
    gameScene.levelsData = levelsData;
    await gameScene.createSavedScene();
    if (!saved) {
      gameScene.getCurrLevel().scene.init();
    }
  },
  initScene(levelsData) {
    gameScene.levelsData = levelsData;
    gameScene.createCodeScene();
  },
  getCurrLevel() {
    return gameScene.levels[gameScene.currLevelID - 1];
  },
  addLevel(level) {
    this.levels.push(level);
  },
  removeLevel(id) {
    this.levels = this.levels.filter((level) => level.id == id);
  },
  async createSavedScene() {
    gameScene.levels = [];
    this.currLevelID = 1;
    for (let l of gameScene.levelsData) {
      let newLevel = new Level(
        l.number,
        l.objects,
        l.commands,
        l.grid,
        l.sounds,
        l.cellSize
      );
      gameScene.addLevel(newLevel);
    }
    await sleep(100);
    gameScene.getCurrLevel().setupCells();
    gameScene.getCurrLevel().createObjects();
  },
  async createScene() {
    gameScene.getCurrLevel().scene.initCodeScene();
    await sleep(100);
    gameScene.levels = [];
    this.currLevelID = 1;
    for (let l of gameScene.levelsData) {
      let newLevel = new Level(
        l.number,
        l.objects,
        l.commands,
        l.grid,
        l.sounds,
        l.cellSize
      );
      gameScene.addLevel(newLevel);
    }
    gameScene.getCurrLevel().createObjects();
    gameScene.getCurrLevel().setupCells();
  },
  reset() {
    gameScene.getCurrLevel().scene.reset();
    gameScene.getCurrLevel().setupCells();
  },
  async createCodeScene() {
    gameScene.getCurrLevel().scene.initCodeScene();
    await sleep(100);
    this.reset();
  },
};

// HELPERS
function manhattanDistance(c1, c2) {
  return Math.abs(c1.c - c2.c) + Math.abs(c1.r - c2.r);
}

function getDirection(c1, c2) {
  let x = c2.c - c1.c;
  let y = c2.r - c1.r;
  return { x, y };
}

function getIndex(r, c) {
  if (
    c > gameScene.getCurrLevel().scene.cols - 1 ||
    r > gameScene.getCurrLevel().scene.rows - 1 ||
    r < 0 ||
    c < 0
  ) {
    return -1;
  }
  return c + gameScene.getCurrLevel().scene.cols * r;
}

function isRC(x, y) {
  const rows = gameScene.getCurrLevel().scene.rows;
  const cols = gameScene.getCurrLevel().scene.cols;
  return x >= 0 && x < rows && y >= 0 && y < cols;
}

function getRC(x, y) {
  const size = gameScene.getCurrLevel().scene.cellSize;
  const col = Math.floor(x / size);
  const row = Math.floor(y / size);
  return [row, col];
}

function boxCollision(r1, r2) {
  var offset = 0;
  if (r1.passible) {
    offset = gameScene.getCurrLevel().scene.cellSize / 2;
  }
  return (
    r2.x + offset <= r1.x + r1.w &&
    r1.x + offset <= r2.x + r2.w &&
    r2.y + offset <= r1.y + r1.h &&
    r1.y + offset <= r2.y + r2.h
  );
}

function areNeighbors(c1, c2) {
  if (c1 && c2) {
    for (const n of c1.allNeighbors) {
      if (n == c2) {
        return true;
      }
    }
  }
  return false;
}

function transformPoint(ctx, x, y) {
  const matrix = ctx.getTransform();
  return {
    x: matrix.a * x + matrix.c * y + matrix.e,
    y: matrix.b * x + matrix.d * y + matrix.f,
  };
}

function generateId() {
  return Math.random().toString(36).substr(2, 5);
}

function download(name, filename) {
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/html;charset=UTF-8," +
      encodeURIComponent(getHTMLcontent(name)) +
      encodeURIComponent(getLevelData()) +
      encodeURIComponent(getJScontent)
  );
  element.setAttribute("download", filename);

  element.style.display = "none";
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

function cloneCanvas(oldCanvas) {
  var newCanvas = document.createElement("canvas");
  var context = newCanvas.getContext("2d");
  newCanvas.width = oldCanvas.width;
  newCanvas.height = oldCanvas.height;

  context.drawImage(oldCanvas, 0, 0);
  return newCanvas;
}

const init = () => {
  const levelsData = JSON.parse(
    document.getElementById("levelsData").textContent
  );
  gameScene.init(levelsData);
};

const initScene = () => {
  const levelsData = JSON.parse(
    document.getElementById("levelsData").textContent
  );
  gameScene.initScene(levelsData);
};

var lastX, lastY;

function createZoomableCanvas() {
  var scene = gameScene.getCurrLevel().scene;
  var canvas = scene.getCanvas();
  var ctx = canvas.getContext("2d");
  scene.ctx = ctx;
  trackTransforms(scene.ctx);

  lastX = canvas.width / 2;
  lastY = canvas.height / 2;

  var dragStart, dragged;

  canvas.addEventListener(
    "mousedown",
    function (evt) {
      dragStart = ctx.transformedPoint(lastX, lastY);
      dragged = false;
    },
    false
  );

  canvas.addEventListener(
    "mousemove",
    function (evt) {
      lastX = evt.offsetX || evt.pageX - canvas.offsetLeft;
      lastY = evt.offsetY || evt.pageY - canvas.offsetTop;
      dragged = true;
      if (dragStart && cursorChosen == CURSOR_TYPES.MOVE) {
        var pt = ctx.transformedPoint(lastX, lastY);
        ctx.translate(pt.x - dragStart.x, pt.y - dragStart.y);
        scene.drawScene();
      }
    },
    false
  );

  canvas.addEventListener(
    "mouseup",
    function (evt) {
      dragStart = null;
    },
    false
  );

  var zoom = function (clicks) {
    var pt = ctx.transformedPoint(lastX, lastY);
    ctx.translate(pt.x, pt.y);
    var factor = Math.pow(scaleFactor, clicks);
    canvas.width = canvas.width * factor;
    canvas.height = canvas.height * factor;
    //ctx.scale(factor, factor);
    ctx.translate(-pt.x, -pt.y);
    scene.drawScene();
  };

  var handleScroll = function (evt) {
    var delta = evt.wheelDelta
      ? evt.wheelDelta / 1000
      : evt.detail
      ? -evt.detail
      : 0;
    if (delta) {
      zoom(delta);
    }
    return evt.preventDefault() && false;
  };

  canvas.addEventListener("DOMMouseScroll", handleScroll, false);
  canvas.addEventListener("mousewheel", handleScroll, false);

  function trackTransforms(ctx) {
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    var xform = svg.createSVGMatrix();
    ctx.getTransform = function () {
      return xform;
    };

    var savedTransforms = [];
    var save = ctx.save;
    ctx.save = function () {
      savedTransforms.push(xform.translate(0, 0));
      return save.call(ctx);
    };

    var restore = ctx.restore;
    ctx.restore = function () {
      xform = savedTransforms.pop();
      return restore.call(ctx);
    };

    var scale = ctx.scale;
    ctx.scale = function (sx, sy) {
      xform = xform.scaleNonUniform(sx, sy);
      return scale.call(ctx, sx, sy);
    };

    var rotate = ctx.rotate;
    ctx.rotate = function (radians) {
      xform = xform.rotate((radians * 180) / Math.PI);
      return rotate.call(ctx, radians);
    };

    var translate = ctx.translate;
    ctx.translate = function (dx, dy) {
      xform = xform.translate(dx, dy);
      return translate.call(ctx, dx, dy);
    };

    var transform = ctx.transform;
    ctx.transform = function (a, b, c, d, e, f) {
      var m2 = svg.createSVGMatrix();
      m2.a = a;
      m2.b = b;
      m2.c = c;
      m2.d = d;
      m2.e = e;
      m2.f = f;
      xform = xform.multiply(m2);
      return transform.call(ctx, a, b, c, d, e, f);
    };

    var setTransform = ctx.setTransform;
    ctx.setTransform = function (a, b, c, d, e, f) {
      xform.a = a;
      xform.b = b;
      xform.c = c;
      xform.d = d;
      xform.e = e;
      xform.f = f;
      return setTransform.call(ctx, a, b, c, d, e, f);
    };

    var pt = svg.createSVGPoint();
    ctx.transformedPoint = function (x, y) {
      pt.x = x;
      pt.y = y;
      return pt.matrixTransform(xform.inverse());
    };
  }
}

var curZoomIn = 0;
var zoom = function (clicks) {
  var newZoomIn = curZoomIn + clicks;
  if (newZoomIn < 0 || newZoomIn > 5) {
    return;
  }
  curZoomIn += clicks;
  var scene = gameScene.getCurrLevel().scene;
  var canvas = scene.getCanvas();
  var ctx = canvas.getContext("2d");

  var pt = ctx.transformedPoint(lastX, lastY);
  ctx.translate(pt.x, pt.y);
  var factor = Math.pow(scaleFactor, clicks);

  canvas.style.transform = `scale(${factor}, ${factor})`;
  ctx.scale(factor, factor);
  ctx.translate(-pt.x, -pt.y);
  scene.drawScene();
};
